json.projects @main_projects do |project|
  json.id project.id
  json.name project.name
  json.description project.description
  json.created_at project.created_at.strftime("%d-%m-%Y %H:%m")
  json.updated_at project.updated_at.strftime("%d-%m-%Y %H:%m")
end