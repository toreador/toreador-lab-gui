json.templates @templates do |template|
  json.id template.id
  json.name template.name
  json.description template.description
  json.flow_type template.batch
  json.created_at template.created_at.strftime("%d-%m-%Y %H:%m")
  json.updated_at template.updated_at.strftime("%d-%m-%Y %H:%m")

end