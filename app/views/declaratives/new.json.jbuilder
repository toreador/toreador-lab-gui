if !@declarative.blank?
  
  json.declarative do
    json.set! "@id", "http://www.toreador-project.eu/TDM/project-spec/" + @declarative.id.to_s
    json.set! "@context", @context
    json.set! "tdm:targetDataSources", @declarative.target_data_sources
    json.set! "tdm:anagraphic", @declarative.anagraphic
    json.set! "tdm:integration", @declarative.integration
    json.set! "tdm:representation", @declarative.representation
    json.set! "tdm:preparation", @declarative.preparation
    json.set! "tdm:analytics", @declarative.analytics
    json.set! "tdm:processing", @declarative.processing
    json.set! "tdm:display", @declarative.display
  end

  json.status do
    json.result "Success"
  end
else
  json.status do
    json.result "Fail"
    json.reason "Error in the declaration of the new declarative"
  end
  
end

