if !@declarative_name.blank?
  
  json.declarative do
    json.set! "name", @declarative_name
  end

  json.status do
    json.result "Success"
  end
else
  json.status do
    json.result "Fail"
    json.reason "Error in the destruction of the declarative"
  end
  
end
