json.project do
  json.id @project.id
  json.main_project @project.main_project_id
  json.name @project.name
  json.anagraphic @project.anagraphic
  json.workflow @project.workflow
  json.oozie_workflow @project.oozie_workflow
  json.owls_workflow @project.owls_workflow
  json.hideDictionary @project.hide_dictionary
  json.visualization_url @project.visualization_url
  json.done @project.done
  json.constraint @project.constraint
  json.flow_type @project.flow_type
  json.platform_flow_type @project.platform_flow_type
end

json.declaratives @project.declaratives do |declarative|
  json.set! "name", declarative.name
  json.set! "@id", "http://www.toreador-project.eu/TDM/project-spec/" + declarative.id.to_s
  json.set! "@context", @context
  json.set! "tdm:targetDataSources", declarative.target_data_sources
  json.set! "tdm:anagraphic", declarative.anagraphic
  json.set! "tdm:integration", declarative.integration
  json.set! "tdm:representation", declarative.representation
  json.set! "tdm:preparation", declarative.preparation
  json.set! "tdm:analytics", declarative.analytics
  json.set! "tdm:processing", declarative.processing
  json.set! "tdm:display", declarative.display
end



