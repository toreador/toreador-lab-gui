projects_json = json.array!(@projects) do |project|
  json.id project.id
  json.name project.name
  json.done project.done
  json.flow_type project.flow_type
  json.visualization_url project.visualization_url
  json.created_at project.created_at.strftime("%d-%m-%Y %H:%m")
  json.updated_at project.updated_at.strftime("%d-%m-%Y %H:%m")
  json.project_main @name.name

  if project.oozie_workflow == nil
    json.set! :status, 'STOPPED' 
  else
    json.set! :status, 'RUNNING'
  end
end

projects_json