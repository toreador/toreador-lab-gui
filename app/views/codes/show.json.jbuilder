json.code_project do
  json.id @project.id
  json.name @project.name
  json.platform @project.platform
  json.deployment @project.deployment
  json.result @project.result
  json.log @project.log
  json.is_running @project.running
  json.code @project.code
  json.code_generated @project.code_generated
end
