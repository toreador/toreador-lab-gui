class Credential < ApplicationRecord
  
  def self.find_by_platform(platform)
  	where(:platform => platform).first
  end
end
