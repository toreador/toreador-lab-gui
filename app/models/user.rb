class User < ApplicationRecord
  include DeviseTokenAuth::Concerns::User
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  
  has_many :projects, dependent: :destroy

  def is_admin?
  	self.is_admin
  end
  
  def role
  	return 'Admin' if self.is_admin?
  	return 'Normal User'
  end

  def self.find_by_email(email)
  	where(:email => email).first
  end       

  def full_name
    return "#{name} #{surname}" if !name.nil? or !surname.nil? 
    return "Unnamed"
  end
end
