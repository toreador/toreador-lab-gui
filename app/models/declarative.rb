class Declarative < ApplicationRecord
  belongs_to :project

  serialize :anagraphic, JSON
  serialize :integration, JSON
  serialize :representation, JSON
  serialize :processing, JSON
  serialize :analytics, JSON
  serialize :display, JSON
  serialize :preparation, JSON
  serialize :target_data_sources, JSON

  def self.createNewDeclarative(project, name)
  	#TODO: Rimuovere questa parte hardcoded
    integration = {
      "@id": "http://www.toreador-project.eu/TDM/project-spec/integration",
      "@context": {
        "s": "http://schema.org/",
        "tdm": "http://www.toreador-project.eu/TDM/"
      },
      "@type": "tdm:Area",
      "tdm:label": "Data Integration",
      "tdm:incorporates":[{
          "@type": "tdm:Goal",
          "tdm:label": "",
          "tdm:constraint": "{}",
          "tdm:incorporates": [{
          }]
        }
      ]
    }



  	representation = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/representation",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Representation",
    "tdm:incorporates": [{
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Format",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }, {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  }
  	preparation = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/preparation",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Preparation",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Knowledge Base Elicitation",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Data Reduction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Expansion and Correction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Transformation",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Cleaning and Integration",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Anonymization",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Techniques",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  }
  	analytics = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/analytics",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Analytics",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Analytics Aim",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Task",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Models",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Learning Approach",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Execution Model",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": [
            {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Single Process"
            }
          ]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Learning Environment",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Model Update",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Analytics Quality",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "True Positive Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "True Negative Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Positive Prediction Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "False Positive Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Root Mean Square Error",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Mean Absolute Error",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Overall Accuracy",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "K Coefficient",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          }
        ]
      }
    ]
  }

    processing = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/processing",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Processing",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Mode",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Analysis Goal",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Interaction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Performance",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "Data Storage Consistency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Elasticity",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Latency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Availability",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Operational Flexibility",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Security",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          }
        ]
      }
    ]
  }

    display = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/display",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Display and Reporting",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Data Model",
        "tdm:constraint": "{ }",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Dimensionality",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Cardinality",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Independent Data Type",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Dependent Data Type",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Operativity",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Interaction",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "User",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Goal",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  }

    declarative = Declarative.new
    declarative.name = name
    declarative.target_data_sources = [""]
    declarative.integration = integration
    declarative.representation = representation
    declarative.preparation = preparation
    declarative.analytics = analytics
    declarative.processing = processing
    declarative.display = display
    declarative.project = project
    declarative.anagraphic = project.anagraphic
    declarative.save
    declarative
  end
  
  def self.already_exists?(project, declarative_name)
  	Declarative.exists?(:project => project, :name=> declarative_name)
  end

end
