class Template < ApplicationRecord
  belongs_to :user
  serialize :graph, JSON
end
