class MainProject < ApplicationRecord
	belongs_to :user
	has_many :projects, dependent: :destroy
	has_many :codes, dependent: :destroy
end
