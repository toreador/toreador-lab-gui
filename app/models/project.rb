class Project < ApplicationRecord
	belongs_to :user
	belongs_to :main_project
	
	has_many :declaratives, dependent: :destroy
	serialize :anagraphic, JSON
	serialize :hide_dictionary, JSON
	serialize :workflow, JSON
  serialize :constraint, JSON

	enum platform: {
   	  dsl: "dsl",
      oozie: "oozie"
  }

  enum platform_flow_type: {
    service_based: 'service_based', 
    code_based: 'code_based'
  }


  def create_visualization_url
    visualization = self.declaratives
    self.visualization_url = "http://toreadorplatform.eng.it:8700/toreador-viz/#!/section0?priority1=3&interactionBoolean=true&interactionSelect=zoom&interactionPriority=2&goalBoolean=true&goalSelect=Composition&goalPriority=2&userBoolean=true&userSelect=Tech-User&userPriority=2&priority2=2&dimensionalityBoolean=true&dimensionalitySelect=Tree&dimensionalityPriority=2&cardinalityBoolean=true&cardinalitySelect=High&cardinalityPriority=2&datatypeBoolean=true&datatypeSelect=Nominal&datatypePriority=2"
  end

  def already_compiled?
  	return !self.oozie_workflow.blank?
  end

  def is_owned_by?(user)
  	return self.user_id == user.id
  end

end
