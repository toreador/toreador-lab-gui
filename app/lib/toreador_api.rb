class ToreadorApi
  require 'credential'
  require 'base64'

  attr_accessor :auth_token

  PLATFORM_URL = TOREADOR_CONFIG['platform_url']
  TAIGER_URL = TOREADOR_CONFIG['taiger_url']
  CODE_GENERATOR_URL = TOREADOR_CONFIG['code_generator_url']
  BEECEPTOR_URL = 'http://toreador.proxy.beeceptor.com'
  TAIGER_USERNAME = ENV["TAIGER_USERNAME"]
  TAIGER_PASSWORD = ENV["TAIGER_PASSWORD"]

  Excon.defaults[:ssl_verify_peer] = false


  def self.authorize
    url = 'http://toreador.hopto.org:8081/toreadorLab-owls-editor-service/api/authenticate'
    credentials = {:username => TAIGER_USERNAME, :password=> TAIGER_PASSWORD}
    p credentials
    response = self.service(url, credentials, nil ,true)
    json_response = nil
    if response != nil and response.status
      json_response = JSON.parse response.body
      taiger_credential = Credential.find_by_platform('taiger')
      if !taiger_credential.blank? and json_response['id_token']!=nil
        taiger_credential.token = json_response['id_token']
      elsif json_response['id_token']!=nil
        taiger_credential = Credential.new()
        taiger_credential.platform = 'taiger'
        taiger_credential.token = json_response['id_token']
      end

      taiger_credential.save
      
    end
    json_response['id_token']
  end

  def self.build_body_to_run(body, owls)
    #body[:name] = "tl" + body[:name].to_s
    body[:status] = "running"
    body[:translation] = ""
    body[:procedure] = ""
    body = body.to_json
    
    #body = 
    
    body = body.sub("\"procedure\":\"\"","\"procedure\":\"" + Base64.strict_encode64(owls) + "\"")
    
    #body["procedure"] = owls
    return body
  end

  def self.get_services(declaratives, counter=3)
  	url = TAIGER_URL + '/submitDeclarativeModels'
    
    auth_token = "Bearer " + self.get_taiger_token()
  	response = self.service(url, declaratives, auth_token, true)
    
    if (response.status == 401 and counter == 0) or response.status == 200
      return response
    elsif response.status == 401 and counter > 0
      self.authorize()
      response = self.get_services(declaratives, counter - 1)
    else
      return response
    end
    response
    
  end

  def self.get_taiger_token
    credential = Credential.find_by_platform('taiger')
    auth_token = nil
    if credential.blank? or credential.token.blank?
      auth_token = self.authorize()
    else
      auth_token = credential.token
    end
    auth_token
  end

  def self.get_owls(workflow, counter=3)
  	url = TAIGER_URL + '/createOWLSfromWorkflow?explicitReferences=INSERT_OWLS_TRIPLES'
    auth_token = "Bearer " + self.get_taiger_token()
  	response = self.service(url, {"id": "http://www.toreador.it", "elements": workflow}, auth_token, true)
    
    if (response.status == 401 and counter == 0) or response.status == 200
      return response
    elsif response.status == 401 and counter > 0
      self.authorize()
      response = self.get_owls(workflow, counter - 1)
    else
      return response
    end
    response

  end

  def self.compile_to_platform(body, platform, already_compiled)
    url = PLATFORM_URL + '/api/orchestration/workflows'
    
    owls = body[:procedure]
    body = self.build_body_to_run(body, owls)
    
    if already_compiled
      return self.compile(url, body, '', false)
    else
      return self.service(url, body, '', false)
    end
    
  end

  def self.run_platform(body, platform)
    url = PLATFORM_URL + '/api/execution/workflows'
    owls = body[:procedure]
    body = self.build_body_to_run(body, owls)
    self.service(url, body, '', false)
  end

  def self.send_constraint_to_code_based_platform(body)
    url = CODE_GENERATOR_URL
    self.service(url, body, '', true)
  end

  #def self.run(body)
  #  url = CODE_GENERATOR_URL + '/run'
  #  self.service(url, body, '')
  #end

  def self.service(url, body, auth_token = '', convert_json = false)
    
      header = { 
        "Accept" => "application/json",
        "Content-Type" => "application/json"}
      
      if auth_token != ''
        header["Authorization"] = auth_token
      end
      
      response = nil
      if convert_json 
  	    response = Excon.post(url,
  		    :headers => header, 
  		    :body => body.to_json,
          :read_timeout => 360,
          :connect_timeout => 360,
          instrumentor: ActiveSupport::Notifications
  	    )
      else
        response = Excon.post(url,
          :headers => header, 
          :body => body,
          :read_timeout => 360,
          :connect_timeout => 360,
          instrumentor: ActiveSupport::Notifications
        )
      end

      response
  end

  def self.compile(url, body, auth_token = '', convert_json = false)
    
      header = { 
        "Accept" => "application/json",
        "Content-Type" => "application/json"}
      
      if auth_token != ''
        header["Authorization"] = auth_token
      end

      response = nil
      if convert_json 
        response = Excon.put(url,
          :headers => header, 
          :body => body.to_json,
          :read_timeout => 360,
          :connect_timeout => 360,
          instrumentor: ActiveSupport::Notifications
        )
      else
        
        response = Excon.put(url,
          :headers => header, 
          :body => body,
          :read_timeout => 360,
          :connect_timeout => 360,
          instrumentor: ActiveSupport::Notifications
        )
      end
       
      
      response
      
  end
  
end