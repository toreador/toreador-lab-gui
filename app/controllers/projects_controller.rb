class ProjectsController < ApplicationController
  before_action :authenticate_current_user

  def index
  	@projects = Project.select("id, name, done, visualization_url, created_at, updated_at, oozie_workflow, flow_type").where(:user=> current_user).where(:main_project => params[:id])
    #@code_generated = Code.where(:main_project => params[:id])
    @name = MainProject.select("name").where(:id=> params[:id]).first
    
  end

  def destroy
  	if !params[:id].blank?
  	  project = Project.where(:id => params[:id]).where(:user => current_user).first
  	
  	  unless project.blank?
        project.destroy
  	  end
  	end
  end

  def show
  	@project = {}
  	p params[:id]
  	if !params[:id].blank?
  	  @project = Project.where(:id => params[:id]).where(:user => current_user).first
      @context = {s: 'http://schema.org/', tdm: 'http://www.toreador-project.eu/TDM/'}
  	  p @project
  	end
  	@project
  end

  def new
    project_id = nil
    if !params[:id].blank? and !params[:flow_type].blank?
      main_project = MainProject.where(:id=>params[:id]).where(:user=>current_user).first
      
      project = Project.new
      project.name = params[:name]
      project.user = current_user
      project.platform_flow_type = params[:flow_type]
      if params[:flow_type]=='code_based'
        project.flow_type = params[:flow_type]
        project.constraint = {"nodes_number"=>{"operator"=>"", "value"=>nil, "goal"=>"performance"}, "ram"=>{"operator"=>"", "value"=>nil, "goal"=>"performance"}, "storage"=>{"operator"=>"", "value"=>nil, "goal"=>"performance"}, "deployment_platform"=>{"value"=>"", "goal"=>"mode"}, "technology"=>{"value"=>"", "goal"=>"mode"}, "parallel_pattern"=>{"value"=>"", "goal"=>"knowledge base elicitation"}, "data_location"=>{"value"=>"", "goal"=>"data source property"}, "web_socket_url"=>{"value"=>"", "goal"=>"data source property"}}
      else
        project.flow_type = params[:service_flow_type]
      end
      project.oozie_workflow = ""
      project.owls_workflow = ""
      project.workflow = {}
      project.anagraphic = {}
      declarative_name = params[:declarative]
      project.hide_dictionary = {}
      project.hide_dictionary[declarative_name] = {}
      project.main_project = main_project

    
      res = project.save

      declarative = Declarative.createNewDeclarative(project, params[:declarative])
      project_id = project.id
    # else
    #  main_project = MainProject.where(:id=>params[:id]).where(:user=>current_user).first
    #  code_generated = Code.new
    #  code_generated.code = ''
    #  code_generated.platform = ''
    #  code_generated.deployment = ''
    #  code_generated.result = ''
    #  code_generated.log = ''
    #  code_generated.code_generated = ''
    #  code_generated.running = false
    #  code_generated.main_project = main_project
    #  code_generated.name = params[:name]

    #  res = code_generated.save
    #  project_id = code_generated.id
    end
    render :json => {"id": project_id}
  end

  def clone
    #TODO: Quando creo il project creo una conf di default
    project_cloned = nil
    unless params[:clone_id].blank? && current_user.blank?
      project = Project.includes(:declaratives).where(:id => params[:clone_id]).where(:user => current_user).first
      project_cloned = project.dup
      project_cloned.name = params[:name]
      project_cloned.done = false
      project_cloned.oozie_workflow = nil
      project_cloned.visualization_url = ""
      project_cloned.save

      project.declaratives.each do |declarative|
        declarative_cloned = declarative.dup
        declarative_cloned.project = project_cloned
        declarative_cloned.save
      end

    end
    
    render :json => {"id": project_cloned.id}
  end

  def update
  end

  def save_workflow
  	#@response = {"id": }
  	p params[:id]
  	if !params[:id].blank? and !params[:graph].blank?
  	  @project = Project.where(:id => params[:id]).where(:user => current_user).first
  	  @project.workflow = params[:graph]
  	  @project.save
  	  render :json => {"id": @project.id}
  	end
  end

  def save_constraint
    #@response = {"id": }
    p params[:id]
    if !params[:id].blank? and !params[:constraint].blank?
      @project = Project.where(:id => params[:id]).where(:user => current_user).first
      
      @project.constraint = params[:constraint]
      result = @project.save
      if result
        constraints = {"constraints": @project.constraint}
        response = ToreadorApi.send_constraint_to_code_based_platform(constraints)
        if (response.status == 200 || response.status == 201)
          render :json => {"id": @project.id}
          return
        else
          render :json => {"result": response.reason_phrase}, status: response.status
          return
        end
      end
      render :json => {"result": "The save action of the constraint was denied"}, status: 500
      return
    end
  end

  def get_workflow
    if !params[:id].blank?
      @project = Project.where(:id => params[:id]).where(:user => current_user).first
      if !@project.blank?
        render :json => {"graph": @project.workflow}
      else
        render :json => {"graph": "{}"}
      end
    end
    return
  end

  def save_owls
  	#@response = {"id": }
  	p params[:id]
  	if !params[:id].blank? and !params[:owls].blank?
  	  @project = Project.where(:id => params[:id]).where(:user => current_user).first
  	  @project.owls_workflow = params[:owls]
  	  @project.save
  	  render :json => {"id": @project.id}
  	end
  end

  def save_oozie
  	#@response = {"id": }
  	p params[:id]
  	if !params[:id].blank? and !params[:oozie].blank?
  	  @project = Project.where(:id => params[:id]).where(:user => current_user).first
  	  @project.oozie_workflow = params[:oozie]
  	  @project.save
  	  render :json => {"id": @project.id}
  	end
  end

  def save_declarative
    p params
    if !params[:id].blank? and !params[:declarative].blank?
      @project = Project.where(:id => params[:id]).where(:user => current_user).first
      if !@project.blank?
        declaratives = params[:declarative]
        # anagraphic
        @project.anagraphic = declaratives["anagraphic"]
        # dictionary
        @project.hide_dictionary = declaratives["dictionary"]
        @project.save
        # form
        declaratives["form"].each do |key, val|
          #TODO: Change using the id
          p key
          declarative = @project.declaratives.where(:name => key).first
          p val
          declarative.anagraphic = val["tdm:anagraphic"]
          declarative.display = val["tdm:display"]
          declarative.integration = val["tdm:integration"]
          declarative.preparation = val["tdm:preparation"]
          declarative.processing = val["tdm:processing"]
          declarative.representation = val["tdm:representation"]
          declarative.analytics = val["tdm:analytics"]
          declarative.target_data_sources = val["tdm:targetDataSources"]
          declarative.save
        end
      end
    end
    render :json => {"id": 23}
  end

  private
    
    def project_params
      params.permit(:name)
    end

end
