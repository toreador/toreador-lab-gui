class TemplatesController < ApplicationController
  before_action :authenticate_current_user

  def new
    result = false
    
    if !params[:name].blank? && !params[:description].blank? && !params[:graph].blank? && !params[:batch].blank?
      
      template = Template.new
      template.name = params[:name]
      template.description = params[:description]
      template.graph = params[:graph]
      template.batch = params[:batch]
      template.user = current_user
      result = template.save

    end

    if result
      render :json => {"id": template.id}
    else
      render :json => {"msg": "Unprocessable entity"}, :status => :unprocessable_entity
    end
  end

  def index
  	@templates = Template.where(:user => current_user)
  end

  def destroy
  	result = false
  	unless params[:id].blank?
  	  template = Template.where(:user => current_user).where(:id => params[:id]).first
  	  unless template.blank?
  	    result = template.destroy
  	  end
  	end

  	if result
      render :json => {"id": template.id}
    else
      render :json => {"msg": "Unprocessable entity"}, :status => :unprocessable_entity
    end
  end

  private
    
    def template_params
      params.permit(:name)
    end
end
