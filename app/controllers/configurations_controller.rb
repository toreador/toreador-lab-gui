class ConfigurationsController < ApplicationController
  #before_action :authenticate_user!

  before_action :authenticate_current_user, except: [:close_run]
  skip_before_action :verify_authenticity_token, :only=> :close_run
  
  def new
  end

  def cleansubconf
      msg = {
  "@id": "http://www.toreador-project.eu/TDM/project-spec/mycustomid",
  "@context": {
    "s": "http://schema.org/",
    "tdm": "http://www.toreador-project.eu/TDM/"
  },
  "tdm:targetDataSources": [
    ""
  ],
  "tdm:anagraphic": {
    "tdm:firstname": "",
    "tdm:lastname": "",
    "tdm:email": "",
    "tdm:affiliation": "",
    "tdm:role": "",
    "tdm:project_title": "",
    "tdm:project_description": ""
  },
  "tdm:integration": {
      "@id": "http://www.toreador-project.eu/TDM/project-spec/integration",
      "@context": {
        "s": "http://schema.org/",
        "tdm": "http://www.toreador-project.eu/TDM/"
      },
      "@type": "tdm:Area",
      "tdm:label": "Data Integration",
      "tdm:incorporates":[{
          "@type": "tdm:Goal",
          "tdm:label": "",
          "tdm:constraint": "{}",
          "tdm:incorporates": [{
          }]
        }
      ]
    },
  "tdm:representation": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/areaRepre",
    "@type": "tdm:Area",
    "tdm:label": "Data Representation",
    "tdm:incorporates": [
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  },
  "tdm:preparation": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/areaPreparation",
    "@type": "tdm:Area",
    "tdm:label": "Data Preparation",
    "tdm:incorporates": [
      {
        "@type": "tdm:Goal",
        "tdm:label": "Knowledge Base Elicitation",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Data Reduction",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Data Expansion and Correction",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Checkbox",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Data Transformation",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Data Cleaning and Integration",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Anonymization",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Techniques",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  },
  "tdm:analytics": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/area-analytics",
    "@type": "tdm:Area",
    "tdm:label": "Data Analytics",
    "tdm:incorporates": [
      {
        "@type": "tdm:Goal",
        "tdm:label": "Analytics Aim",
        "tdm:constraint": "{ \"percentage\": {\"value\":23}}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Task",
            "tdm:visualisationType": "Checkbox",
            "tdm:constraint": "{}",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Models",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Checkbox",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Learning Approach",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Goal",
        "tdm:constraint": "{}",
        "tdm:label": "Analytics Quality",
        "tdm:incorporates": [
          {
            "@type": "tdm:Indicator",
            "tdm:constraint": "{}",
            "tdm:label": "True Positive Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "True Negative Rate",
            "tdm:constraint": "{ \"percentage\": {\"value\":23}}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Positive Prediction Rate",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "False Positive Rate",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Root Mean Square Error",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Mean Absolute Error",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Overall Accuracy",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "K Coefficient",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          }
        ]
      }
    ]
  },
  "tdm:processing": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/area-processing",
    "@type": "tdm:Area",
    "tdm:label": "Data Processing",
    "tdm:incorporates": [
      {
        "@type": "tdm:Goal",
        "tdm:label": "Mode",
        "tdm:incorporates": [
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Analysis Goal",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Interaction",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Performance",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Data Storage Consistency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Elasticity",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Latency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Availability",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Operational Flexibility",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Security",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:value": 0
              }
            ]
          }
        ]
      }
    ]
  },
  "tdm:display": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/area-display",
    "@type": "tdm:Area",
    "tdm:label": "Data Display and Reporting",
    "tdm:incorporates": [
      {
        "@type": "tdm:Goal",
        "tdm:label": "Data Model",
        "tdm:constraint": "{ \"percentage\": {\"value\":23}}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Dimensionality",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Data Cardinality",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Independent Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Dependent Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Operativity",
        "tdm:constraint": "{}",
        "tdm:incorporates": [
          {
            "@type": "tdm:Feature",
            "tdm:label": "Interaction",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "User",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Goal",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  }
};
    render :json => msg
  end

  def getoozie

    workflow = ""
    unless params.blank?
      id = params[:id]
      project = Project.find(id)
      if !project.blank?
        workflow = project.oozie_workflow
      end
    end

    render :json => {"data": workflow}
  end

  def getowls
    #msg = "Sono dell'owls"  
    #render :json => {"data": msg}
    workflow = params['elements']
    response = ToreadorApi.get_owls(workflow) 
    render :json => response.body      
  end

  def getserviceselection
    
    if params.blank? || params['form'].blank? || params['form'] == ''
      msg = "Cannot select services with an empty form"
      render :json => msg, status: 500
      return
    end

    declaratives = params['form']
    
    response = ToreadorApi.get_services(declaratives) 
    
    templates = Template.where(:user => current_user).as_json

    resp = JSON.parse response.body

    if !resp.blank? and response.status == 200
      resp['servicesByArea'].store('Templates', templates)
    else
      resp = {
        "servicesByArea": 
        {
          "Data Representation": [],
          "Data Preparation": [],
          "Data Analytics":[],
          "Data Processing":[],
          "Data Display and Reporting":[],
          "Templates": []
        },
        "message": ""
      }
      
      resp['message'] = "Error in the Service Selection API" if response.status == 500
      resp['message'] = "The API call used an invalid key" if response.status == 401 
      render :json => resp, status: response.status
      return
    end
    #p resp
    render :json => resp
    return
  end

  def compile
    unless configuration_params.blank? and configuration_params['id'].blank?
      
      project = Project.find(configuration_params['id'])

      if !project.is_owned_by?(current_user)
        render :json => {"result": "Operation not permitted"}, status: 500
        return
      end
      
      if !project.blank? and configuration_params["OWLS"] != ""
        #TODO: Adesso l'errore del servizio di TAIGER ci spacca la traduzione
        project.owls_workflow = configuration_params["OWLS"]
        project.save
      end
      
      response = create(configuration_params, true, project)
      
      if (response.status == 200 || response.status == 201) && !project.blank?
        if configuration_params['type'] == 'stream'
          project.done = true
        else
          project.done = false
        end
        
        body_json = JSON.parse response.body
        #TODO: Controllo del campo translation, se è vuoto o non esiste 
        # allora torno una stringa vuota
        project.oozie_workflow = body_json["translation"]

        project.save 
        render :json => response.body
        return
      end
      
    end
    
    render :json => {"result": "Operation not permitted"}, status: 500
    return
  end

  def run
    unless configuration_params.blank? and configuration_params['id'].blank?
      
      project = Project.find(configuration_params['id'])
      
      if !project.is_owned_by?(current_user)
        render :json => {"result": "Operation not permitted"}, status: 500
        return
      end

      response = create(configuration_params, false, project)

      if (response.status == 200 || response.status == 201) && !project.blank?
        if configuration_params['type'] == 'stream'
          #project.visualization_url = response.body["url"]
          project.done = false
        else
          project.done = true
        end

        body_json = JSON.parse response.body

        # TODO: Change it calculating the actual implementation of the path
        #project.visualization_url = "http://toreadorplatform.eng.it:8700/toreador-viz/#!/section0?priority1=3&interactionBoolean=true&interactionSelect=zoom&interactionPriority=2&goalBoolean=true&goalSelect=Composition&goalPriority=2&userBoolean=true&userSelect=Tech-User&userPriority=2&priority2=2&dimensionalityBoolean=true&dimensionalitySelect=Tree&dimensionalityPriority=2&cardinalityBoolean=true&cardinalitySelect=High&cardinalityPriority=2&datatypeBoolean=true&datatypeSelect=Nominal&datatypePriority=2"
        # create the proper visualization URL based on the declaratives
        project.create_visualization_url
        project.save 
        render :json => response.body
        return
      end
      
    end
    
    render :json => {"result": "Operation not permitted"}, status: 500
    return
  end

  def close_run
    msg = "Failed"
    status = 500
    unless close_run_params.blank?
      project = Project.find(close_run_params[:id])
      unless project.blank? 
        project.visualization_url = close_run_params[:url]
        project.done = true
        project.save

        msg = "Success"
        status = 200
      end
    end

    render :json => {"msg": msg}, status: status
  end

  private
    
    def configuration_params
      params.require(:body).permit(:id,:OWLS,:type,:platform)
    end

    def close_run_params
      params.except(:format, :configuration).permit(:id, :url)
    end

    def create(configuration_params, compile, project)

      body = configuration_params
      
      id = "tl" + project.id.to_s 
      
      unless configuration_params['platform'].blank?
        project.platform = configuration_params['platform'].downcase
        project.save
      end

      # TODO: Devo gestire il caso l'OWLS sia nullo. 
      # In teoria dovrei sempre salvare l'owls che mi è stato tornato ?
      # Quale workflow devo tenere in conto ?
      # Ogni volta che affronto un tab e vado avanti sovrascrivo a meno che sia in esecuzione ?
      # Se esistono già delle mezze configurazioni come mi regolo ?
      # Ritorno quelle nella tab e poi eventualmente l'utente le modifica ?
      owls = project.owls_workflow

      # Filter to get only the services in order to send it to the ENG platform
      services = []
      owls_to_compile = nil
      workf = project.workflow.dup

      nodes_filtered = workf.keep_if {|k, value| value['group']=='nodes' && value['data']['nodeType']['type']=='SERVICE'}
      nodes_filtered.each do |k, value| 
        value['data']['service'].delete('serviceName')
        value['data']['service'].delete('owls')
        services.push(value['data']['service'])
      end
      
      owls_to_compile = {name: id, mode: project.flow_type.upcase, procedure: owls, targetPlatform: project.platform.upcase, services: services}
      
      response = nil
      if compile
        response = ToreadorApi.compile_to_platform(owls_to_compile, project.platform.upcase, project.already_compiled?)
      else
        response = ToreadorApi.run_platform(owls_to_compile, project.platform.upcase)
      end
      
      return response
    end

end
