class UsersController < ApplicationController
  def index
	  @users = User.all
  end

  def show
	  @user = User.find(params[:id]) #TODO passare l'id
  end

  def edit
	  @user = User.find(params[:id])
  end

  def update
  	if params[:id].blank?
  	  redirect_to user_index_url, alert: "Please specify the correct user"
  	end

  	if current_user.is_admin?
  	  user = User.find(params[:id])
  	  if user.update_attributes(update_params)
	    redirect_to user_show_url(params[:id]), notice: "User updated"
	  else
	    redirect_to user_show_url(params[:id]), alert: "Cannot update the user"
	  end
    end
  end

  def delete
  	if current_user.is_admin?
	    @user = User.find(params[:id])
 	    @user.destroy!
 	  end
	
	  redirect_to user_index_url
  end

  def all
  	respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context) }
    end
  end

  private

  def update_params
  	params.require(:user).permit(:name, :surname, :is_admin)
  end

end
