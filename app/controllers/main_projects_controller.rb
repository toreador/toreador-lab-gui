class MainProjectsController < ApplicationController
  before_action :authenticate_current_user

  def index
  	@main_projects = MainProject.select("id, name, description, created_at, updated_at").where(:user=> current_user)
  end

  def show
  	@main_projects = {}
  	
  	if !params[:id].blank?
  	  @project = MainProject.where(:id => params[:id]).where(:user => current_user).first
  	  p @project
  	end
  	@project
  end
  
  def clone
    #TODO: Quando creo il project creo una conf di default
    project_cloned = nil
    unless params[:clone_id].blank? && current_user.blank?
      clonable = MainProject.where(:id => params[:clone_id]).where(:user => current_user).first
      clonable = clonable.deep_clone include: { projects: :declaratives }
      clonable.name = params[:name] if !params[:name].blank?
      clonable.description = params[:description] if !params[:description].blank?

      clonable.projects.each do |project|
        project.done = false
        project.visualization_url = nil
        project.oozie_workflow = nil
      end

      clonable.save
    end
    
    render :json => {"id": clonable.id}
  end

  def destroy
    if !params[:id].blank?
      main_project = MainProject.where(:id => params[:id]).where(:user => current_user).first
    
      unless main_project.blank?
        main_project.destroy
      end
    end
  end

  def new
    unless params[:name].blank? && params[:description].blank?
      main_project = MainProject.new
      main_project.name = params[:name]
      main_project.description = params[:description]
      main_project.user = current_user
      main_project.save
    end 
    
  end

end
