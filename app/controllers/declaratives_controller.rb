class DeclarativesController < ApplicationController
  before_action :authenticate_current_user

  def new
  	declarative_name = params[:name]
  	project = Project.where(:id => params[:id]).where(:user => current_user).first
  	@declarative = nil

  	if project and declarative_name and !Declarative.already_exists?(project, declarative_name)

      @declarative = Declarative.createNewDeclarative(project, declarative_name)
      # '{\"s\": \"http://schema.org/\",\"tdm\": \"http://www.toreador-project.eu/TDM/\"}'
      @context = {s: 'http://schema.org/', tdm: 'http://www.toreador-project.eu/TDM/'}
      
      project.hide_dictionary[declarative_name] = {}
      project.save
  	end
  	@declarative
  end

  def destroy
  	@declarative_name = nil
  	unless params[:id].blank?
  	  declarative = Declarative.where(:id => params[:id]).first
  	  if declarative
  	  	if declarative.project.user == current_user
  	  	  project = declarative.project	
  	  	  @declarative_name = declarative.name
  	  	  if declarative.destroy
  	  	    project.hide_dictionary.delete(@declarative_name)
            project.save
  	  	  end
  	    end
  	  end
  	end
  	@declarative_name
  end

  private
    
    def declarative_params
      params.required(:id).permit(:name)
    end
end
