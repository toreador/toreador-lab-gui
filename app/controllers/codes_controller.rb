class CodesController < ApplicationController
  before_action :authenticate_current_user

  def index
  	@projects = Project.select("id, name, done, visualization_url, created_at, updated_at, oozie_workflow, flow_type").where(:user=> current_user).where(:main_project => params[:id])
    @code_generated = Code.where(:main_project => params[:id])
    #byebug
  end

  def destroy
  	if !params[:id].blank?

      if MainProject.includes(:codes).where(codes: {id: params[:id]}).where(user: current_user).exists?
        project_code = Code.where(:id => params[:id]).first
        unless project_code.blank?
          project_code.destroy
        end
      end
  	
  	  
  	end
  end

  def show
  	@project = {}
  	p params[:id]
  	if !params[:id].blank?
      if MainProject.includes(:codes).where(codes: {id: params[:id]}).where(user: current_user).exists?
        @project = Code.where(:id => params[:id]).first
      end

  	end
  	@project
  end

  

  def clone
    #TODO: Quando creo il project creo una conf di default
    project_cloned = nil
    unless params[:clone_id].blank?
      if MainProject.includes(:codes).where(codes: {id: params[:clone_id]}).where(user: current_user).exists?
        project_code = Code.where(:id => params[:clone_id]).first
        project_code_cloned = project_code.dup
        project_code_cloned.name = params[:name]
        project_code_cloned.running = false
        project_code_cloned.result = ''
        project_code_cloned.log = ''
        project_code_cloned.code_generated = ''
        project_code_cloned.save
      end  
    end
    
    render :json => {"id": project_code_cloned.id}
  end

  def save
    code = nil
    unless params[:id].blank?
      code = Code.where(:id => params[:id]).first
      code.code = params[:code]
      code.platform = params[:platform]
      code.deployment = params[:deployment]
      code.save
    end

    render :json => {"id": code.id}
  end

  def update
  end

  private
    
    def project_params
      params.permit(:name)
    end

end
