class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  include Error::ErrorHandler
  
  protect_from_forgery with: :exception
  #before_action :authenticate_user!

  before_action :configure_permitted_parameters, if: :devise_controller?
  after_action :set_xsrf_token_cookie

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  respond_to :json

  def angular
    render 'layouts/application'
  end

  protected

  # Add my attributes added to the devise User class
  def configure_permitted_parameters
  	added_attrs = [:surname, :name, :email, :password, :password_confirmation, :remember_me] 
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def record_not_found
  	render json: {error: exception.message}.to_json, status: 404
    return
  end

  # Setto il cookie per permettere anche ad Angular di usare le chiamate
  def set_xsrf_token_cookie
    if protect_against_forgery? && !respond_to?(:__exclude_xsrf_token_cookie?)
      cookies['XSRF-TOKEN'] = form_authenticity_token
    end
  end

  # Controllo la validità della richiesta di Angular
  def verified_request?
    if respond_to?(:valid_authenticity_token?, true)
      super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
    else
      super || form_authenticity_token == request.headers['X-XSRF-TOKEN']
    end
  end

  def authenticate_current_user
    head :unauthorized if get_current_user.nil?
  end

  def get_current_user
    return nil unless cookies[:auth_headers]
    auth_headers = JSON.parse(cookies[:auth_headers])

    expiration_datetime = DateTime.strptime(auth_headers["expiry"], "%s")
    current_user = User.find_by(uid: auth_headers["uid"])

    if current_user &&
       current_user.tokens.has_key?(auth_headers["client"]) &&
       expiration_datetime > DateTime.now

      @current_user = current_user
    end
    @current_user
  end
  

end
