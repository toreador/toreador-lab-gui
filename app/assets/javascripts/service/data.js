angular.module('formApp').service('DataService',['$http', '$q', '$rootScope', function($http, $q, $rootScope){
	var service = {};

  service.getServices = function(myForm){
    console.log(myForm);
    
    return $http.post(
      Routes.configurations_get_service_selection_path({format: 'json'}), {"form": myForm}).then(function(response){
      console.log(response);
      return response.data;
    }, function(response){
      
      $rootScope.addAlert('danger', 'error', response.data.message);
      
      var services = { 
        "servicesByArea": {
          "Data Integration": [],
          "Data Representation": [],
          "Data Analytics": [],
          "Data Processing": [],
          "Data Display and Reporting": [],
          "Templates": []
        }
      };
      return services; 
    });
    
    /*
    return $http({
      method: 'POST',
      url: 'http://54.229.142.100:8081/toreadorLab-owls-editor-service/api/services/submitDeclarativeModel',
      data: myForm,
      headers: {
        'Content-Type': 'application/ld+json'
      }
    }).then(function(result){
      console.log(response);
      return response.data;
    })
    */
    /*
    return $http.post('http://54.229.142.100:8081/toreadorLab-owls-editor-service/api/services/submitDeclarativeModels', myForm).then(function(response){
    //return $http.post(Routes.configurations_get_service_selection_path({format: 'json'}), {"form": myForm}).then(function(response){
      console.log(response);
      return response.data;
    });
    //return this.services;
    */
  };

  service.saveTemplate = function(template){
    return $http.post(Routes.template_new_path({format: 'json'}),template).then(function(response){
      $rootScope.addAlert('success', 'Saved', 'The template is now saved in the platform');
      return response.data;
    }, function(response){
      
      $rootScope.addAlert('danger', 'error', response.data.message);
      
      return nil; 
    })
  };

  service.getWorkflow = function(projectId){
    //console.log(myForm);
    
    return $http.get(
      Routes.configuration_get_oozie_path(projectId, {format: 'json'})).then(function(response){
      return response.data;
    }, function(response){
      
      $rootScope.addAlert('danger', 'error', response.data.message);
      response.data.data = "";
      return response.data;
      }) 
    
    // '"' + owls + '"'
    //debugger;
    /*
    return $http.post('http://alphaplus.dti.unimi.it/oozie', {"owls": owls}).then(function(response){
      debugger;
      console.log(response);
      return response.data;
    });
*/
    //return this.services;
  };

  service.compileWorkflow = function(body){
    //var numId = parseInt(id, 10);
    return $http.post(
      Routes.configuration_compile_path({format: 'json'}), {"body": body}).then(function(response){
      $rootScope.addAlert('success', 'Success', 'The workflow is now compiled for the platform chosen by the user');
      return response.data;
    }, function(response){
      
      $rootScope.addAlert('danger', 'error', response.data.message);
      
      return ""; 
    });
  }


  service.runWorkflow = function(body){
    //var numId = parseInt(id, 10);
    return $http.post(
      Routes.configuration_run_path({format: 'json'}), {"body": body}).then(function(response){
      $rootScope.addAlert('success', 'Success', 'The workflow is running !');
      return response.data;
    }, function(response){
      
      $rootScope.addAlert('danger', 'error', response.data.message);
      
      return ""; 
    });
  }

  service.getOWLSWorkflow = function(workflow){
    var data = {};
    var startPresent = false, 
        stopPresent = false;

    if(workflow === undefined || workflow.nodes === undefined){
      data.data = ""
      $rootScope.addAlert('danger', 'error', "We cannot compile to OWL-S from an empty Service Composition !");
      return data.data;
    }

    var nodesNumber = workflow.nodes.length;
    
    if(nodesNumber == 0){
      data.data = ""
      $rootScope.addAlert('danger', 'error', "We cannot compile to OWL-S from an empty Service Composition !");
      return data.data;
    }

    for (var i = nodesNumber - 1; i >= 0; i--) {
      if(workflow.nodes[i].data.id == "START"){
        startPresent=true;
      }

      if(workflow.nodes[i].data.id == "STOP"){
        stopPresent=true;
      }
    };

    if(!startPresent || !stopPresent){
      data.data = ""
      $rootScope.addAlert('danger', 'error', "We cannot compile to OWL-S because the compiler needs a START and a STOP !");
      return data.data;
    }

    return $http.post(Routes.configuration_get_owls_path({format: 'json'}), 
          {"id": "http://www.toreador.it", "elements": workflow})
        .then(function(response){
          return response.data;
        }, function(response){
          $rootScope.addAlert('danger', 'error', response.data.message);
          response.data.data = "";
          return response.data; 
        })
  
    /*
    return $http.post('http://54.171.144.211:8081/toreadorLab-owls-editor-service/api/authenticate',
      {"username": "admin", "password":"T0r34d0Rlab3d1t0r"}).then(function(response){
        var token = 'Bearer ' + response.data.id_token;
        var config = {
          headers: {
            'Authorization': token
          }
        };

        $http.post('http://54.171.144.211:8081/toreadorLab-owls-editor-service/apiservices/createOWLSfromWorkflow?explicitReferences=INSERT_OWLS_TRIPLES', 
          {"id": "http://www.toreador.it", "elements": workflow}, 
          config )
        .then(function(response){
          console.log(response);
          return response.data;
        })
    });
    */
    
    /*
    return $http.get(
      Routes.configuration_get_owls_path({format: 'json'})).then(function(response){
      return response.data;
    });
    */
  };


	service.getCleanForm = function(){
		console.log(this.cleanForm);
		//this.cleanForm = {'Hello':'hello'};
    return $http.get(
      Routes.configuration_get_clean_subconf_path({format: 'json'})).then(function(response){
      return response.data;
    }, function(response){
      
      $rootScope.addAlert('danger', 'error', response.data.message);
      
      return {}; 
    });
		//return this.cleanForm;
	};

  service.getAnalytics = function(){
    return this.analytics;
  };

  service.getProcessing = function(){
    return this.processing;
  };

  service.getDisplay = function(){
    return this.display;
  };

  service.getRepresentation = function(){
    return this.representation;
  };

  service.getPreparation = function(){
    return this.preparation;
  };

  service.services = {
    "representation": [
      {
      "id": 1,
      "title": "Service Name",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 2,
      "title": "Service Name2",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 3,
      "title": "Service Name3",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 4,
      "title": "Service Name4",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 5,
      "title": "Service Name5",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 6,
      "title": "Service Name6",
      "description": "Some description",
      "loop": false
      }
    ],
    "preparation": [{
      "id": 1,
      "title": "Service Name",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 2,
      "title": "Service Name2",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 3,
      "title": "Service Name3",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 4,
      "title": "Service Name4",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 5,
      "title": "Service Name5",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 6,
      "title": "Service Name6",
      "description": "Some description",
      "loop": false
      }],
    "analytics": [{
      "id": 1,
      "title": "Service Name",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 2,
      "title": "Service Name2",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 3,
      "title": "Service Name3",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 4,
      "title": "Service Name4",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 5,
      "title": "Service Name5",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 6,
      "title": "Service Name6",
      "description": "Some description",
      "loop": false
      }],
    "processing": [{
      "id": 1,
      "title": "Service Name",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 2,
      "title": "Service Name2",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 3,
      "title": "Service Name3",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 4,
      "title": "Service Name4",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 5,
      "title": "Service Name5",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 6,
      "title": "Service Name6",
      "description": "Some description",
      "loop": false
      }],
    "display": [{
      "id": 1,
      "title": "Service Name",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 2,
      "title": "Service Name2",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 3,
      "title": "Service Name3",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 4,
      "title": "Service Name4",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 5,
      "title": "Service Name5",
      "description": "Some description",
      "loop": false
      },
      {
      "id": 6,
      "title": "Service Name6",
      "description": "Some description",
      "loop": false
      }]
  };

	service.cleanForm = {
  "@id": "http://www.toreador-project.eu/TDM/project-spec/mycustomid",
  "@context": {
    "s": "http://schema.org/",
    "tdm": "http://www.toreador-project.eu/TDM/"
  },
  "tdm:targetDataSources": [
    ""
  ],
  "tdm:anagraphic": {
    "tdm:firstname": "",
    "tdm:lastname": "",
    "tdm:email": "",
    "tdm:affiliation": "",
    "tdm:role": "",
    "tdm:project_title": "",
    "tdm:project_description": ""
  },
  "tdm:representation": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/representation",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Representation",
    "tdm:incorporates": [{
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Format",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }, {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  },
  "tdm:preparation": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/preparation",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Preparation",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Knowledge Base Elicitation",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Data Reduction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Expansion and Correction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Transformation",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Cleaning and Integration",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Anonymization",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Techniques",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  },
  "tdm:analytics": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/analytics",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Analytics",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Analytics Aim",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Task",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Models",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Learning Approach",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Execution Model",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Learning Environment",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Model Update",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Analytics Quality",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "True Positive Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "True Negative Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Positive Prediction Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "False Positive Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Root Mean Square Error",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Mean Absolute Error",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Overall Accuracy",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "K Coefficient",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 0
            }]
          }
        ]
      }
    ]
  },
  "tdm:processing": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/processing",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Processing",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Mode",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Analysis Goal",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Interaction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Performance",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "Data Storage Consistency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Elasticity",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Latency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Availability",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Operational Flexibility",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Security",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 0
            }]
          }
        ]
      }
    ]
  },
  "tdm:display": {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/display",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Display and Reporting",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Data Model",
        "tdm:constraint": "{ }",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Dimensionality",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Cardinality",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Independent Data Type",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Dependent Data Type",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": []
        }]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Operativity",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Interaction",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "User",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Goal",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": []
          }
        ]
      }
    ]
  }
};

	service.display = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/display",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Display and Reporting",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Data Model",
        "tdm:constraint": "{ }",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Dimensionality",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "1D"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "2D"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "nD"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "tree"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "graph"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Cardinality",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Low"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "High"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Independent Data Type",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Interval"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Ordinal"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Nominal"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Ratio"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Dependent Data Type",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Interval"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Ordinal"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Nominal"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Ratio"
          }]
        }]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Operativity",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Interaction",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Filter"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Zoom"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Overview"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Details-on-demand"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Extract"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Projection"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "History"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Link&Brush"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Distortion"
            }]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "User",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Techie"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Lay-User"
            }]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Goal",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Trends"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Distribution"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Relationship"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Comparison"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Cluster/categorize"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Composition"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Geospatial"
            }, {
              "@type": "tdm:Feature",
              "tdm:constraint": "{}",
              "tdm:label": "Order/Rank/Sort"
            }]
          }
        ]
      }
    ]
  };

	service.representation = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/representation",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Representation",
    "tdm:incorporates": [{
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Structured"
              },
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Unstructured"
              },
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Semi-structured"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Tabular"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Document Oriented"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Graph Based"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Relational"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Extended Relational"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Key Value"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "API Based"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Column Oriented"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Synopses"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Format",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "text/csv"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "application/json"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "application/x-sql"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "application/zip"
              }
            ]
          }, {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Categorical/Nominal"
              },
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Categorical/Ordinal"
              },
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Quantitative/discrete"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Quantitative/Continuous"
              }
            ]
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Model Type",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Data Structure",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Structured"
              },
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Unstructured"
              },
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Semi-structured"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Tabular"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Document Oriented"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Graph Based"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Relational"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Extended Relational"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Key Value"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "API Based"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Column Oriented"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Type",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "text/csv"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "application/json"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "application/x-sql"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "application/zip"
              }
            ]
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Source Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Strong Consistency"
              },
              {
                "@type": "tdm:Objective",
                "tdm:constraint": "{}",
                "tdm:label": "Weak Consistency"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Local System"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Distributed System"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Data Stream"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Database"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "File System"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Vertical"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Horizontal"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Functional"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Memory caches"
              }
            ]
          }
        ]
      },
      {
        "@type": "tdm:Feature",
        "tdm:label": "Data Storage Property",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Feature",
            "tdm:label": "Coherence Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Strong Consistency"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Weak Consistency"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Data Management",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Local System"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Distributed System"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Data Stream"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Database"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "File System"
              }
            ]
          },
          {
            "@type": "tdm:Feature",
            "tdm:label": "Partitioning",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Vertical"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Horizontal"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Functional"
              },
              {
                "@type": "tdm:Feature",
                "tdm:constraint": "{}",
                "tdm:label": "Memory caches"
              }
            ]
          }
        ]
      }
    ]
  };

	service.processing = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/processing",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Processing",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Mode",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Analysis Goal",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Real-Time"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Near Real-Time"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Batch"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Interaction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Dynamic"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Multiple"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Single"
          }]
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Performance",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "Data Storage Consistency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Elasticity",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Latency",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Availability",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Operational Flexibility",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Security",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:value": 50
            }]
          }
        ]
      }
    ]
  };

	service.preparation = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/preparation",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Preparation",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Knowledge Base Elicitation",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Data Reduction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data Cube Aggregation"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Dimension reduction"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data discretization"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data selection"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Expansion and Correction",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Remove missing values"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Rule based"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Deterministic imputation"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Basic numeric imputation"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "KNN imputation"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Hot-deck imputation"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Cold-deck imputation"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Entity expansion and alignment"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Record linkage"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Transformation",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Rule-based"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Ordering"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Normalization"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Transposing"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Data Cleaning and Integration",
          "tdm:constraint": "{}",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data Type Conversion"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data Format Conversion"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data Encoding Conversion"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Scale Conversion"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data Granularity Conversion"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data Identifier Conversion"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Terminology Conversion"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Data Deduplication"
          }, {
            "@type": "tdm:Objective",
            "tdm:constraint": "{}",
            "tdm:label": "Outlier Identification"
          }]
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Anonymization",
        "tdm:constraint": "{}",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Model",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:label": "Privacy Preserving Data Publishing"
            }, {
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:label": "Privacy Preserving Data Mining"
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Anonymization Techniques",
            "tdm:constraint": "{}",
            "tdm:visualisationType": "Option",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:label": "K-anonimity family"
            }, {
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:label": "Differential privacy"
            }, {
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:label": "Hashing"
            }, {
              "@type": "tdm:Objective",
              "tdm:constraint": "{}",
              "tdm:label": "Obfuscation"
            }]
          }
        ]
      }
    ]
  };

	service.analytics = {
    "@id": "http://www.toreador-project.eu/TDM/project-spec/analytics",
    "@context": {
      "s": "http://schema.org/",
      "tdm": "http://www.toreador-project.eu/TDM/"
    },
    "@type": "tdm:Area",
    "tdm:label": "Data Analytics",
    "tdm:incorporates": [{
        "@type": "tdm:Goal",
        "tdm:label": "Analytics Aim",
        "tdm:incorporates": [{
          "@type": "tdm:Indicator",
          "tdm:label": "Task",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Link Prediction"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Regression"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Binary Classification"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Multi-class classification"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Structured output classification"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Subgroup Discovery"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Anomaly/Fault detection"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Flat Clustering"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Hierarchical Clustering"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Crisp Clustering"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Fuzzy Clustering"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Sequence Discovery"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Time Series Analysis"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Association Rules"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Root Cause Analysis"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Models",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Descriptive"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Prescriptive"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Predictive"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Diagnostic"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Learning Approach",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Supervised"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Unsupervised"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Semi-supervised"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Execution Model",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Incremental"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Time Window"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Synopsis-based"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Single Process"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Learning Environment",
          "tdm:visualisationType": "Option",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Stationary"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Non Stationary"
          }]
        }, {
          "@type": "tdm:Indicator",
          "tdm:label": "Model Update",
          "tdm:visualisationType": "Checkbox",
          "tdm:incorporates": [{
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Metric-based"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Density-based"
          }, {
            "@type": "tdm:Objective",
      "tdm:constraint": "{}",
            "tdm:label": "Overlapping"
          }]
        }]
      },
      {
        "@type": "tdm:Goal",
        "tdm:label": "Analytics Quality",
        "tdm:incorporates": [{
            "@type": "tdm:Indicator",
            "tdm:label": "True Positive Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "True Negative Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Positive Prediction Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "False Positive Rate",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Root Mean Square Error",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Mean Absolute Error",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "Overall Accuracy",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          },
          {
            "@type": "tdm:Indicator",
            "tdm:label": "K Coefficient",
            "tdm:visualisationType": "Slider",
            "tdm:incorporates": [{
              "@type": "tdm:Objective",
              "tdm:value": 50
            }]
          }
        ]
      }
    ]
  };

return service;
}]);