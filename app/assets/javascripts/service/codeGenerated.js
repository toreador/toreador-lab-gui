angular
	.module('formApp')
	.service('CodeGeneratedService',
		['$http','$rootScope', '$state', function CodeGeneratedService($http, $rootScope, $state) {
			var service = {};

			service.getCode = function(projectID){
				return $http.get(
      				Routes.codes_get_path(projectID, {format: 'json'})).then(function(response){
      					return response.data;
    			});
			};

			service.saveCode = function(id, code, platform, deployment){
				return $http.post(Routes.codes_save_path({format: "json"}),{"id": id,"code":code, "platform": platform, "deployment": deployment}).then(function(response){
					return response.data;
				});
			};

			service.runCode = function(projectID){
				return $http.get(
      				Routes.projects_path(projectID, {format: 'json'})).then(function(response){
      					return response.data;
    			});
			};

			service.stopCode = function(projectID){
				return $http.get(
      				Routes.projects_path(projectID, {format: 'json'})).then(function(response){
      					return response.data;
    			});
			};

			service.deleteCode = function(projectID){
				return $http.delete(Routes.codes_destroy_path(projectID,{format: "json"})).then(function(response){
					return response.data;
				});
			};

			service.cloneCode = function(idClone, projectName){
				return $http.post(Routes.codes_clone_path({format: "json"}),{"clone_id": idClone,"name":projectName}).then(function(response){
					return response.data;
				});
			};


			return service;
		}]);