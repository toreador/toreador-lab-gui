angular
	.module('formApp')
	.service('UserService',
		['$http', '$auth','$rootScope', '$state', '$localStorage', function($http, $auth, $rootScope, $state, $localStorage){
			var service = {
				
			};

            service.logout = function(){
                return $auth.signOut()
                    .then(function(resp) {
                        $rootScope.user = null;
                        $localStorage.$reset();
                        $state.go('authentication.login');
                        // TODO: Mettere notifica
                    })
                    .catch(function(resp) {
                        // handle error response
                        // TODO: Mettere notifica qualcosa è andato storto 
                    });
            };


            service.goToProjects = function(){
                $state.go('main_projects');
            };

            service.goToTemplates = function(){
                $state.go('templates');
            };
			
			$rootScope.$on("auth:invalid", function(ev, user) {
    			console.log('Invalid User');
    			$state.go('authentication.login');
    		});
			
			$rootScope.$on("auth:login-success", function(ev, user) {
    			console.log('Logged In');
    			console.log("User: " + user.email);
    			service.user = user;
                $rootScope.user = user;
    			$state.go('main_projects');
    		});

            $rootScope.$on("auth:validation-success", function(ev, user) {
                console.log('Authenticated User');
                //debugger;
                $rootScope.user = user;
                service.user = user;
            });

            $rootScope.$on("auth:validation-error", function(ev, user) {
                console.log('Unauthenticated User');
                //debugger;
                $state.go('authentication.login');
            });

    		$rootScope.$on("auth:login-error", function(ev, reason) {
                debugger;
    			console.log('Cannot login');
    			console.log('auth failed because', reason.errors[0]);
    			$state.go('authentication.login');
    		});

            $rootScope.$on("auth:session-expired", function(ev, reason) {
                console.log('Cannot login');
                console.log('auth failed because', reason.errors[0]);
                $state.go('authentication.login');
            });

			return service;

}]);