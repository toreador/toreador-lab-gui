angular
	.module('formApp')
	.service('MainProjectsService',
		['$http','$rootScope', '$state', function MainProjectsService($http, $rootScope, $state) {
			var service = {};

			service.getProjects = function(){
				return $http.get(
      				Routes.main_projects_path({format: 'json'})).then(function(response){
      					return response.data;
    			}, function(response){
      
      				$rootScope.addAlert('danger', 'error', response.data.message);
      
      				return []; 
    			});
			};

			service.deleteProject = function(projectID){
				return $http.delete(Routes.main_projects_destroy_path(projectID,{format: "json"})).then(function(response){
					$rootScope.addAlert('success', 'success', 'The project is deleted');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', response.data.message);
      
      				return {}; 
    			});
			};

			service.createProject = function(projectName, description){
				return $http.post(Routes.main_projects_new_path({format: "json"}),{"name":projectName, "description": description}).then(function(response){
					$rootScope.addAlert('success', 'success', 'The project is created');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', response.data.message);
      
      				return {}; 
    			});
			};

			service.cloneProject = function(idClone, projectName, projectDescription){
				return $http.post(Routes.main_projects_clone_path({format: "json"}),{"clone_id": idClone,"name":projectName, "description": projectDescription}).then(function(response){
					$rootScope.addAlert('success', 'success', 'The project is cloned');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', response.data.message);
      
      				return {}; 
    			});
			};

			service.getProject = function(projectID){
				return $http.get(
      				Routes.projects_get_path(projectID, {format: 'json'})).then(function(response){
      					return response.data;
    			}, function(response){
      
      				$rootScope.addAlert('danger', 'error', response.data.message);
      
      				return {}; 
    			});
			};


			return service;
		}]);