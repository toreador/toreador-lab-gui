angular
	.module('formApp')
	.service('TemplatesService',
		['$http','$rootScope', '$state', function TemplatesService($http, $rootScope, $state) {
			var service = {};

			service.getTemplates = function(){
				return $http.get(
      				Routes.templates_path({format: 'json'})).then(function(response){
      					return response.data;
    			}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'Cannot get the templates due to an error in the platform');
      
      				return true; 
    			});
			};

			service.deleteTemplate = function(templateID){
				return $http.delete(Routes.templates_destroy_path(templateID,{format: "json"})).then(function(response){
					$rootScope.addAlert('success', 'success', 'The template is deleted successfully');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'Cannot delete the template due to an error in the platform');
      
      				return true; 
    			});
			};

			return service;
		}]);