angular
	.module('formApp')
	.service('ProjectsService',
		['$http','$rootScope', '$state', function ProjectsService($http, $rootScope, $state) {
			var service = {};

			service.getProjects = function(projectID){
				return $http.get(
      				Routes.projects_path(projectID, {format: 'json'})).then(function(response){
      					return response.data;
    			}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'Cannot reach the platform');
      
      				return true; 
    			});
			};

			service.deleteProject = function(projectID){
				return $http.delete(Routes.projects_destroy_path(projectID,{format: "json"})).then(function(response){
					$rootScope.addAlert('success', 'success', 'The subproject is deleted');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The delete action was impossible to perform');
      
      				return true; 
    			});
			};

			service.createProject = function(projectName, declarativeName, flowType, serviceFlowType, main_project_id){
				debugger;
				return $http.post(Routes.projects_new_path({format: "json"}),{"name":projectName, "declarative": declarativeName, "flow_type": flowType, "service_flow_type": serviceFlowType,"id": main_project_id}).then(function(response){
					$rootScope.addAlert('success', 'success', 'The subproject is created');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'Cannot create a project due to an error in the platform');
      
      				return true; 
    			});
			};

			service.cloneProject = function(idClone, projectName){
				return $http.post(Routes.projects_clone_path({format: "json"}),{"clone_id": idClone,"name":projectName}).then(function(response){
					$rootScope.addAlert('success', 'success', 'The subproject is cloned');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'Cannot clone the project due to an error in the platform');
      
      				return true; 
    			});
			};

			service.getProject = function(projectID){
				return $http.get(
      				Routes.projects_get_path(projectID, {format: 'json'})).then(function(response){
      					return response.data;
    			}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The save action was impossible to perform');
      
      				return true; 
    			});
			};

			service.addSubconf = function(projectID, confName){
				return $http.post(Routes.declaratives_new_path({format: 'json'}),{"id":projectID, "name":confName}).then(function(response){
      					$rootScope.addAlert('success', 'success', 'Subconfiguration added');
      					return response.data;
    			}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'Cannot add the subconfiguration because the platform respondend with an error');
      
      				return true; 
    			});
			};

			service.deleteSubconf = function(subConfID){
				return $http.delete(Routes.declaratives_destroy_path(subConfID,{format: 'json'})).then(function(response){
      					$rootScope.addAlert('success', 'success', 'The subproject is deleted');
      					return response.data;
    			}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The delete action was impossible to perform');
      
      				return true; 
    			});
			};

			service.saveWorkflow = function(id, workflow){
				var graph = {"id": id, "graph": workflow};
				return $http.post(Routes.projects_save_workflow_path({format: 'json'}), graph).then(function(response){
					$rootScope.addAlert('success', 'success', 'The workflow is saved');
					debugger;
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The save action was impossible to perform');
      
      				return true; 
    			})
			};

			service.getWorkflow = function(id){
				return $http.get(Routes.projects_get_workflow_path(id, {format: 'json'})).then(function(response){
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'Cannot retrieve the workflow of the project');
      
      				return true; 
    			})
			};

			service.saveOwls = function(id, owls){
				var owlsToSave = {"id": id, "owls": owls};
				return $http.post(Routes.projects_save_owls_path({format: 'json'}), owlsToSave).then(function(response){
					$rootScope.addAlert('success', 'success', 'The OWL-S composition is saved');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The save action was impossible to perform');
      
      				return true; 
    			})
			};

			service.saveOozie = function(id, oozie){
				var oozieToSave = {"id": id, "oozie": oozie};
				return $http.post(Routes.projects_save_oozie_path({format: 'json'}), oozieToSave).then(function(response){
					$rootScope.addAlert('success', 'success', 'The subproject is deleted');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The save action was impossible to perform');
      
      				return true; 
    			})
			};

			service.saveForm = function(id, form){
				var declarative = {"id": id, "declarative": form}
				return $http.post(Routes.projects_save_declarative_path({format: 'json'}), declarative).then(function(response){
					$rootScope.addAlert('success', 'success', 'The model is saved');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The save action was impossible to perform');
      
      				return true; 
    			})
			};

			service.saveConstraint = function(id, constraint){
				var constraint = {"id": id, "constraint": constraint};
				return $http.post(Routes.projects_save_constraint_path({format: 'json'}), constraint).then(function(response){
					$rootScope.addAlert('success', 'success', 'The constraints are saved');
					return response.data;
				}, function(response){
      
      				$rootScope.addAlert('danger', 'error', 'The saving action of the constraint was impossible to perform');
      
      				return true; 
    			})

			}

			return service;
		}]);