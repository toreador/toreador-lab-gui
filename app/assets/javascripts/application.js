// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require vendor/jquery/jquery

// JAVASCRIPT INSERITI DA ROBERTO
//= require vendor/jquery-browser-mobile/jquery.browser.mobile
//= require vendor/popper/umd/popper.min
//= require vendor/bootstrap/js/bootstrap
//= require vendor/bootstrap-datepicker/js/bootstrap-datepicker
//= require vendor/common/common
//= require vendor/nanoscroller/nanoscroller
//= require vendor/magnific-popup/jquery.magnific-popup
//= require vendor/jquery-placeholder/jquery-placeholder
//= require vendor/modernizr/modernizr
//= require js/theme
//= require js/theme.init
//= require js/theme.admin.extension


// FINE JAVASCRIPT INSERITI DA ROBERTO

//= require js-routes
//= require tether
//= require jquery_ujs
// require bootstrap-sprockets
//= require angular/angular
//= require angular-rails-templates
//= require angular-tooltips
//= require ng-dialog
//= require angular-animate
//= require angularjs-slider
//= require angular-ui-router
//= require checklist-model
//= require angular-growl-notifications
//= require angular-pretty-xml
//= require angular-cookie
//= require ng-token-auth
//= require ngstorage
//= require cytoscape
//= require ui-ace 
//= require lodash
//= require cytoscape-edgehandles
//= require ngCytoscape
//= require jsonq
//= require angular-initial-value


//= require_tree .