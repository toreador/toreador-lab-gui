angular
	.module('formApp')
	.controller('authenticationController',
		['$scope', '$auth', '$http', '$state', 'UserService', function($scope, $auth, $http, $state, UserService){
			//$scope.loginForm = {};
			$scope.state = $state.current.name;

			$scope.goToReset = function(){
				$state.go('authentication.reset');
			};

			$scope.goToRegistration = function(){
				$state.go('authentication.registration');
			};

			$scope.submitLogin = function(loginForm){
				$auth.submitLogin(loginForm)
        			.then(function(resp) {
          				// handle success response
          				console.log(resp);
                 // $state.go('authentication.registration');
        		})
        			.catch(function(resp) {
          				// handle error response
          				console.log(resp);
        		});
			};


			$scope.requestPasswordReset = function(loginForm) {
				loginForm.redirect_url = "http://localhost:3000/esempio";
      			$auth.requestPasswordReset(loginForm)
        			.then(function(resp) {
         			// handle success response
         			console.log(resp);
        		})
        			.catch(function(resp) {
          			// handle error response
          			console.log(resp);
        		});
    		};

    		$scope.submitRegistration = function(registrationForm) {
      			$auth.submitRegistration(registrationForm)
        			.then(function(resp) {
          			// handle success response
          			console.log(resp);
        		})
        			.catch(function(resp) {
          			// handle error response
          			console.log(resp);
        	});
    };

}]);