angular
	.module('formApp')
	.controller('templatesController',
		['$scope', '$auth', '$http', '$state', 'PagerService', 'templates','UserService', 'ngDialog', 'TemplatesService', function($scope, $auth, $http, $state, PagerService, templates, UserService, ngDialog, TemplatesService){

            $scope.configurations = ["1","2","3"];
			var vm = this;
 			
 			$scope.templateSelected = null;
 			$scope.templates = templates.templates;
    		//$scope.dummyItems = _.range(1, 151); // dummy array of items to be paged
    		$scope.pager = {};
    		$scope.setPage = setPage;
    		
    		$scope.currState = $state;
 			
    		initController();
 			
    		function initController() {
        		// initialize to page 1
        		$scope.setPage(1);
    		}
 
    		function setPage(page) {
        		if (page < 1 || page > $scope.pager.totalPages) {
            		return;
        	}
 
        	// get pager object from service
        	$scope.pager = PagerService.GetPager($scope.templates.length, page);
 
        	// get current page of items
        	$scope.items = $scope.templates.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);

        	$scope.deleteTemplate = function(templateID){
        		$scope.templateSelected = templateID;
        		//console.log("Delete Project " + projectID);
        		ngDialog.openConfirm({ 
            		template: 'removeTemplate', 
            		className: 'ngdialog-theme-default',
            		scope: $scope 
        		}).then(function (success) {
          			var result = TemplatesService.deleteTemplate($scope.templateSelected)
          			.then(
          				function(deletion){
          					$state.go($state.current, {}, {reload: true});
          				}
          			);
        		}, function (error) {
            		//TODO: MESSAGGIO D'ERRORE
        		});
        	};
    }
}]);