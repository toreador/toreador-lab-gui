angular
	.module('formApp')
	.controller('mainProjectsController',
		['$scope', '$auth', '$http', '$state', 'PagerService', 'projects','UserService', 'ngDialog', 'MainProjectsService', '$rootScope', function($scope, $auth, $http, $state, PagerService, projects, UserService, ngDialog, MainProjectsService, $rootScope){

            $scope.configurations = ["1","2","3"];
			var vm = this;
 			
 			$scope.projectSelected = null;
 			$scope.projects = projects.projects;
    		//$scope.dummyItems = _.range(1, 151); // dummy array of items to be paged
    		$scope.pager = {};
    		$scope.setPage = setPage;
    		$scope.newProjectName = {};
    		$scope.currState = $state;
 			
    		initController();
 			
    		function initController() {
        		// initialize to page 1
        		$scope.setPage(1);
    		}
 
    		function setPage(page) {
        		if (page < 1 || page > $scope.pager.totalPages) {
            		return;
        	}
 
        	// get pager object from service
        	$scope.pager = PagerService.GetPager($scope.projects.length, page);
 
        	// get current page of items
        	$scope.items = $scope.projects.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);

        	$scope.openProject = function(projectID){
        		//console.log("Open Project " + projectID);
        		//TODO: Vado nello stato della form usando l'id
        		//$state.go('form.anagraphic',{id: projectID});
                // TODO: Vado nei progetti che fanno parte di quel Main Project
                $state.go('projects',{id: projectID});
        	};

            $scope.showResults = function(projectID){
                $state.go('form.visualization',{id: projectID});
            };

        	$scope.deleteProject = function(projectID){
        		$scope.projectSelected = projectID;
        		//console.log("Delete Project " + projectID);
        		ngDialog.openConfirm({ 
            		template: 'removeProject', 
            		className: 'ngdialog-theme-default',
            		scope: $scope 
        		}).then(function (success) {
          			var result = MainProjectsService.deleteProject($scope.projectSelected)
          			.then(
          				function(deletion){
          					$state.reload();
          				}
          			);
        		}, function (error) {
            		//TODO: MESSAGGIO D'ERRORE
        		});
        	};

        	$scope.createProject = function(event){
        		
        			ngDialog.openConfirm({ 
            			template: 'createProject', 
            			className: 'ngdialog-theme-default',
            			scope: $scope 
        			}).then(function (success) {
        				if($scope.newProjectName.name !== undefined 
        					&& $scope.newProjectName.description !== undefined 
        					&& $scope.newProjectName.description !== '' 
        					&& $scope.newProjectName.name !== ''){

          					var result = MainProjectsService.createProject($scope.newProjectName.name, $scope.newProjectName.description)
          					.then(
          						function(project){
          							$state.reload();
          						}
          					);
          				} else {
                            $rootScope.addAlert('danger', 'danger', 'All the fields of the form is mandatory');
                        }
        			}, function (error) {
            			//TODO: MESSAGGIO D'ERRORE
            			console.log(error);
        			});        		
        	};

            $scope.cloneProject = function(id){
                $scope.projectSelected = id;

                ngDialog.openConfirm({ 
                        template: 'cloneProject', 
                        className: 'ngdialog-theme-default',
                        scope: $scope 
                    }).then(function (success) {
                        if($scope.newProjectName.name !== undefined  
                            && $scope.newProjectName.name !== ''){

                            var result = MainProjectsService.cloneProject(id, $scope.newProjectName.name, $scope.newProjectName.description)
                            .then(
                                function(project){
                                    //$state.go('main_projects');
                                    $state.reload();
                                }
                            );
                        }
                    }, function (error) {
                        //TODO: MESSAGGIO D'ERRORE
                        console.log(error);
                    }); 
            };
    }
}]);