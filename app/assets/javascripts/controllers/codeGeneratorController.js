angular
	.module('formApp')
	.controller('codeGeneratedController',
		['$window', '$scope', '$auth', '$http', '$state', 'projects','UserService', 'ngDialog', 'CodeGeneratedService', function($window, $scope, $auth, $http, $state, projects, UserService, ngDialog, CodeGeneratedService){

 			$scope.codeProject = projects.code_project;
    		
    		$scope.currState = $state;
 			
    		$scope.downloadResults = function($event){
                $scope.openTab($scope.codeProject.result);
            };

            $scope.downloadCode = function($event){
                $scope.openTab($scope.codeProject.code_generated);
            };

            $scope.downloadLogs = function($event){
                $scope.openTab($scope.codeProject.log);
            };

            $scope.saveCode = function($event){
                debugger;
                CodeGeneratedService.saveCode($scope.codeProject.id, $scope.codeProject.code, $scope.codeProject.platform, $scope.codeProject.deployment);
            };
 
            $scope.openTab = function(url){
                $window.open(url, '_blank');
            };
    		
}]);