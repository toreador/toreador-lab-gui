angular
	.module('formApp')
	.controller('projectsController',
		['$scope', '$auth', '$http', '$state', 'PagerService', 'projects','UserService', 'ngDialog', 'ProjectsService', 'CodeGeneratedService', '$rootScope', function($scope, $auth, $http, $state, PagerService, projects, UserService, ngDialog, ProjectsService, CodeGeneratedService, $rootScope){

            $scope.configurations = ["1","2","3"];
			var vm = this;
 			
 			$scope.projectSelected = null;
 			$scope.projects = projects
            $scope.nameMain = "";
    		//$scope.dummyItems = _.range(1, 151); // dummy array of items to be paged
    		$scope.pager = {};
    		$scope.setPage = setPage;
    		$scope.newProjectName = {};
    		$scope.currState = $state;
 			
    		initController();
 			
    		function initController() {
        		// initialize to page 1
        		$scope.setPage(1);
                if($scope.projects.length > 0){
                    $scope.nameMain = $scope.projects[0].project_main;
                }
    		}
 
    		function setPage(page) {
        		if (page < 1 || page > $scope.pager.totalPages) {
            		return;
        	}

            $scope.isNotValid = function(){
                if($scope.newProjectName.declarative === undefined || $scope.newProjectName.declarative.length < 5 || $scope.newProjectName.name === undefined || $scope.newProjectName.name.length < 5){
                    return true
                }
                return false
            };
 
        	// get pager object from service
        	$scope.pager = PagerService.GetPager($scope.projects.length, page);
 
        	// get current page of items
        	$scope.items = $scope.projects.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);

        	$scope.openProject = function(projectID){
        		console.log("Open Project " + projectID);
        		//TODO: Vado nello stato della form usando l'id
        		$state.go('form.anagraphic',{id: projectID});
        	};

            $scope.showResults = function(projectID){
                $state.go('form.visualization',{id: projectID});
            };

        	$scope.deleteProject = function(projectID){
        		$scope.projectSelected = projectID;
        		//console.log("Delete Project " + projectID);
        		ngDialog.openConfirm({ 
            		template: 'removeProject', 
            		className: 'ngdialog-theme-default',
            		scope: $scope 
        		}).then(function (success) {
          			var result = ProjectsService.deleteProject($scope.projectSelected)
          			.then(
          				function(deletion){
          					$state.go($state.current, {}, {reload: true});
          				}
          			);
        		}, function (error) {
            		//TODO: MESSAGGIO D'ERRORE
        		});
        	};

        	$scope.createProject = function(event){
        		
        			ngDialog.openConfirm({ 
            			template: 'createProject', 
            			className: 'ngdialog-theme-default',
            			scope: $scope 
        			}).then(function (success) {
        				if($scope.newProjectName.name !== undefined 
        					&& $scope.newProjectName.declarative !== undefined 
        					&& $scope.newProjectName.declarative !== '' 
        					&& $scope.newProjectName.name !== ''){

          					var result = ProjectsService.createProject($scope.newProjectName.name, $scope.newProjectName.declarative, $scope.newProjectName.flowType, $scope.newProjectName.serviceFlowType, $scope.currState.params.id)
          					.then(
          						function(project){
                                    $state.go("form.anagraphic", {id: project.id});
                                    /*
                                    if($scope.newProjectName.flowType !== 'code_generated') {
                                        $state.go("form.anagraphic", {id: project.id});
                                    } else {
                                        $state.go("code_generated", {id: project.id});
                                    }
          							*/
          						}
          					);
          				} else {
                            $rootScope.addAlert('danger', 'danger', 'All the fields of the form is mandatory');
                        }
        			}, function (error) {
            			//TODO: MESSAGGIO D'ERRORE
            			console.log(error);
        			});        		
        	};

            $scope.cloneProject = function(id){
                $scope.projectSelected = id;

                ngDialog.openConfirm({ 
                        template: 'cloneProject', 
                        className: 'ngdialog-theme-default',
                        scope: $scope 
                    }).then(function (success) {
                        if($scope.newProjectName.name !== undefined  
                            && $scope.newProjectName.name !== ''){

                            var result = ProjectsService.cloneProject(id, $scope.newProjectName.name)
                            .then(
                                function(project){
                                    $state.go("form.anagraphic", {id: project.id});
                                }
                            );
                        }
                    }, function (error) {
                        //TODO: MESSAGGIO D'ERRORE
                        console.log(error);
                    }); 
            };

            $scope.cloneProjectCode = function(id){
                $scope.projectSelected = id;

                ngDialog.openConfirm({ 
                        template: 'cloneProject', 
                        className: 'ngdialog-theme-default',
                        scope: $scope 
                    }).then(function (success) {
                        if($scope.newProjectName.name !== undefined  
                            && $scope.newProjectName.name !== ''){

                            var result = CodeGeneratedService.cloneCode(id, $scope.newProjectName.name)
                            .then(
                                function(project){
                                    $state.go($state.current, {}, {reload: true});
                                }
                            );
                        }
                    }, function (error) {
                        //TODO: MESSAGGIO D'ERRORE
                        console.log(error);
                    }); 
            };

            $scope.openProjectCode = function(projectID){
                console.log("Open Code Project " + projectID);
                //TODO: Vado nello stato della form usando l'id
                $state.go('code_generated',{id: projectID});
            };

            $scope.deleteProjectCode = function(projectID){
                $scope.projectSelected = projectID;
                //console.log("Delete Project " + projectID);
                ngDialog.openConfirm({ 
                    template: 'removeProject', 
                    className: 'ngdialog-theme-default',
                    scope: $scope 
                }).then(function (success) {
                    var result = CodeGeneratedService.deleteCode($scope.projectSelected)
                    .then(
                        function(deletion){
                            $state.go($state.current, {}, {reload: true});
                        }
                    );
                }, function (error) {
                    //TODO: MESSAGGIO D'ERRORE
                });
            };
    }
}]);