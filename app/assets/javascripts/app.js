
// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
angular.module('formApp', [
  'templates',
  'ngAnimate', 
  'ui.router', 
  '720kb.tooltips', 
  'ngDialog', 
  'rzModule', 
  'checklist-model',
  'growlNotifications',
  'ngStorage',
  'ngCytoscape',
  'prettyXml',
  'ng-token-auth',
  'initialValue',
  'ui.ace'
  ]).run(['$rootScope','UserService','$transitions', function($rootScope, UserService, $transitions){
    /*
    $rootScope.$on("auth:invalid", function(ev, user) {
          console.log('Invalid User');
    });
    */

    $rootScope.alertMessage = null;
    $rootScope.showAlert = false;

    $rootScope.removeAlert = function(){
      $rootScope.showAlert = false;
      $rootScope.alertMessage = null;
    };

    $rootScope.addAlert = function(classAlert, alertType, message){
      $rootScope.removeAlert();
      $rootScope.alertMessage = {'class': classAlert, 'alertType': alertType, 'message': message};
      $rootScope.showAlert = true;
    };

    $rootScope.logout = function(){
      UserService.logout();
    };

    $rootScope.Utils = {
     keys : Object.keys
    };

    $rootScope.showConfigurations = function(){
      UserService.goToProjects();
    };

    $rootScope.showTemplates = function(){
      UserService.goToTemplates();
    };

    /*
    $rootScope.events = 
      [
        {
          "name": "Esempio",
          "message": "Messaggio di esempio",
          "type": "success"
        },
        {
          "name": "Esempio2",
          "message": "Messaggio di esempio2",
          "type": "error"
        }
      ];
    */
    $transitions.onStart({ to: 'form.visualization' }, function(trans) {
      
      var projectService = trans.injector().get('ProjectsService');
      var project = projectService.getProject(trans.params().id)
      console.log("Sono pronto per la visualizzazione del progetto: ");
      console.log(project);
      if(project.done === false || project.visualization_url === ''){
        return trans.router.stateService.target('form.workflow');
      }
      //console.log($scope.done);
      /*
      var auth = trans.injector().get('AuthService');
      if (!auth.isAuthenticated()) {
        // User isn't authenticated. Redirect to a new Target State
        return trans.router.stateService.target('login');
      }
      */
    });


  }]).config(['$stateProvider', '$urlRouterProvider', '$authProvider', function($stateProvider, $urlRouterProvider, $authProvider) {
    $authProvider.configure({
       /* apiUrl: 'http://localhost:3000', */
       apiUrl: 'http://alphaplus.di.unimi.it/mytoreador', 
      validateOnPageLoad: true
    });

    $stateProvider
    
        // route to show our basic form (/form)
        .state('form', {
            url: '/form/:id',
            templateUrl: 'form/_form.html',
            params: {temp: 'index'},
            controller: 'formController',
            resolve: {
              services: ['DataService', function(DataService){
                return {};
              }],
              auth : ['$auth', 'UserService', function($auth, UserService){
                return $auth.validateUser();
              }],
              project : ['$stateParams', 'ProjectsService', function($stateParams, ProjectsService){
                return ProjectsService.getProject($stateParams.id);
              }],
              owls : ['DataService', function(DataService){
                return {};//DataService.getWorkflow($localStorage.formData);;
              }],
              oozie : ['DataService', function(DataService){
                return {};//DataService.getWorkflow($localStorage.formData);;
              }],
            }
        })
        
        // nested states 
        // each of these sections will have their own view
        // url will be nested (/form/analytics)
        .state('form.analytics', {
            url: '/analytics',
            params: {temp: 'analytics'},
            templateUrl: 'form/_form-analytics.html'
        })
        
        // url will be /form/display
        .state('form.display', {
            url: '/display',
            params: {temp: 'display'},
            templateUrl: 'form/_form-display.html'
        })

        // url will be /form/preparation
        .state('form.preparation', {
            url: '/preparation',
            params: {temp: 'preparation'},
            templateUrl: 'form/_form-preparation.html'
        })

        // url will be /form/processing
        .state('form.processing', {
            url: '/processing',
            params: {temp: 'processing'},
            templateUrl: 'form/_form-processing.html'
        })

        // url will be /form/representation
        .state('form.representation', {
            url: '/representation',
            params: {temp: 'representation'},
            templateUrl: 'form/_form-representation.html'
        })

        // url will be /form/anagraphic
        .state('form.anagraphic', {
            url: '/anagraphic/',
            params: {temp: 'anagraphic'},
            templateUrl: 'form/_form-anagraphic.html'
        })
        // url will be /form/services
        .state('form.services',{
          url:'/services',
          params: {temp: 'serviceSelection', myForm: null},
          templateUrl: 'form/_form-service-selection.html',
          controller: 'formController',
          resolve: {
            services : ['DataService', '$stateParams', '$localStorage', function(DataService, $stateParams, $localStorage) {
                console.log($stateParams);
                //return DataService.getServices($stateParams.myForm.formData);
                return DataService.getServices($localStorage.formData);
            }]
          }
        })
        // url will be /form/workflow
        .state('form.workflow',{
          url:'/workflow',
          params: {temp: 'workflow', myForm: null},
          templateUrl: 'form/_form-workflow.html',
          controller: 'formController',
          resolve: {
            oozie : ['DataService', '$stateParams', '$localStorage', function(DataService, $stateParams, $localStorage) {
                //console.log($stateParams);
                return DataService.getWorkflow($stateParams.id);
            }]
          }
        })
        // url will be /form/owls
        .state('form.owls',{
          url:'/owls',
          params: {temp: 'owls', myForm: null},
          templateUrl: 'form/_form-owls.html',
          controller: 'formController',
          resolve: {
            owls : ['DataService', '$stateParams', '$localStorage', function(DataService, $stateParams, $localStorage) {
                //console.log($stateParams);
                //console.log(JSON.stringify($localStorage.graph));
                return DataService.getOWLSWorkflow($localStorage.graph);
            }]
          }
        })
        // url will be /form/visualization
        .state('form.visualization',{
          url:'/visualization',
          params: {temp: 'visualization', myForm: null},
          templateUrl: 'form/_form-visualization.html',
          controller: 'formController'
          
        })
        .state('form.code_generation',{
          url:'/code_generation',
          params: {temp: 'code_generation', myForm: null},
          templateUrl: 'code_generated/_show_code_constraint.html',
          controller: 'formController'
          
        })
        .state('authentication', {
            url: '/authentication',
            template: '<div ui-view="form" />',
            templateUrl: 'form/_form.html',
            params: {temp: 'index'},
            controller: 'authenticationController',
        })
        // url will be /authentication/login
        .state('authentication.login', {
            url: '/login',
            views: {
              // targets uiview name='content' created by 'parent' state
              'form@authentication': {
                templateUrl: 'authentication/_login.html'
              }
            },
            params: {temp: 'login'},
            
        })
        // url will be /authentication/reset
        .state('authentication.reset', {
            url: '/reset',
            views: {
              // targets uiview name='content' created by 'parent' state
              'form@authentication': {
                templateUrl: 'authentication/_password-reset.html'
              }
            },
            params: {temp: 'reset'},
            
        })// url will be /authentication/registration
        .state('authentication.registration', {
            url: '/registration',
            views: {
              // targets uiview name='content' created by 'parent' state
              'form@authentication': {
                templateUrl: 'authentication/_registration.html'
              }
            },
            params: {temp: 'registration'},
            
        })
        .state('templates',{
          url:'/templates',
          params: {temp: 'templates', myForm: null},
          templateUrl: 'templ/_show_templates.html',
          controller: 'templatesController',
          resolve: {
            templates : ['TemplatesService', '$stateParams', '$localStorage', function(TemplatesService, $stateParams, $localStorage) {
                return TemplatesService.getTemplates();
            }],
            auth : ['$auth', 'UserService', function($auth, UserService){
              return $auth.validateUser();
            }]
          }
        })// url will be /projects
        .state('projects',{
          url:'/main_project/:id',
          params: {temp: 'projects', myForm: null},
          templateUrl: 'projects/_show_configurations.html',
          controller: 'projectsController',
          resolve: {
            projects : ['ProjectsService', '$stateParams', '$localStorage', function(ProjectsService, $stateParams, $localStorage) {
                $localStorage.$reset();
                return ProjectsService.getProjects($stateParams.id);
            }],
            auth : ['$auth', 'UserService', function($auth, UserService){
              return $auth.validateUser();
            }]
          }
        })// url will be /main_projects
        .state('main_projects',{
          url:'/main_projects',
          params: {temp: 'projects', myForm: null},
          templateUrl: 'main_projects/_show_configurations.html',
          controller: 'mainProjectsController',
          resolve: {
            projects : ['MainProjectsService', '$stateParams', '$localStorage', function(MainProjectsService, $stateParams, $localStorage) {
                $localStorage.$reset();
                return MainProjectsService.getProjects();
            }],
            auth : ['$auth', 'UserService', function($auth, UserService){
              return $auth.validateUser();
            }]
          }
        }).state('code_generated',{
          url:'/code_generated/:id',
          params: {temp: 'code_generated', myForm: null},
          templateUrl: 'code_generated/_show_code.html',
          controller: 'codeGeneratedController',
          resolve: {
            projects : ['CodeGeneratedService', '$stateParams', '$localStorage', function(CodeGeneratedService, $stateParams, $localStorage) {
                return CodeGeneratedService.getCode($stateParams.id);
            }],
            auth : ['$auth', 'UserService', function($auth, UserService){
              return $auth.validateUser();
            }]
          }
        });
       
    // catch all route
    // send users to the form page 
    $urlRouterProvider.otherwise('/main_projects');
}])
.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            element.hover(function(){
                // on mouseenter
                element.tooltip('show');
            }, function(){
                // on mouseleave
                element.tooltip('hide');
            });
        }
    };
})
.directive('loading', ['$http', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (value) {
          if (value) {
            element.removeClass('ng-hide');
          } else {
            element.addClass('ng-hide');
          }
        });
      }
    };
}]).directive('twoway', 
      function($timeout) {
        return {
          scope: true,
          require: '?ngModel',
          link: function(scope, elem, attrs, ngModel) {
            $timeout( function() {
              
              if(ngModel.$modelValue !== undefined && ngModel.$modelValue['tdm:label'] !== undefined && ngModel.$modelValue['tdm:label'] === ngModel.$$attr.value['tdm:label']){
                ngModel.$setViewValue(ngModel.$$attr.value);
                ngModel.$render();
              }
              
            });
          }
        };
      }
    )
.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function link(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  }})
// Direttiva di esempio per gestire il change del radio button
.directive('toreadorChange', function() {
  return { 
    scope: true,
    link:  function link(scope, element, attrs) {
      element.bind('change', function() {
        
        //TODO: Calcolo le interferenze in una variabile di scope utilizzate dall'ng-hide per vedere se un elemento
        // è visibile o meno

        var interference = transformToInterference(scope.formData);
        var dictionary = calculateDictionary(interference, scope.hideDictionary, scope.conflicts, scope.notifications);
        //var elementPath = element.data("form")+'::'+element.data('goal')+'::'+element.data('question')+'::'+element.val();
        //var slider = checkSlider(scope.sliders, elementPath, scope.calculateRandom);
        //scope.formData[element.data("form")][element.data('goal')][element.data('question')+'_'+'constraint'] = slider.floor;
        angular.copy(dictionary, scope.hideDictionary);
        
        scope.$apply();
      });
      
      function checkSlider(sliders, elementPath, calculateRandom){
        if(sliders[elementPath] === undefined){
          var slider = {};
          var max = calculateRandom(3,30);
          var min = calculateRandom(1,3);

          slider.floor = calculateRandom(1,min);
          slider.ceil = calculateRandom(slider.floor + 1,max); 

          sliders[elementPath] = slider;
          return slider;
        } else {
          return sliders[elementPath];
        }
      };

      function transformToInterference(formData){
        var interference = [];
        var goals = ['tdm:analytics', 'tdm:display', 'tdm:preparation', 'tdm:processing', 'tdm:representation'];
        for(index = 0; index < goals.length; index++){
          var goal = goals[index];
          if(formData[goal] !== undefined){
            var area = formData[goal];
            for(var goalIndex in area['tdm:incorporates']){
              var goal = area['tdm:incorporates'][goalIndex];
              
              for(var indicatorIndex in goal['tdm:incorporates']){
                var indicator = goal['tdm:incorporates'][indicatorIndex];
                if(indicator['tdm:visualisationType'] !== 'Slider'){
                  for(var featureIndex in indicator['tdm:incorporates']){
                    
                    var feature = indicator['tdm:incorporates'][featureIndex];
                    var element = area['tdm:label'].toLowerCase()+'::'+goal['tdm:label'].toLowerCase()+'::'+indicator['tdm:label'].toLowerCase()+'::'+feature['tdm:label'].toLowerCase();
                    interference.push(element);
                  }
                }
              }
            }
          }
        }
        return interference;
      };

      function calculateDictionary(interference, dictionary, conflicts, notifications){
        dictionary = {};
        notifications.length = 0;
        if(interference !== undefined && interference.length > 0){
          if(conflicts !== undefined){
            for(var index = 0; index < interference.length; index++){
              if(conflicts[interference[index]] !== undefined){
                for(var indexConflicts = 0; indexConflicts < conflicts[interference[index]].length; indexConflicts++){
                  var el = conflicts[interference[index]][indexConflicts];
                  var elementForNotification = el.split('::');
                  var notification = {};
                  notification.area = elementForNotification[0];
                  notification.goal = elementForNotification[1];
                  notification.indicator = elementForNotification[2];
                  notification.feature = elementForNotification[3];
                  notifications.push(notification);
                  dictionary[el] = true;
                }
              }
            }
          }
        }
        return dictionary;
      };

    }
  }

  
}).filter('getNodes', function () {
  return function (item) {
      var elements = [];
      for(var node in item){
        if(item[node].group == 'nodes'){
          elements.push(item[node].id);
        }

        //console.log(item[node].id)
      }
      
      return elements;
  };
})

// our controller for the form
// =============================================================================
.controller('formController', [
  '$scope', 
  '$stateParams', 
  'ngDialog', 
  '$http', 
  '$state',
  'DataService', 
  'services',
  '$localStorage', 
  'cytoData',
  'auth',
  'UserService',
  'project',
  'owls',
  'oozie',
  'ProjectsService',
  '$sce',
  '$rootScope',
  '$timeout', function(
    $scope, 
    $stateParams, 
    ngDialog, 
    $http, 
    $state, 
    DataService, 
    services, 
    $localStorage,
    cytoData,
    auth,
    UserService,
    project,
    owls,
    oozie,
    ProjectsService,
    $sce,
    $rootScope,
    $timeout) {
    
    $scope.alert = alert.bind(window);
    
    $scope.$storage = $localStorage;

    $scope.id = $stateParams.id;

    $scope.currState = $state;
    $scope.showStatus = false;
    $scope.showConstraintForm = true;

    //CYTOSCOPE
    var ctrl = this;
    $scope.graph = {};
    $scope.showNode = false;
    $scope.showEdge = false;
    $scope.nodeId = "";
    $scope.nodeData = {serviceName: ""};
    $scope.edgeTo = "";
    $scope.edgeFrom = "";
    $scope.edgeId = "";
    $scope.edgeType = false;
    $scope.conditionalEdgeTo = {"id":""};


    $scope.owlsToShow = "";
    $scope.serviceSelected = {};

    $scope.operatorName = {name:""};
    $scope.operator = {};

    $scope.description="";

    $scope.elements = {};


    $scope.templates = [
      {
        "id": 1,
        "name": "SVM Template",
        "description": "Load dataset from ftp and calculate an SVM prediction after cleaning the dataset",
        "structure": {"nodes":[{"data":{"service":{"name":"ftptohdfs","id":"http://www.example.com/toreador/BDMOntologies/Sparkcleanser.owl","description":"Exports a file from an FTP location to HDFS","ontoId":"http://www.example.com/toreador/BDMOntologies/Sparkcleanser.owl","owls":"","parameters":{"hdfsPath":{"id":"http://www.example.com/toreador/BDMOntologies/Sparkcleanser.owl#hdfsPath","description":"HDFS path for input data","mandatory":false,"defaultValue":null,"key":"hdfsPath","type":"http://www.w3.org/2001/XMLSchema#string"},"ftpPath":{"id":"http://www.example.com/toreador/BDMOntologies/Sparkcleanser.owl#ftpPath","description":"the FTP path for the input data","mandatory":false,"defaultValue":null,"key":"ftpPath","type":"http://www.w3.org/2001/XMLSchema#string"}},"serviceName":"FtpSink"},"shape":"ellipse","nodeType":{"type":"SERVICE"},"id":"FtpSink"},"position":{"x":416.25,"y":171},"group":"nodes","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""},{"data":{"service":{"name":"spark-cleanser","id":"http://www.example.com/toreador/BDMOntologies/OpenJDKRuntimeEnvironmentdatacheck.owl","description":"Replaces every occurrency of a regular expression with an empty string.","ontoId":"http://www.example.com/toreador/BDMOntologies/OpenJDKRuntimeEnvironmentdatacheck.owl","owls":"","parameters":{"inputdatapath":{"id":"http://www.example.com/toreador/BDMOntologies/OpenJDKRuntimeEnvironmentdatacheck.owl#inputdatapath","description":"HDFS path to input data","mandatory":false,"defaultValue":null,"key":"inputdatapath","type":"http://www.w3.org/2001/XMLSchema#string"},"outputpath":{"id":"http://www.example.com/toreador/BDMOntologies/OpenJDKRuntimeEnvironmentdatacheck.owl#outputpath","description":"HDFS output path to the cleansed data","mandatory":false,"defaultValue":null,"key":"outputpath","type":"http://www.w3.org/2001/XMLSchema#string"},"stringpattern":{"id":"http://www.example.com/toreador/BDMOntologies/OpenJDKRuntimeEnvironmentdatacheck.owl#stringpattern","description":"String regular expression to cleanse","mandatory":false,"defaultValue":null,"key":"stringpattern","type":"http://www.w3.org/2001/XMLSchema#string"},"delimiter":{"id":"http://www.example.com/toreador/BDMOntologies/OpenJDKRuntimeEnvironmentdatacheck.owl#delimiter","description":"String regular expression to cleanse","mandatory":false,"defaultValue":null,"key":"delimiter","type":"http://www.w3.org/2001/XMLSchema#string"}},"serviceName":"Cleans"},"shape":"ellipse","nodeType":{"type":"SERVICE"},"id":"Cleans"},"position":{"x":277.5,"y":171},"group":"nodes","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""},{"data":{"service":{"name":"spark-svm-predict","id":"http://www.example.com/toreador/BDMOntologies/Sparkkmeans.owl","description":"Predict data with a SVM model in batch mode using Spark","ontoId":"http://www.example.com/toreador/BDMOntologies/spark-svm-predict.owl","owls":"","parameters":{"inputpath":{"id":"http://www.example.com/toreador/BDMOntologies/Sparkkmeans.owl#epsilon","description":"path to read the data","mandatory":true,"defaultValue":null,"key":"inputpath","type":"http://www.w3.org/2001/XMLSchema#string"},"outputpath":{"id":"http://www.example.com/toreador/BDMOntologies/Sparkkmeans.owl#outputpath","description":"path to write the predicted data","mandatory":true,"defaultValue":null,"key":"outputpath","type":"http://www.w3.org/2001/XMLSchema#string"},"delimiter":{"id":"http://www.example.com/toreador/BDMOntologies/Sparkkmeans.owl#delimiter","description":"delimiter symbol of the data","mandatory":false,"defaultValue":null,"key":"delimiter","type":"http://www.w3.org/2001/XMLSchema#string"},"model":{"id":"http://www.example.com/toreador/BDMOntologies/Sparkkmeans.owl#model","description":"path to the model","mandatory":false,"defaultValue":null,"key":"model","type":"http://www.w3.org/2001/XMLSchema#string"}},"serviceName":"SVM"},"shape":"ellipse","nodeType":{"type":"SERVICE"},"id":"SVM"},"position":{"x":138.75,"y":171},"group":"nodes","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""},{"data":{"shape":"rectangle","nodeType":{"type":"START","operatorType":"","conf":{}},"id":"START"},"position":{"x":370,"y":342},"group":"nodes","removed":false,"selected":true,"selectable":true,"locked":false,"grabbable":true,"classes":""},{"data":{"shape":"rectangle","nodeType":{"type":"STOP","operatorType":"","conf":{}},"id":"STOP"},"position":{"x":185,"y":342},"group":"nodes","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""}],"edges":[{"data":{"target":"Cleans","source":"FtpSink","id":"1524835409103"},"position":{},"group":"edges","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""},{"data":{"target":"SVM","source":"Cleans","id":"1524835444841"},"position":{},"group":"edges","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""},{"data":{"target":"FtpSink","source":"START","id":"1524835465596"},"position":{},"group":"edges","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""},{"data":{"target":"STOP","source":"SVM","id":"1524835474680"},"position":{},"group":"edges","removed":false,"selected":false,"selectable":true,"locked":false,"grabbable":true,"classes":""}]}
      }
    ];
    
    init();

    /*
    $scope.configurationsName = $localStorage.configurationsName || ["1"];
    $scope.configurationSelected = $localStorage.configurationSelected || "1";
    $scope.anagraphic = $localStorage.anagraphic || {};
    // we will store all of our form data in this object
    $scope.cleanForm = $scope.cleanForm || DataService.getCleanForm();
    $scope.formData = $localStorage.formData || {};
    //TODO: Modificare perché ci sarà una init che caricherà la conf corretta
    $scope.formData[$scope.configurationSelected] = DataService.cleanForm; 
    
    $scope.hideDictionary = {
      "Prova":{},
      "Prova2":{}
    };
    
    $scope.oldDictionary = {
      "Prova":{},
      "Prova2":{}
    };
    */

    $scope.newSubconfName = {name:''};

    $scope.user = UserService.user;
    
    $scope.foo = services.servicesByArea;

    // we store the route name here to have information about the form to visualize
    $scope.par = $stateParams.temp;

    $scope.showDetails = {};
    $scope.analytics = DataService.getAnalytics();
    $scope.preparation = DataService.getPreparation();
    $scope.processing = DataService.getProcessing();
    $scope.representation = DataService.getRepresentation();
    $scope.display = DataService.getDisplay();
    $scope.egestionDataSources = [''];
    //$scope.mainId = 1;
    
    // TODO: Mettere un fix
    $scope.oozieWorkflow = oozie;
    
    $scope.owlsWorkflow = owls;
    $scope.services = services.servicesByArea;
    
    
    $scope.notifications = [];

    $scope.newTemplate = {};


    $scope.constraints = {"data":[
      {
        "name": "k",
        "type": "int",
        "val": 1
      },
      {
        "name": "scc",
        "type": "text",
        "val": ""
      },
    ]};

    function init(){
      $scope.formData = {};
      $scope.configurationsName = [];
      $scope.done = project.project.done;
      $scope.visualization_url = $sce.trustAsResourceUrl(project.project.visualization_url);
      $scope.generation_url = $sce.trustAsResourceUrl('http://193.206.100.151:8080');
      //$scope.configurationsName = project.project.name || ["1"];
      $scope.anagraphic = project.project.anagraphic || {};
      $scope.mainId = project.project.main_project;
      $scope.projectName = project.project.name || "";
      $scope.flow_type = project.project.flow_type || "";
      $scope.platform_flow_type = project.project.platform_flow_type || "";
      $scope.constraint = project.project.constraint || "";
      // we will store all of our form data in this object
      
      $scope.elements = $localStorage.elements || project.project.workflow;

      $scope.cleanForm = $scope.cleanForm || DataService.getCleanForm();
      for(var index = 0; index<project.declaratives.length; index++){
        
        $scope.formData[project.declaratives[index].name] = {
          "@id": project.declaratives[index]["@id"],
          //"@context": JSON.parse(project.declaratives[index]["@context"]),
          "@context": project.declaratives[index]["@context"],
          "tdm:targetDataSources": project.declaratives[index]["tdm:targetDataSources"],
          "tdm:anagraphic": project.declaratives[index]["tdm:anagraphic"],
          "tdm:integration": project.declaratives[index]["tdm:integration"],
          "tdm:representation": project.declaratives[index]["tdm:representation"],
          "tdm:preparation":project.declaratives[index]["tdm:preparation"],
          "tdm:analytics": project.declaratives[index]["tdm:analytics"],
          "tdm:processing": project.declaratives[index]["tdm:processing"],
          "tdm:display": project.declaratives[index]["tdm:display"]
        }
        
        /*
        $scope.formData[project.declaratives[index].name] = {
          "@id": project.declaratives[index]["@id"],
          "@context": JSON.parse(project.declaratives[index]["@context"]),
          "tdm:targetDataSources": project.declaratives[index]["tdm:targetDataSources"],
          "tdm:anagraphic": {},
          "tdm:representation": project.declaratives[index]["tdm:representation"],
          "tdm:preparation":{},
          "tdm:analytics": {},
          "tdm:processing": {},
          "tdm:display": {}
        }
        
        $scope.hideDictionary = {
          "Prova":{},
          "Prova2":{},
          "Prova3":{}
        };
    
        $scope.oldDictionary = {
          "Prova":{},
          "Prova2":{},
          "Prova3":{}
        };
        */



        $scope.hideDictionary = project.project.hideDictionary;
        $scope.oldDictionary = {};

        $scope.configurationsName.push(project.declaratives[index].name);
        //$scope.configurationsName.push({"name":project.declaratives[index].name});
        cytoData.getGraph().then(function(graph){
          $scope.graph = graph;
        });
      }

      $scope.test = $scope.formData;

      //TODO: FIX ME
      $scope.configurationSelected = {"name": ""};
      for(property in $scope.formData){
        $scope.configurationSelected.name = property
      }

      //$scope.configurationSelected = $scope.formData[project.declaratives[0].name];
      //debugger;
      //$scope.formData = $localStorage.formData || {};
      //TODO: Modificare perché ci sarà una init che caricherà la conf corretta
      //$scope.formData[$scope.configurationSelected] = DataService.cleanForm; 
    };

    $scope.$watch('$root.showAlert', function(newVal, oldVal){
      if(newVal == true){
        $timeout(function() {
            $rootScope.removeAlert();
        }, 4500);
      }
    }, true);

    $scope.goToMainProject = function(){
      $state.go('projects',{id: $scope.mainId});
    };
    

    $scope.calculateCheck = function(val, answer){
      
      var result = false;
      if(val !== undefined && answer != undefined){
        result = val['tdm:label'] == answer['tdm:label'] ? true : false;
      }
       
      return result;
      
    }

    $scope.changeSubconfiguration = function(){
      console.log("Changed selection " + $scope.configurationSelected.name );
    };
    
    $scope.addSubconf = function(){
      ngDialog.openConfirm({ 
            template: 'addSubconfName', 
            className: 'ngdialog-theme-default',
            scope: $scope 
        }).then(function (success) {
          ProjectsService.addSubconf($stateParams.id, $scope.newSubconfName.name)
            .then(function(success){
              if(success.status.result === "Success"){
                $scope.configurationsName.push($scope.newSubconfName.name);
                $scope.configurationSelected.name= $scope.configurationsName[$scope.configurationsName.length-1];
                angular.copy(success.declarative, $scope.formData[$scope.configurationSelected.name]);
                $scope.formData[$scope.configurationSelected.name] = angular.copy(success.declarative);
                $scope.formData[$scope.configurationSelected.name]["tdm:anagraphic"] = $scope.anagraphic
                $scope.newSubconfName.name = '';
                $scope.hideDictionary[$scope.configurationSelected.name] = {};
                $scope.oldDictionary[$scope.configurationSelected.name] = {};
              }
            }, function (error) {
             console.log("Errore");
            });
          });
          /*
          $scope.configurationsName.push($scope.newSubconfName.name);
          $scope.configurationSelected = $scope.configurationsName[$scope.configurationsName.length-1];
          $scope.formData[$scope.configurationSelected] = {};
          
          $scope.cleanForm.then(function(cleanForm){
            console.log(cleanForm);
            angular.copy(cleanForm, $scope.formData[$scope.configurationSelected]);
            $scope.hideDictionary[$scope.configurationSelected] = {};
            $scope.formData[$scope.configurationSelected]['tdm:anagraphic'] = $scope.anagraphic;
          })
          
          $scope.newSubconfName.name = '';
        }, function (error) {
            
        });
         */
    };

    $scope.checkServiceComposition = function(){
      if($scope.elements["START"]!== undefined && 
        $scope.elements["START"].group=="nodes" && 
        $scope.elements["START"].data.nodeType.type=="START" &&
        $scope.elements["STOP"]!== undefined && 
        $scope.elements["STOP"].group=="nodes" && 
        $scope.elements["STOP"].data.nodeType.type=="STOP"){
        return false;
      }
      return true
    }

    $scope.removeSubconf = function(){
      ngDialog.openConfirm({ 
            template: 'removeSubconf', 
            className: 'ngdialog-theme-default',
            scope: $scope 
        }).then(function (success) {
          console.log("Rimuovo la subconf " + $scope.configurationSelected.name);
          removeFromConf();
        }, function (error) {
            
        });
    };    

    $scope.showForm = function(stateName){
      //debugger;
      if(stateName === 'form.services' || 
        stateName === 'form.owls' || 
        stateName === 'form.workflow' ||
        stateName === 'form.visualization' ||
        stateName === 'form.code_generation' || 
        stateName === 'form.code_based_generator' ){
        return false;
      }
      return true;
    };

    $scope.showOwls = function(owls){
      $scope.owlsToShow = owls;
      ngDialog.openConfirm({ 
            template: 'showService', 
            className: 'ngdialog-theme-default owls-width',
            scope: $scope,
            data: owls
        }).then(function (success) {
          console.log("Mostro OWLS " + owls);
          $scope.owlsToShow = "";
        }, function (error) {
            
        });
    };

    function removeFromConf(){
      if($scope.configurationsName.length == 1){
        console.log("Non puoi cancellare anche l'ultima configurazione");
        return false;
      }

      var declarativeID = getDeclarativeID($scope.formData[$scope.configurationSelected.name]["@id"]);
      ProjectsService.deleteSubconf(declarativeID)
            .then(function(response){
              if(response.status.result === "Success"){
                var declarativeName = response.declarative.name;
                var indexElement = $scope.configurationsName.indexOf(declarativeName);
                if(indexElement != -1) {
                  $scope.configurationsName.splice(indexElement, 1);
                }
                delete $scope.formData[declarativeName];
                delete $scope.hideDictionary[declarativeName];
                delete $scope.oldDictionary[declarativeName];

                $scope.configurationSelected.name = $scope.configurationsName[0];
              }
              
              
            }, function (error) {
             console.log("Errore");
            });

      
      /*
      //TODO: Rimuovere la configurazione, se è l'unica allora notifica
      if($scope.configurationsName.length == 1){
        console.log("Non puoi cancellare anche l'ultima configurazione");
        return false;
      }

      //TODO: Rimuovere la formData associata e la hideDictionary corrispondente(tramite servizi ?)
      var indexElement = $scope.configurationsName.indexOf($scope.configurationSelected.name);
      if(indexElement != -1) {
        $scope.configurationsName.splice(indexElement, 1);
      }

      delete $scope.formData[$scope.configurationSelected.name];
      delete $scope.hideDictionary[$scope.configurationSelected.name];
      delete $scope.oldDictionary[$scope.configurationSelected.name];

      $scope.configurationSelected.name = $scope.configurationsName[0];
      */
    };

    function getDeclarativeID(str){
      const regex = /\d{1,}$/g;
      var result;
      while ((m = regex.exec(str)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
          regex.lastIndex++;
        }
    
        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
          //console.log(`Found match, group ${groupIndex}: ${match}`);
          result = match;
        });
        return result;
      }
    };

    $scope.options = { //See http://js.cytoscape.org/#core/initialisation for core options
      textureOnViewport:true,
      pixelRatio: 'auto',
      motionBlur: false,
      hideEdgesOnViewport:true
    };

   $scope.layout = {name: 'breadthfirst'}   //See http://js.cytoscape.org/#collection/layout for available layouts and options

   $scope.cy_graph_ready = function(evt){
      console.log('graph ready to be interacted with: ', evt);
   };
   
   /*
   $scope.elements = {};
   debugger;
   if($localStorage.elements !== {}){
    $scope.elements = $localStorage.elements;
   } else {
    $scope.elements = ProjectsService.getWorkflow($scope.id);
   };
    */
   //$scope.elements = $localStorage.elements !== {} || ProjectsService.getWorkflow($scope.id);
   /*
   {
    n1:{
     group: 'nodes',
     data:{serviceName: "tempp", 'shape':"rectangle"} //Data property mandatory for all elements
    },
    n2:{
     group: 'nodes',
     data:{serviceName: "tempp2", 'shape':"ellipse"}
   },
   n3:{
     group: 'nodes',
     data:{serviceName: "temp3", 'shape':"ellipse"}
   },
    e1:{
     group:'edges',
     data:{
      target: 'n1',  //Source and Target mandatory for edges.
      source: 'n2'
     }
    },
    e2:{
     group:'edges',
     data:{
      target: 'n2',  //Source and Target mandatory for edges.
      source: 'n2'
     }
    },
    e3:{
     group:'edges',
     data:{
      target: 'n3',  //Source and Target mandatory for edges.
      source: 'n1'
     }
    }
   }
   */
    $scope.style = [ // See http://js.cytoscape.org/#style for style formatting and options.
          {
            selector: 'node',
            style: {
                'shape': 'data(shape)',
                'border-width': 0,
                'background-color': '#b51621',
                'content': 'data(id)',
                'font-family': '"Titillium Web", HelveticaNeue-Light, "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif'
            }
          },{
            selector: 'edge',
            style: {
                'target-arrow-shape': 'triangle',
                'target-arrow-fill': 'filled',
                'target-arrow-color': 'black',
                'line-color': 'black'
            }
          },{
      selector: '$node > node',
      css: {
        'padding-top': '10px',
        'padding-left': '10px',
        'padding-bottom': '10px',
        'padding-right': '10px',
        'text-valign': 'top',
        'text-halign': 'center',
        'background-color': '#bbb'
      }
    }
    ]

    $scope.$on('cy:node:click', function(ng,cy){
      $scope.showNode = true;
      var node = cy.cyTarget;
      $scope.nodeId = node.id();
      $scope.nodeData = node.data();

      $scope.edgeId = "";
      $scope.edgeFrom = "";
      $scope.edgeTo = "";
      $scope.showEdge = false;
      
      $('.panel-collapse.in').collapse('toggle');
      /*
      cy.cy.edges().animate({
          style: { 'line-color': 'red' }
      }, {
        duration: 1000
      });
      */
      //$scope.graph.fit();
      $scope.$apply();
      
    });

    $scope.$on('cy:edge:click', function(ng,cy){
      var edge = cy.cyTarget;
      $scope.showEdge = true;
      $scope.edgeTo = edge.data().target;
      $scope.edgeFrom = edge.data().source;
      $scope.edgeId = edge.data().id;

      $scope.nodeId = "";
      $scope.nodeData = {serviceName: ""};
      $scope.showNode = false;

      $scope.$apply();
    });
  
    $scope.centerWorkflow = function(){
      $scope.graph.center();
    };

    $scope.clearWorkflow = function(){
      // for all properties
      
      Object.getOwnPropertyNames($scope.elements).forEach(function (prop) {
        delete $scope.elements[prop];
      });
      /*
      $scope.elements = {};
      */
      //$scope.graph.elements().remove();
      //$scope.graph.json().elements;
    };

    $scope.fit = function(){
      $scope.graph.fit();
    };

    $scope.addServiceToWorkflow = function(service, nodeType){
      
      angular.copy(service, $scope.serviceSelected);

      // Metto tutti i valori o 0 se INT 
      // oppure false se BOOLEAN oppure "" se STRING oppure 0.0 se DOUBLE

      var numberOfProperties = $scope.serviceSelected.length;
      for(i = 0; i<$scope.serviceSelected.properties.length; i++){
        for(property in $scope.serviceSelected.properties[i]){
          var propertyToSet = $scope.serviceSelected.properties[i][property];
          if(propertyToSet.value == null && propertyToSet.defaultValue == null){
            switch(propertyToSet.type) {
              case "INT":
                  propertyToSet.defaultValue = 0;
                  break;
              case "DOUBLE":
                  propertyToSet.defaultValue = 0.0;
                  break;
              case "STRING":
                  propertyToSet.defaultValue = "";
                  break;
              case "BOOLEAN":
                  propertyToSet.defaultValue = false;
                  break;
              default:
                  propertyToSet.defaultValue = "";
            }
          }
          // PROVA PER VEDERE SE IL TEMPLATE DEL SERVICE SELECTED PRENDE I VALORI CORRETTI
          propertyToSet.value = angular.copy(propertyToSet.defaultValue);
        }
      }

      ngDialog.openConfirm({ 
            template: 'addServiceForm', 
            className: 'ngdialog-theme-default',
            scope: $scope,
            data: owls
        }).then(function (success) {
          //TODO: Inserisco nel workflow
          //console.log($scope.serviceSelected);
          //$scope.serviceSelected.shape = "ellipse";
          var node = {
            group: "nodes",
            data: {}
          };
          //debugger;
          node.id = angular.copy($scope.serviceSelected.serviceName);
          node.data.service = angular.copy($scope.serviceSelected);
          node.data.service.owls = "";
          node.data.shape = "ellipse";
          node.data.nodeType = {
                                  "type":nodeType
                              };
          //debugger;
          $scope.elements[$scope.serviceSelected.serviceName] = angular.copy(node);
          /*
          $scope.elements[$scope.serviceSelected.serviceName] = {
            group: "nodes",
            data: {shape: "ellipse"}
          };
          */
          
        }, function (error) {
            
      });

      
      /*
      $scope.graph.add({
        group: "nodes",
        data: {serviceName: service.title, shape: "ellipse"}
      });
      */
      

      //debugger;
      $scope.graph.fit();
    };

    $scope.addTemplateToWorkflow = function(template){
        // Add Nodes to the canvas
        for(var i=0; i<template.graph.nodes.length; i++){
          var templateNode = template.graph.nodes[i];
          var nodeType = templateNode.data.nodeType;
          var node = {
            group: "nodes",
            data: {}
          };
          
          
          node.id = angular.copy(templateNode.data.id);
          node.data.service = angular.copy(templateNode.data.service);
          node.data.shape = angular.copy(templateNode.data.shape);
          node.data.nodeType = angular.copy(templateNode.data.nodeType);
        
          $scope.elements[templateNode.data.id] = angular.copy(node);
        }
        
        // Add Edges to the canvas

        //TODO: ATTENZIONE MANCA IL CASO OPERATOR
        for(var i=0; i<template.graph.edges.length; i++){
          var templateEdge = template.graph.edges[i];
          var node = {
            id: angular.copy(templateEdge.data.id),
            group: "edges",
            data: {
                target: angular.copy(templateEdge.data.target),
                source: angular.copy(templateEdge.data.source)
            }
          };
      
          $scope.elements[templateEdge.data.id] = angular.copy(node);
        }


    };


    $scope.addConstraint = function(){
      ngDialog.openConfirm({ 
            template: 'addConstraint', 
            className: 'ngdialog-theme-default',
            scope: $scope,
            data: owls
        }).then(function (success) {
          //TODO: Inserisco nel workflow
          //console.log($scope.serviceSelected);
          //$scope.serviceSelected.shape = "ellipse";
          
          /*
          $scope.elements[$scope.serviceSelected.serviceName] = {
            group: "nodes",
            data: {shape: "ellipse"}
          };
          */
          
          
        }, function (error) {
            
      });
    }

    $scope.addConditionalEdge = function(edgeType){
      $scope.edgeType = edgeType;
      
      ngDialog.openConfirm({ 
            template: 'addConditionalEdge', 
            className: 'ngdialog-theme-default',
            scope: $scope
        }).then(function (success) {
          
          var node = {
            group: "nodes",
            data: {}
          };

          
        if($scope.conditionalEdgeTo.id === '' && $scope.conditionalEdgeTo.id === undefined){
          // TODO: Notifica dell'errore
          return false;
        }

        if($scope.nodeId === '' && $scope.nodeId === undefined){
          // TODO: Notifica dell'errore
          return false;
        }

        if(!selectById($scope.nodeId) && !selectById($scope.conditionalEdgeTo.id) > 0){
          $scope.edgeTo = "";
          $scope.edgeFrom = "";
          return false;
        }

        var node = {
          group: "edges",
          data: {
            target: $scope.conditionalEdgeTo.id,
            source: $scope.nodeId
          }
        };

        var edgeId = new Date().getTime();
        node.id = edgeId;
      
        $scope.elements[edgeId] = angular.copy(node);
        
        if($scope.edgeType){
          $scope.elements[$scope.nodeId].data.nodeType.conf['ifTrue'] = edgeId.toString();
        } else {
          $scope.elements[$scope.nodeId].data.nodeType.conf['ifFalse'] = edgeId.toString();
        }
        
      

        $scope.conditionalEdgeTo.id = "";
          
        }, function (error) { 
          $scope.operator.type = 'OPERATOR';
          $scope.operator.operatorType = '';
          $scope.operator.conf = {};
          $scope.operatorName.name = '';
      });

    }

    $scope.addSplit = function(splitType){

      $scope.operator.type = 'OPERATOR';
      $scope.operator.operatorType = splitType;
      $scope.operator.conf = {};
      
      ngDialog.openConfirm({ 
            template: 'addSplitOperator', 
            className: 'ngdialog-theme-default',
            scope: $scope
        }).then(function (success) {
          
          var node = {
            group: "nodes",
            data: {}
          };

          
          node.id = angular.copy($scope.operatorName.name);
          node.data.shape = "rectangle";
          node.data.nodeType = angular.copy($scope.operator);
          node.data.nodeType.operatorType = angular.copy($scope.operator.operatorType);

          $scope.elements[$scope.operatorName.name] = angular.copy(node);

          $scope.operator.type = 'OPERATOR';
          $scope.operator.operatorType = '';
          $scope.operator.conf = {};
          $scope.operatorName.name = '';
          
        }, function (error) { 
          $scope.operator.type = 'OPERATOR';
          $scope.operator.operatorType = '';
          $scope.operator.conf = {};
          $scope.operatorName.name = '';
      });


      /*
      angular.extend($scope.elements,{
                n1:{
                    data:{
                      shape: "diamond",
                      nodeType: "OPERATOR"
                    },
                    position:{
                        x: 50,
                        y: 50
                    }
                }
            })
      */
    };

    $scope.addIterate = function(operatorType){
      
      $scope.operator.type = 'OPERATOR';
      $scope.operator.operatorType = operatorType;
      $scope.operator.conf = {};

      ngDialog.openConfirm({ 
            template: 'addIteratorOperator', 
            className: 'ngdialog-theme-default',
            scope: $scope
        }).then(function (success) {
          
          var node = {
            group: "nodes",
            data: {}
          };

          node.id = angular.copy($scope.operatorName.name);
          node.data.shape = "diamond";
          node.data.nodeType = angular.copy($scope.operator);
          node.data.nodeType.operatorType = angular.copy($scope.operator.operatorType);
          
          $scope.elements[$scope.operatorName.name] = angular.copy(node);

          $scope.operator.type = 'OPERATOR';
          $scope.operator.operatorType = '';
          $scope.operator.conf = {};
          $scope.operatorName.name = '';
          
        }, function (error) { 
          $scope.operator.type = 'OPERATOR';
          $scope.operator.operatorType = '';
          $scope.operator.conf = {};
          $scope.operatorName.name = '';
      });

      
    };

    $scope.addTerminator = function(operatorType){
          var node = {
            group: "nodes",
            data: {}
          };

          /*
            "data": {
            "shape": "rectangle",
            "nodeType": {
              "type": “START”,
              "operatorType": “”,
              "conf": {}
            },
            "id": "split"
          }
          
          */

          $scope.operator.type = operatorType;
          $scope.operator.operatorType = '';
          $scope.operator.conf = {};

          node.id = angular.copy(operatorType);
          node.data.shape = "rectangle";
          node.data.nodeType = angular.copy($scope.operator);
          node.data.nodeType.operatorType = "";
          
          $scope.elements[operatorType] = angular.copy(node);
    }

    $scope.showConditionalEdges = function(){
      if($scope.nodeData && $scope.nodeData.nodeType !== undefined){
        var operator = $scope.nodeData.nodeType.type;
        var operatorType = $scope.nodeData.nodeType.operatorType;
        if(operator === 'OPERATOR'){
          if(operatorType === 'IF' || operatorType === 'REPEATWHILE' || operatorType === 'REPEATUNTIL'){
            return true;
          }
        }
      }
      
      return false;
    };

    $scope.showEditNode = function(){
      if($scope.nodeData && $scope.nodeData.nodeType !== undefined){
        var operatorType = $scope.nodeData.nodeType.operatorType;
        if($scope.nodeData.nodeType.type === 'SERVICE' || 
          operatorType === 'IF' ||
          operatorType === 'REPEATWHILE' || 
          operatorType === 'REPEATUNTIL'){
          return true;
        }
      }

      return false;
    };

    $scope.addEdge = function(){
      if($scope.edgeTo === '' && $scope.edgeTo === undefined){
        // TODO: Notifica dell'errore
        return false;
      }

      if($scope.edgeFrom === '' && $scope.edgeFrom === undefined){
        // TODO: Notifica dell'errore
        return false;
      }

      if(!selectById($scope.edgeFrom) && !selectById($scope.edgeTo)){
        $scope.edgeTo = "";
        $scope.edgeFrom = "";
        return false;
      }

      var node = {
        group: "edges",
        data: {
          target: $scope.edgeTo,
          source: $scope.edgeFrom
        }
      };

      var edgeId = new Date().getTime();
      node.id = edgeId;
      
      $scope.elements[edgeId] = angular.copy(node);
      
      /*
      $scope.graph.add({
        group: "edges",
        data:{
          target: $scope.edgeTo,
          source: $scope.edgeFrom
        }
      });
      */

      $scope.edgeTo = "";
      $scope.edgeFrom = "";
    };

    $scope.editNode = function(){
      
      if($scope.nodeData.nodeType.type === 'SERVICE'){
        ngDialog.openConfirm({ 
            template: 'editServiceForm', 
            className: 'ngdialog-theme-default',
            scope: $scope,
            data: owls
        }).then(function (success) {
          
          $scope.elements[$scope.nodeId].data = angular.copy($scope.nodeData);
          
          return true;
        }, function (error) {
            
        });
        
      } else if($scope.nodeData.nodeType.type === 'OPERATOR'){
        
        var operatorType = $scope.nodeData.nodeType.operatorType;
        if(operatorType === 'REPEATUNTIL' || operatorType === 'REPEATWHILE' || operatorType === 'IF'){
          ngDialog.openConfirm({ 
            template: 'editIteratorOperator', 
            className: 'ngdialog-theme-default',
            scope: $scope,
            data: owls
          }).then(function (success) {
            
            $scope.elements[$scope.nodeId].data = angular.copy($scope.nodeData);
            
            return true;
          }, function (error) {
            
          });

        }

      }
      
    };

    function findEdgesToDelete(nodeId){
      var ids = [];
      for (var k in $scope.elements) {
        if ($scope.elements.hasOwnProperty(k)) {
           if($scope.elements[k].group=="edges"){
              if($scope.elements[k].data.source==nodeId || $scope.elements[k].data.target==nodeId){
                ids.push(k);
              }
           }
        }
      }
      return ids;
    }

    $scope.deleteNode = function(){
      var selected = $scope.elements[$scope.nodeId];
      if(selected === undefined){
    
        return false;
      }

      var edges = findEdgesToDelete($scope.nodeId);
      if(edges.length > 0){
        for(index=0; index<edges.length; index++){
          delete $scope.elements[edges[index]];
        } 
      }
      
      console.log(selected[0]);
      delete $scope.elements[$scope.nodeId];

      //$scope.graph.remove(selector);

      $scope.nodeId = "";
      $scope.nodeData = {serviceName: ""};

    };

    $scope.submit=function(){
      if($scope.platform_flow_type=='code_based'){
        ProjectsService.saveConstraint($scope.id, $scope.constraint).then(function(){
            $scope.showConstraintForm = false;
            
        }).catch(function(){
          $rootScope.addAlert('danger', 'error', 'This project cannot go to the Code Based Compiler');
        });
      } else {
          $rootScope.addAlert('danger', 'error', 'This project cannot go to the Code Based Compiler');
      }
        
    };
  


    $scope.deleteEdge = function(){
      //var selector = "#"+$scope.edgeId;
      //var selected = $scope.graph.filter(selector);
      var selected = $scope.elements[$scope.edgeId];
      if(selected === undefined){
        // TODO: notify we can't delete the node
        return false;
      }
      
      //$scope.graph.remove(selector);
      delete $scope.elements[$scope.edgeId];
      $scope.edgeId = "";
      $scope.edgeFrom = "";
      $scope.edgeTo = "";
      $scope.showEdge = false;

    };

    // Reset the selection of node and edge
    $scope.closeModifier = function(){
      $scope.showNode = false;
      $scope.showEdge = false;
      $scope.edgeId = "";
      $scope.edgeFrom = "";
      $scope.edgeTo = "";
      $scope.nodeId = "";
      $scope.nodeData = {serviceName: ""};
    };

    function selectById(id){
      //var selector = "#"+id;
      //var selected = $scope.graph.filter(selector);
      //return selected; 
      return $scope.elements[id] ===undefined ? false : true;

      
    }

    // END CYTOSCOPE

    $scope.conflicts = {
      "data analytics::analytics aim::learning approach::supervised": ["data analytics::analytics aim::task::flat clustering", "data analytics::analytics aim::models::prescriptive"],
      "data analytics::analytics aim::task::regression": ["data analytics::analytics aim::models::prescriptive", "data analytics::analytics quality::true positive rate"]
    };

    $scope.legend = {
      "Learning Approach::Semi-supervised": "Semi-supervised learning is a class of supervised learning tasks and techniques that also make use of unlabeled data for training – typically a small amount of labeled data with a large amount of unlabeled data. Semi-supervised learning falls between unsupervised learning (without any labeled training data) and supervised learning (with completely labeled training data). Many machine-learning researchers have found that unlabeled data, when used in conjunction with a small amount of labeled data, can produce considerable improvement in learning accuracy.  <a href=\"https://en.wikipedia.org/wiki/Semi-supervised_learning\" target=\"_blank\">More Info</a>",
      "Learning Approach::Supervised": "Supervised learning is the machine learning task of inferring a function from labeled training data.[1] The training data consist of a set of training examples. In supervised learning, each example is a pair consisting of an input object (typically a vector) and a desired output value (also called the supervisory signal). A supervised learning algorithm analyzes the training data and produces an inferred function, which can be used for mapping new examples. An optimal scenario will allow for the algorithm to correctly determine the class labels for unseen instances. This requires the learning algorithm to generalize from the training data to unseen situations in a reasonable way (see inductive bias). <a href=\"https://en.wikipedia.org/wiki/Supervised_learning\" target=\"_blank\">More Info</a>",
      "Learning Approach::Unsupervised": "Unsupervised machine learning is the machine learning task of inferring a function to describe hidden structure from unlabeled data (a classification or categorization is not included in the observations). Since the examples given to the learner are unlabeled, there is no evaluation of the accuracy of the structure that is output by the relevant algorithm—which is one way of distinguishing unsupervised learning from supervised learning and reinforcement learning. <a href=\"https://en.wikipedia.org/wiki/Unsupervised_learning\" target=\"_blank\">More Info</a>",
      "Storage Layout::Row-based": "Row-based systems are designed to efficiently return data for an entire row, or record, in as few operations as possible. This matches the common use-case where the system is attempting to retrieve information about a particular object, say the contact information for a user in a rolodex system, or product information for an online shopping system. By storing the record's data in a single block on the disk, along with related records, the system can quickly retrieve records with a minimum of disk operations. <a href=\"https://en.wikipedia.org/wiki/Column-oriented_DBMS#Row-oriented_systems\" target=\"_blank\">More Info</a>",
      "Storage Layout::Columnar": "In a row-oriented indexed system, the primary key is the rowid that is mapped to indexed data. In the column-oriented system, the primary key is the data, mapping back to rowids. <a href=\"https://en.wikipedia.org/wiki/Column-oriented_DBMS#Column-oriented_systems\" target=\"_blank\">More Info</a>",
      "Storage Layout::Log structured merge trees": "Log-structured merge-tree (or LSM tree) is a data structure with performance characteristics that make it attractive for providing indexed access to files with high insert volume, such as transactional log data. <a href=\"https://en.wikipedia.org/wiki/Log-structured_merge-tree\" target=\"_blank\">More Info</a>",
      "Data Structure::Unstructured": "Everything that has not been specifically structured is considered unstructured.  The list of truly unstructured data includes free text such as documents produced in your company, images and videos, audio files, and some types of social media. ",
      "Data Structure::Semi-structured": "Semi-structured data is information that doesn’t reside in a relational database but that does have some organizational properties that make it easier to analyze. With some process you can store them in relation database (it could be very hard for somme kind of semi structured data), but the semi structure exist to ease space, clarity or compute.<br/> <p><strong>Examples: </strong></p> CSV but  XML and JSON documents are semi structured documents,  NoSQL databases are considered as semi structured.",
      "Data Structure::Structured": "It concerns all data which can be stored in database SQL  in table with rows and columns. They have relationnal key and  can be easily mapped into pre-designed fields.",
      "Data Model::Document Oriented": "Documents in a document store are roughly equivalent to the programming concept of an object. They are not required to adhere to a standard schema, nor will they have all the same sections, slots, parts, or keys. Generally, programs using objects have many different types of objects, and those objects often have many optional fields. <a href=\"https://en.wikipedia.org/wiki/Document-oriented_database\" target=\"_blank\">More Info</a>",
      "Data Model::Graph Based": "A graph data structure consists of a finite (and possibly mutable) set of vertices or nodes or points, together with a set of unordered pairs of these vertices for an undirected graph or a set of ordered pairs for a directed graph. These pairs are known as edges. The vertices may be part of the graph structure, or may be external entities represented by integer indices or references. <a href=\"https://en.wikipedia.org/wiki/Graph_(abstract_data_type)\" target=\"_blank\">More Info</a>",
      "Data Model::Relational": "Approach to managing data using a structure and language consistent with first-order predicate logic, where all data is represented in terms of tuples, grouped into relations. <a href=\"https://en.wikipedia.org/wiki/Relational_model\" target=\"_blank\">More Info</a>",
      "Data Model::Column Oriented": "In the column-oriented system, the primary key is the data, mapping back to rowids. <a href=\"https://en.wikipedia.org/wiki/Column-oriented_DBMS\" target=\"_blank\">More Info</a>",
      "Data Model::Extended Relational": "Developed to reflect more precisely the properties and constraints that are found in more complex databases, such as in engineering design and manufacturing (CAD/CAM), telecommunications, complex software systems and geographic information systems (GIS). <a href=\"https://en.wikipedia.org/wiki/Enhanced_entity%E2%80%93relationship_model\" target=\"_blank\">More Info</a>",
      "Data Model::Key Value": "Associative array like dictionaries or map as a fundamental data model. <a href=\"https://en.wikipedia.org/wiki/Key-value_database\" target=\"_blank\">More Info</a>",
      "Data Model::API Based":"Remote APIs allow developers to manipulate remote resources through protocols, specific standards for communication that allow different technologies to work together, regardless of language or platform. <a href=\"https://en.wikipedia.org/wiki/Application_programming_interface\" target=\"_blank\">More Info</a>",
      "Data Management::Local System":"A local system refers to an application or a system of applications managed by a single operating system.",
      "Data Management::Distributed System":"A distributed system is a collection of independent computers that appears to its users as a single coherent system.",
      "Data Management::Data Stream":"Stream processing is a computing paradigm that has emerged from the necessity of handling high volumes of data in real time. In contrast to traditional databases, stream-processing systems perform continuous queries and handle data on-the- fly. Today, a wide range of application areas relies on efficient pattern detection and queries over streams. The advent of Cloud computing fosters the develop- ment of elastic stream-processing platforms, which are able to dynamically adapt based on different cost–benefit trade-offs. <a href=\"http://onlinelibrary.wiley.com/store/10.1002/widm.1100/asset/widm1100.pdf;jsessionid=F415190F6D1D5F135D323E1680394827.f02t03?v=1&t=j2rm9j75&s=a2c630f8af33f98361531bef55f2d4cc6e1532ef\" target=\"_blank\">More Info</a>",
      "Data Management::Database":"A database management system (DBMS) is a computer software application that interacts with the user, other applications, and the database itself to capture and analyze data. A general-purpose DBMS is designed to allow the definition, creation, querying, update, and administration of databases",
      "Data Management::File System":"In computing, a file system or filesystem is used to control how data is stored and retrieved. The Hadoop distributed file system (HDFS) is a distributed, scalable, and portable file system written in Java for the Hadoop framework. Some consider HDFS to instead be a data store due to its lack of POSIX compliance and inability to be mounted, but it does provide shell commands and Java API methods that are similar to other file systems. A Hadoop cluster has nominally a single namenode plus a cluster of datanodes, although redundancy options are available for the namenode due to its criticality.",
      "Coherence Model::Strong Consistency":"Operations on shared data are synchronized: <ul> <li>Strict consistency (related to time)</li><li>Sequential consistency (what we are used to)</li><li>Causal consistency (maintains only causal relations)</li><li>PRAM consistency (maintains only individual ordering)</li></ul> <a href=\"https://www.cs.rice.edu/~druschel/comp413/lectures/replication.html\" target=\"_blank\">More Info</a>",
      "Coherence Model::Weak Consistency":"Synchronization occurs only when shared data is locked and unlocked:General weak consistency, Release consistency, Entry consistency. <a href=\"https://www.cs.rice.edu/~druschel/comp413/lectures/replication.html\" target=\"_blank\">More Info</a>",
      "Partitioning::Vertical": "Vertical partitioning (often called clustering). In this strategy, each partition holds a subset of the fields for items in the data store. The fields are divided according to their pattern of use. For example, frequently accessed fields might be placed in one vertical partition and less frequently accessed fields in another. <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Partitioning::Horizontal":"Horizontal partitioning (often called sharding). In this strategy, each partition is a data store in its own right, but all partitions have the same schema. Each partition is known as a shard and holds a specific subset of the data, such as all the orders for a specific set of customers in an e-commerce application. <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Partitioning::Functional":"Functional partitioning. In this strategy, data is aggregated according to how it is used by each bounded context in the system. For example, an e-commerce system that implements separate business functions for invoicing and managing product inventory might store invoice data in one partition and product inventory data in another. <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Processing Type::Composition":"This goal is declared when there is an interest in highlighting the components of data. For instance, a stacked column chart is well suited to meet this goal.",
      "Processing Type::Geospatial":"The goal is that of analyzing data values using a geographical map as a graphical context. A choropleth map is an example",
      "Processing Type::Order/Rank/Sort":"This goal is declared if there is an interest in analyzing objects by emphasizing their ordering. Examples of visualizations addressing this goal are alphabetical lists of names and lists of objects ordered by one of their attributes",
      "Processing Type::Relationship":"This goal is declared when there is an interest in analyzing the correlation between two or more objects or attribute values of the dataset, for instance using a scatter plot",
      "Processing Type::Comparison":"Refers to the process of examining two or more objects or values to establish their similarities and dissimilarities, e.g., by means of a column chart",
      "Processing Type::Cluster/Categorize":"Categorization is the assignment of similar data objects to a category so that objects in the same category are more similar to each other than to those in other categories. This goal aims at analyzing data in such a way as to emphasize their grouping into categories. A dendrogram is a well-suited visualization for this goal",
      "Processing Type::Distribution":"Captures how objects are dispersed in space, and specifically supports the detection of outliers and gaps that are important for understand- ing data quality and data coverage. A well-suited visualization type for this goal is a column histogram",
      "Processing Type::Trends":"Is a pattern of gradual change in the average or general tendency of data variables in a series of data records, and can be analyzed using for instance a line chart",
      "Dimensionality":"Used to enable TOREADOR users to declare the number of variables they wish to visualize, aimed at letting the platform suggest the most suitable visualization. All variables (both dependent and independent) are counted",
      "Dimensionality::1D":"A single numerical value or a string is visualized, using for instance a gauge or an alert",
      "Dimensionality::2D":"One dependent variable is visualized as a function of one independent variable using for instance a single-line chart or a tag cloud",
      "Dimensionality::nD":"Here each data object is a point in an n-dimensional space like in a pivot table as an example",
      "Dimensionality::tree":"A collection of items with each item (except the root) having a link to one parent item, displayed for instance by means of a dendrogram",
      "Dimensionality::graph":"A collection of items with each item linked to an arbitrary number of other items. A network map is an example of a well-suited visualization for this kind of data",
      "Data Cardinality":"Used to enable TOREADOR users to qualitatively declare the cardinality of the data to be visualized, aimed at letting the platform suggest the most suitable visualization",
      "Data Cardinality::Low":"For low-cardinality data (approximately, from a few items to a few dozens items), a discrete visualization such as a bar chart or a pie chart could be appropriate",
      "Data Cardinality::High":"For high-cardinality data (some dozens items or more), a continuous visualization such as a line chart or a heat map could be appropriate",
      "Independent Data Type":"Used to enable TOREADOR users to declare the type of the independent variable(s) in the dataset to be analyzed, aimed at letting the platform suggest the most suitable visualization",
      "Interaction":"Used to enable TOREADOR users to declare the type of inter- actions to be supported by the visualization, aimed at letting the platform suggest the most suitable visualization",
      "Interaction::Overview":"gain an overview of the entire data collection, using for instance zoomed out views of each data type to see the entire collection plus an adjoining detail view",
      "Interaction::Zoom":"Zoom in on items of interest, by controlling the zoom focus and the zoom factor. Zooming could be on one dimension at a time or by adjusting the size of the field-of-view box",
      "Interaction::Filter":"filter out uninteresting items, to enable users to quickly focus on their interests by eliminating unwanted items. This is achieved by applying dynamic queries to the items in the data collection",
      "Interaction::Details-on-demand":"Select an item or group of items and get details when needed. The usual approach here is to click on an item to get a pop-up window with values of each of the item attributes",
      "Interaction::History":"Keep a history of actions to support undo, redo, and progressive refinement",
      "Interaction::Extract":"Enable the extraction of sub-collections and of the parameters of the query used to get the data",
      "Interaction::Projection":"The basic idea is to dynamically change the projections in order to explore a multidimensional data set, for instance by showing all its interesting two-dimensional projections as a series of scatter plots",
      "Interaction::Distortion":"Show portions of the data with a high level of detail while others are shown with a lower level of detail",
      "Interaction::Link&Brush":"Combine different visualization methods to overcome the shortcomings of each single technique. In particular, to encourage and simplify the detection of dependencies and correlations, in- teractive changes made in one visualization are automatically reflected in the other visualizations",
      "User":"Used to enable TOREADOR users to declare their skill, aimed at letting the platform suggest the most suitable visualization",
      "User::Techie":"These users have some experience with advanced software tools, are used to deal with large volumes of heterogeneous data, and have a deeper understanding of big data analytic processes, so they can properly interact with complex visualizations to obtain insights for decision making",
      "User::Lay-user":"Computer-literate though they may have troubles in understanding and taking full advantage from complex visualizations. They range from novice to casual users",
      "Independent Data Type::Nominal":"In this qualitative data type, also called categorical, each data vari- able is assigned to one category and no two variables are assigned to the same category. The basic operation allowed is the determination of equality, and the allowed statistics are number of cases, mode, and contingency correlation. Examples include Boolean data and labels describing objects (e.g., people’s nations)",
      "Independent Data Type::Ordinal":"An ordinal scale (also called a sequence) is qualitative. Values can be sorted but there is no information as to how close or distant values are from one another. The basic operation allowed is determination of greater or less, and the allowed statistics are median and percentiles. Examples include days of the week and rankings such as low, medium, and high.",
      "Independent Data Type::Interval":"This quantitative type is also called value and discrete scale, and it supports the determination of equality of intervals or differences and (from a statistical point of view) the computation of mean, standard deviation, rank- order correlation, and product-moment correlation. Examples are calendar dates and the Celsius and Fahrenheit temperature scales, which have an arbi- trarily defined zero point",
      "Independent Data Type::Ratio":"The ratio data type (also called proportional or continuous) is a quan- titative numerical scale; it represents values organized as an ordered sequence, with meaningful uniform spacing, and has a unique and non-arbitrary zero point. Most physical measurements (including length, weight, height, time, etc.) and business figures (e.g., sold quantities and revenues) belong to this type, which supports determination of equality of ratios and computation of the coefficient of variation",
      "Dependent Data Type":"Used to enable TOREADOR users to declare the type of the dependent variable(s) in the dataset to be analyzed, aimed at letting the platform suggest the most suitable visualization",
      "Dependent Data Type::Nominal":"In this qualitative data type, also called categorical, each data vari- able is assigned to one category and no two variables are assigned to the same category. The basic operation allowed is the determination of equality, and the allowed statistics are number of cases, mode, and contingency correlation. Examples include Boolean data and labels describing objects (e.g., people’s nations)",
      "Dependent Data Type::Ordinal":"An ordinal scale (also called a sequence) is qualitative. Values can be sorted but there is no information as to how close or distant values are from one another. The basic operation allowed is determination of greater or less, and the allowed statistics are median and percentiles. Examples include days of the week and rankings such as low, medium, and high.",
      "Dependent Data Type::Interval":"This quantitative type is also called value and discrete scale, and it supports the determination of equality of intervals or differences and (from a statistical point of view) the computation of mean, standard deviation, rank- order correlation, and product-moment correlation. Examples are calendar dates and the Celsius and Fahrenheit temperature scales, which have an arbi- trarily defined zero point",
      "Dependent Data Type::Ratio":"The ratio data type (also called proportional or continuous) is a quan- titative numerical scale; it represents values organized as an ordered sequence, with meaningful uniform spacing, and has a unique and non-arbitrary zero point. Most physical measurements (including length, weight, height, time, etc.) and business figures (e.g., sold quantities and revenues) belong to this type, which supports determination of equality of ratios and computation of the coefficient of variation",
      "Data Density::Low":"For low-cardinality data (approximately, from a few items to a few dozens items), a discrete visualization such as a bar chart or a pie chart could be appropriate",
      "Data Density::High":"For high-cardinality data (some dozens items or more), a continuous visualization such as a line chart or a heat map could be appropriate",
      "User::Techie":"These users have some experience with advanced software tools, are used to deal with large volumes of heterogeneous data, and have a deeper understanding of big data analytic processes, so they can properly interact with complex visualizations to obtain insights for decision making",
      "User::Lay-User":"These users are computer-literate though they may have troubles in understanding and taking full advantage from complex visualizations. They range from novice to casual users",
      "Goal":"Used to enable TOREADOR users to declare their analysis goal, aimed at letting the platform suggest the most suitable visualization",
      "Goal::Composition":"Refers to the way distinct parts of objects are arranged to form a whole. This goal is declared when there is an interest in highlighting the components of data. For instance, a stacked column chart is well suited to meet this goal",
      "Goal::Geospatial":"The goal is that of analyzing data values using a geographical map as a graphical context. A choropleth map is an example of a type of visualization that fits this goal",
      "Goal::Order/Rank/Sort":"declared if there is an interest in analyzing objects by emphasizing their ordering. Examples of visualizations addressing this goal are alphabetical lists of names and lists of objects ordered by one of their attributes",
      "Goal::Relationship":"Declared when there is an interest in analyzing the correlation between two or more objects or attribute values of the dataset, for instance using a scatter plot",
      "Goal::Comparison":"Refers to the process of examining two or more objects or values to establish their similarities and dissimilarities, e.g., by means of a column chart",
      "Goal::Cluster/Categorize":"Aims at analyzing data in such a way as to empha- size their grouping into categories. A dendrogram is a well-suited visualization for this goal",
      "Goal::Distribution":"Captures how objects are dispersed in space, and specifically supports the detection of outliers and gaps that are important for understand- ing data quality and data coverage. A well-suited visualization type for this goal is a column histogram",
      "Goal::Trends":"A pattern of gradual change in the average or general tendency of data variables in a series of data records, and can be analyzed using for instance a line chart",
      "Analysis Goal::Near Real-Time": "Refers to the time delay introduced, by automated data processing or network transmission, between the occurrence of an event and the use of the processed data, such as for display or feedback and control purposes. For example, a near-real-time display depicts an event or situation as it existed at the current time minus the processing time, as nearly the time of the live event. <a href=\"https://en.wikipedia.org/wiki/Real-time_computing#Near_real-time\" target=\"_blank\">More Info</a>",
      "Analysis Goal::Batch":"Batch data processing is an efficient way of processing high volumes of data is where a group of transactions is collected over a period of time. <a href=\"http://www.datasciencecentral.com/profiles/blogs/batch-vs-real-time-data-processing\" target=\"_blank\">More Info</a>",
      "Analysis Goal::Real-Time":"A real-time system has been described as one which controls an environment by receiving data, processing them, and returning the results sufficiently quickly to affect the environment at that time. <a href=\"https://en.wikipedia.org/wiki/Real-time_computing\" target=\"_blank\">More Info</a>",
      "Interaction::Dynamic":"When do not know in advance the task, to specify it we need multiple interactions and the number of interactions is dynalically determined.",
      "Interaction::Multiple":"When do not know in advance the task, to specify it we need multiple interactions but this number is predetermined.",
      "Interaction::Single":"When know in advance the task and the processing apply it",
      "Data Storage Consistency":"Whereas eventual consistency is only a liveness guarantee (updates will be observed eventually), strong eventual consistency (SEC) adds the safety guarantee that any two nodes that have received the same (unordered) set of updates will be in the same state. If, furthermore, the system is monotonic, the application will never suffer rollbacks. Conflict-free replicated data types are a common approach to ensuring SEC. <a href=\"https://en.wikipedia.org/wiki/Eventual_consistency\" target=\"_blank\">More Info</a>",
      "Elasticity":" <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Latency":"Improve performance. Data access operations on each partition take place over a smaller volume of data. Provided that the data is partitioned in a suitable way, partitioning can make your system more efficient. Operations that affect more than one partition can run in parallel. Each partition can be located near the application that uses it to minimize network latency. <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Availability":"Improve availability. Separating data across multiple servers avoids a single point of failure. If a server fails, or is undergoing planned maintenance, only the data in that partition is unavailable. Operations on other partitions can continue. Increasing the number of partitions reduces the relative impact of a single server failure by reducing the percentage of data that will be unavailable. Replicating each partition can further reduce the chance of a single partition failure affecting operations. It also makes it possible to separate critical data that must be continually and highly available from low-value data that has lower availability requirements (log files, for example). <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Operational Flexibility":"Provide operational flexibility. Partitioning offers many opportunities for fine tuning operations, maximizing administrative efficiency, and minimizing cost. For example, you can define different strategies for management, monitoring, backup and restore, and other administrative tasks based on the importance of the data in each partition. <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Security":"Improve security. Depending on the nature of the data and how it is partitioned, it might be possible to separate sensitive and non-sensitive data into different partitions, and therefore into different servers or data stores. Security can then be specifically optimized for the sensitive data. <a href=\"https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning\" target=\"_blank\">More Info</a>",
      "Task::Binary Classification": "Binary or binomial classification is the task of classifying the elements of a given set into two groups on the basis of a classification rule. <a href=\"https://en.wikipedia.org/wiki/Binary_classification\" target=\"_blank\">More Info</a>",
      "Task::Multi-class classification": "In machine learning, multiclass or multinomial classification is the problem of classifying instances into one of the more than two classes. <a href=\"https://en.wikipedia.org/wiki/Binary_classification\" target=\"_blank\">More Info</a>",
      "Task::Regression":"In statistical modeling, regression analysis is a statistical process for estimating the relationships among variables. It includes many techniques for modeling and analyzing several variables, when the focus is on the relationship between a dependent variable and one or more independent variables (or 'predictors'). More specifically, regression analysis helps one understand how the typical value of the dependent variable (or 'criterion variable') changes when any one of the independent variables is varied, while the other independent variables are held fixed. <a href=\"https://en.wikipedia.org/wiki/Regression_analysis\" target=\"_blank\">More Info</a>",
      "Task::Structured output classification":"Structured prediction or structured (output) learning is an umbrella term for supervised machine learning techniques that involves predicting structured objects, rather than scalar discrete or real values. <a href=\"https://en.wikipedia.org/wiki/Structured_prediction\" target=\"_blank\">More Info</a>",
      "Task::Forecasting":"The process of making predictions of the future based on past and present data and most commonly by analysis of trends. <a href=\"https://en.wikipedia.org/wiki/Forecasting\" target=\"_blank\">More Info</a>",
      "Task::Link Prediction":"The problem we want to tackle here is to predict the likelihood of a future association between two nodes, knowing that there is no association between the nodes in the current state of the graph.  <a href=\"http://be.amazd.com/link-prediction/\" target=\"_blank\">More Info</a>",
      "Task::Subgroup Discovery":"The goal of subgroup discovery is to find rules describing subsets of the population that are sufficiently large and statistically unusual. <a href=\"https://docs.rapidminer.com/studio/operators/modeling/predictive/rules/subgroup_discovery.html\" target=\"_blank\">More Info</a>",
      "Task::Anomaly/Fault detection":"Anomaly detection (also outlier detection) is the identification of items, events or observations which do not conform to an expected pattern or other items in a dataset. <a href=\"https://en.wikipedia.org/wiki/Anomaly_detection\" target=\"_blank\">More Info</a>",
      "Task::Flat Clustering":"Clustering algorithms group a set of documents into subsets or clusters . The algorithms' goal is to create clusters that are coherent internally, but clearly different from each other. In other words, documents within a cluster should be as similar as possible; and documents in one cluster should be as dissimilar as possible from documents in other clusters. <a href=\"https://nlp.stanford.edu/IR-book/html/htmledition/flat-clustering-1.html\" target=\"_blank\">More Info</a>",
      "Task::Hierarchical Clustering":"Hierarchical clustering (or hierarchic clustering ) outputs a hierarchy, a structure that is more informative than the unstructured set of clusters returned by flat clustering.[*]Hierarchical clustering does not require us to prespecify the number of clusters and most hierarchical algorithms that have been used in IR are deterministic. These advantages of hierarchical clustering come at the cost of lower efficiency. The most common hierarchical clustering algorithms have a complexity that is at least quadratic in the number of documents compared to the linear complexity of  $K$-means and EM <a href=\"http://nlp.stanford.edu/IR-book/html/htmledition/hierarchical-clustering-1.html#ch:hierclust\" target=\"_blank\">More Info</a>",
      "Task::Fuzzy Clustering":"Clustering algorithms group a set of documents into subsets or clusters . The algorithms' goal is to create clusters that are coherent internally, but clearly different from each other. In other words, documents within a cluster should be as similar as possible; and documents in one cluster should be as dissimilar as possible from documents in other clusters. <a href=\"https://nlp.stanford.edu/IR-book/html/htmledition/flat-clustering-1.html\" target=\"_blank\">More Info</a>", 
      "Task::Crisp Clustering":"In crisp clustering (also known as hard clustering), data is divided into distinct clusters, where each data point can only belong to exactly one cluster",
      "Task::Pattern Discovery":"Pattern recognition is a branch of machine learning that focuses on the recognition of patterns and regularities in data. <a href=\"https://en.wikipedia.org/wiki/Pattern_recognition\" target=\"_blank\">More Info</a>",
      "Task::Classification":"classification is the problem of identifying to which of a set of categories (sub-populations) a new observation belongs, on the basis of a training set of data containing observations (or instances) whose category membership is known. <a href=\"https://en.wikipedia.org/wiki/Statistical_classification\" target=\"_blank\">More Info</a>",
      "Task::Sequence Discovery":"Sequential pattern mining is a topic of data mining concerned with finding statistically relevant patterns between data examples where the values are delivered in a sequence.[1] It is usually presumed that the values are discrete, and thus time series mining is closely related, but usually considered a different activity. Sequential pattern mining is a special case of structured data mining. <a href=\"https://en.wikipedia.org/wiki/Sequential_pattern_mining\" target=\"_blank\">More Info</a>",
      "Task::Time Series Analysis": "A time series is a series of data points indexed (or listed or graphed) in time order. Most commonly, a time series is a sequence taken at successive equally spaced points in time. Thus it is a sequence of discrete-time data. <a href=\"https://en.wikipedia.org/wiki/Time_series\" target=\"_blank\">More Info</a>",
      "Task::Association Rules":"Association rule learning is a rule-based machine learning method for discovering interesting relations between variables in large databases. It is intended to identify strong rules discovered in databases using some measures of interestingness. <a href=\"https://en.wikipedia.org/wiki/Association_rule_learning\" target=\"_blank\">More Info</a>",
      "Task::Root Cause Analysis":"Root cause analysis (RCA) is a method of problem solving used for identifying the root causes of faults or problems.[1] A factor is considered a root cause if removal thereof from the problem-fault-sequence prevents the final undesirable event from recurring; whereas a causal factor is one that affects an event's outcome, but is not a root cause. Though removing a causal factor can benefit an outcome, it does not prevent its recurrence with certainty. <a href=\"https://en.wikipedia.org/wiki/Root_cause_analysis\" target=\"_blank\">More Info</a>",
      "Anonymization Techniques::Hashing":"A cryptographic hash function is a mapping from data (keys) to values (hash values). The mapping is such that it is easy to apply the function to obtain the hash value, but it is computationally intractable the reverse. <a href=\"https://en.wikipedia.org/wiki/Hash_function\" target=\"_blank\">More Info</a>. Mapping data through hashing functions protects privacy since the hash values obfuscate the original data.",
      "Anonymization Techniques::Obfuscation":"Data masking or data obfuscation is the process of hiding original data with random characters or other data. These latter data might be more general data related to the original ones by a hierarchical relationship of type one-to-many (such as country-region-city spatial containment relationship) <a href=\"https://en.wikipedia.org/wiki/Data_masking\" target=\"_blank\">More Info</a>",
      "Anonymization Techniques::Differential privacy":"Differential privacy aims to provide means to maximize the accuracy of (statistical) queries on data while minimizing the chances of violating the privacy of entities represented in the data. <a href=\"https://en.wikipedia.org/wiki/Differential_privacy\" target=\"_blank\">More Info</a>",
      "Anonymization Techniques::K-anonimity family":"A release of data is said to have the k-anonymity property if the information for each person contained in the release cannot be distinguished from at least k-1 individuals whose information also appear in the release. <a href=\"https://en.wikipedia.org/wiki/K-anonymity\" target=\"_blank\">More Info</a>",
      "Models::Prescriptive":"Prescriptive analytics automatically synthesizes big data, mathematical sciences, business rules, and machine learning to make predictions and then suggests decision options to take advantage of the predictions. <a href=\"http://www.rosebt.com/blog/descriptive-diagnostic-predictive-prescriptive-analytics\" target=\"_blank\">More Info</a>",
      "Models::Descriptive":"Descriptive analytics looks at data and analyzes past events for insight as to how to approach the future. Descriptive analytics looks at past performance and understands that performance by mining historical data to look for the reasons behind past success or failure. <a href=\"http://www.rosebt.com/blog/descriptive-diagnostic-predictive-prescriptive-analytics\" target=\"_blank\">More Info</a>",
      "Models::Predictive":"Predictive analytics turns data into valuable, actionable information. Predictive analytics uses data to determine the probable future outcome of an event or a likelihood of a situation occurring. <a href=\"http://www.rosebt.com/blog/descriptive-diagnostic-predictive-prescriptive-analytics\" target=\"_blank\">More Info</a>",
      "Models::Diagnostic":"Diagnostic analytics are used for discovery or to determine why something happened. For example, for a social media marketing campaign, you can use descriptive analytics to assess the number of posts, mentions, followers, fans, page views, reviews, pins, etc. There can be thousands of online mentions that can be distilled into a single view to see what worked in your past campaigns and what didn’t. <a href=\"http://www.ingrammicroadvisor.com/data-center/four-types-of-big-data-analytics-and-examples-of-their-use\" target=\"_blank\">More Info</a>",
      "CONSISTENCY":"A service that is consistent operates fully or not at all. <a href=\"http://www.julianbrowne.com/article/viewer/brewers-cap-theorem\" target=\"_blank\">More Info</a>",
      "Positive Prediction Rate":"Proportions of positive results in statistics and diagnostic tests that are true positive. <a href=\"https://en.wikipedia.org/wiki/Positive_and_negative_predictive_values\" target=\"_blank\">More Info</a>",
      "True Positive Rate":"Measures the proportion of positives that are correctly identified as such. <a href=\"https://en.wikipedia.org/wiki/Sensitivity_and_specificity\" target=\"_blank\">More Info</a>",
      "True Negative Rate":"Measures the proportion of negatives that are correctly identified as such. <a href=\"https://en.wikipedia.org/wiki/Sensitivity_and_specificity\" target=\"_blank\">More Info</a>",
      "False Positive Rate":"The false positive rate is calculated as the ratio between the number of negative events wrongly categorized as positive (false positives) and the total number of actual negative events (regardless of classification). <a href=\"https://en.wikipedia.org/wiki/False_positive_rate\" target=\"_blank\">More Info</a>",
      "Root Mean Square Error":"Used measure of the differences between values (sample and population values) predicted by a model or an estimator and the values actually observed. <a href=\"https://en.wikipedia.org/wiki/Root-mean-square_deviation\" target=\"_blank\">More Info</a>",
      "Mean Absolute Error":"Quantity used to measure how close forecasts or predictions are to the eventual outcomes. <a href=\"https://en.wikipedia.org/wiki/Mean_absolute_error\" target=\"_blank\">More Info</a>",
      "Overall Accuracy":"Number of correctly predicted items/total of item to predict <a href=\"https://en.wikipedia.org/wiki/Precision_and_recall\" target=\"_blank\">More Info</a>",
      "K Coefficient":"Statistic which measures inter-rater agreement for qualitative (categorical) items. <a href=\"https://en.wikipedia.org/wiki/Precision_and_recall\" target=\"_blank\">More Info</a>",
      "Data Reduction::Data Cube Aggregation":"Produces data cubes for storing multidimensional aggregated data (e.g. extracted from a data warehouse) for OLAP analysis. For example, data on daily sales hold on millions of items and can be aggregated into monthly sales of some selected categories of items. <a href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2367630/\" target=\"_blank\">More Info</a>",
      "Data Reduction::Dimension reduction":"Dimension reduction leads to the encoding of data in a reduced format, with or without loss with respect to the initial data set. For example, principal component analysis can be used for dimensionality reduction that applies projections of initial data onto a space of a smaller dimension. <a href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2367630/\" target=\"_blank\">More Info</a>",
      "Data Reduction::Data discretization":"Data discretization techniques are used to reduce the number of values of an attribute and consequently facilitate interpretation of mining results. Automatic discretization methods exist for continuous numerical attributes that recursively partition the attribute values according to a given scale. For example, the range of an attribute price can be divided by the means of histogram analysis into several intervals, which can in turn be iteratively aggregated into larger intervals. However, these methods do not apply for discrete or nominal attributes, when the attribute values of which are not ordered. The scale for an attribute has then to be manually defined by domain experts and possibly refined with the help of heuristic methods. <a href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2367630/\" target=\"_blank\">More Info</a>",
      "Data Reduction::Data selection":"Data selection aims at identifying appropriate subsets among the initial set of attributes. This operation can be performed with the help of heuristic methods based on tests of significance or entropy-based attribute evaluation measures such as the information gain. <a href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2367630/\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Remove missing values":"A missing value, often represented by a placeholder such as NA, is a datum of which the type is known but its value isn't. Therefore, it is impossible to perform statistical analysis on data where one or more values in the data are missing. One may then choose to either omit elements from a dataset. <a href=\"https://cran.r-project.org/doc/contrib/de_Jonge+van_der_Loo-Introduction_to_data_cleaning_with_R.pdf\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Rule based":"In practice, data correction procedures involve a lot of ad-hoc transformations. This may lead to long scripts where one selects parts of the data, changes some variables, selects another part, changes some more variables, etc. When such scripts are neatly written and commented, they can almost be treated as a log of the actions performed by the analyst. However, as scripts get longer it is better to store the transformation rules separately and log which rule is executed on what record. <a href=\"https://cran.r-project.org/doc/contrib/de_Jonge+van_der_Loo-Introduction_to_data_cleaning_with_R.pdf\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Deterministic imputation":"When the data you are analyzing is generated by people rather than machines or measurement devices, certain typical human-generated errors are likely to occur. Given that data has to obey certain edit rules, the occurrence of such errors can sometimes be detected from raw data with (almost) certainty. Examples of errors that can be detected are typing errors in numbers (under linear restrictions) rounding errors in numbers and sign errors or variable swaps. In some cases a missing value can be determined because the observed values combined with their constraints force a unique solution. As an example, consider a record with variables listing the costs for staff cleaning, housing and the total total. We have the following rules. staff + cleaning + housing = total staff ≥ 0 housing ≥ 0 cleaning ≥ 0 In general, if one of the variables is missing the value can clearly be derived by solving it for the first rule (providing that the solution doesn't violate the last rule). However, there are other cases where unique solutions exist. Suppose that we have staff = total. Assuming that these values are correct, the only possible values for the other two variables is housing = cleaning = 0. <a href=\"https://cran.r-project.org/doc/contrib/de_Jonge+van_der_Loo-Introduction_to_data_cleaning_with_R.pdf\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Basic numeric imputation":"Using statistical techniques and observing the data distribution one may infer a value that will not affect the overall analysis. <a href=\"https://cran.r-project.org/doc/contrib/de_Jonge+van_der_Loo-Introduction_to_data_cleaning_with_R.pdf\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::KNN imputation":"In 𝑘 nearest neighbor imputation one defines a distance function 𝑑(𝑖, 𝑗) that computes a measure of dissimilarity between records. A missing value is then imputed by finding first the 𝑘 records nearest to the record with one or more missing values. Next, a value is chosen from or computed out of the 𝑘 nearest neighbors. In the case where a value is picked from the 𝑘 nearest neighbors, kNN-imputation is a form of hot-deck imputation. <a href=\"https://cran.r-project.org/doc/contrib/de_Jonge+van_der_Loo-Introduction_to_data_cleaning_with_R.pdf\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Hot-deck imputation":"In hot deck imputation, missing values are imputed by copying values from similar records in the same dataset. A very simple technique may impute a default value. Random value may also be selected. One form of hot-deck imputation is called <strong>last observation carried forward</strong> (or LOCF for short), which involves sorting a dataset according to any of a number of variables, thus creating an ordered dataset. The technique then finds the first missing value and uses the cell value immediately prior to the data that are missing to impute the missing value. The process is repeated for the next cell with a missing value until all missing values have been imputed. In the common scenario in which the cases are repeated measurements of a variable for a person or other entity, this represents the belief that if a measurement is missing, the best guess is that it hasn't changed from the last time it was measured. This method is known to increase risk of increasing bias and potentially false conclusions. <a href=\"https://cran.r-project.org/doc/contrib/de_Jonge+van_der_Loo-Introduction_to_data_cleaning_with_R.pdf\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Cold-deck imputation":"Cold-deck imputation selects data to be inputed from another dataset. <a href=\"https://cran.r-project.org/doc/contrib/de_Jonge+van_der_Loo-Introduction_to_data_cleaning_with_R.pdf\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Entity expansion and alignment":"Adding data to dataset by fetching it from webservices (i.e. returning json). For example, can be used for geocoding addresses to geographic coordinates. Aligning to Wikidata (formerly Freebase): this involves reconciliation — mapping string values in cells to entities in Wikidata. <a href=\"https://en.wikipedia.org/wiki/OpenRefine\" target=\"_blank\">More Info</a>",
      "Data Expansion and Correction::Record linkage":"Record linkage (RL) refers to the task of finding records in a data set that refer to the same entity across different data sources (e.g., data files, books, websites, databases). Record linkage is necessary when joining data sets based on entities that may or may not share a common identifier (e.g., database key, URI, National identification number), as may be the case due to differences in record shape, storage location, and/or curator style or preference. A data set that has undergone RL-oriented reconciliation may be referred to as being cross-linked. Record linkage is called data linkage in many jurisdictions, but is the same process. <a href=\"https://en.wikipedia.org/wiki/Record_linkage\" target=\"_blank\">More Info</a>",
      "Data Transformation::Rule-based":"Rules encode some business logic allowing data to be transformed according to a desired format. Use this feature if you need to specify custom business rules to transform data.",
      "Data Transformation::Ordering":"Sort the data according to some attributes",
      "Data Transformation::Normalization":"Use this transformation if you want, for instance, to constraint the data in a given range, or change how the values are distributed. Maps values into a new range so that some desired properties are met. <a href=\"https://en.wikipedia.org/wiki/Normalization_(statistics)\" target=\"_blank\">More Info</a>.",
      "Data Transformation::Transposing":"This transformation allows to rotate data tables so that rows become columns and vice versa",
      "Data Cleaning and Integration::Data Type Conversion":"Differences in types: such conflict occurs when the same real world property is represented using different data types in two distinct data sources; for example, the same attribute may be a number in one source and a string in another source. <a href=\"http://link.springer.com/chapter/10.1007/3-540-36271-1_12\" target=\"_blank\">More Info</a>",
      "Data Cleaning and Integration::Data Format Conversion":"Differences in data format, for example when the instances of two equivalent at- tributes have different lengths in two distinct sources. <a href=\"http://link.springer.com/chapter/10.1007/3-540-36271-1_12\" target=\"_blank\">More Info</a>",
      "Data Cleaning and Integration::Data Encoding Conversion":"Differences in the encoding of attributes, for example the attributes gender in two different data sources can be encoded using the set of instances {1, 2} and {F, M} respectively. <a href=\"http://link.springer.com/chapter/10.1007/3-540-36271-1_12\" target=\"_blank\">More Info</a>",
      "Data Cleaning and Integration::Scale Conversion":"Differences in scale, when the instances of two equivalent attributes are expressed using different units, such as Euro and USD for financial data. <a href=\"http://link.springer.com/chapter/10.1007/3-540-36271-1_12\" target=\"_blank\">More Info</a>",
      "Data Cleaning and Integration::Data Granularity Conversion":"Differences in granularity, for example, two attributes sales may refer to monthly sales in one source and annual sales in another one. <a href=\"http://link.springer.com/chapter/10.1007/3-540-36271-1_12\" target=\"_blank\">More Info</a>",
      "Data Cleaning and Integration::Data Identifier Conversion":"Conflicts related to the identifiers: two possible situations may arise; (i) two instances may have the same identifier value in two distinct sources although representing two distinct real world objects, or (ii) two instances may have two different identifier values in two distinct sources although representing the same real world object. <a href=\"http://link.springer.com/chapter/10.1007/3-540-36271-1_12\" target=\"_blank\">More Info</a>",
      "Data Cleaning and Integration::Terminology Conversion":"Differences in the terminology, for example, the attributes medicine may have in two distinct sources the two values antibiotic and penicillin for naming the same medicine. <a href=\"http://link.springer.com/chapter/10.1007/3-540-36271-1_12\" target=\"_blank\">More Info</a>",
      "Anonymization Model::Privacy Preserving Data Publishing":"Used to generate a masked version of data that preserves both individual privacy and information usefulness",
      "Anonymization Model::Privacy Preserving Data Mining":"This allows creating and publishing analytics or data mining models without disclosing private information"
    };

    $scope.disableCheckAll = function(area, goalIndex, questionIndex){
      var elementsFromPath = getElementFromPath(area, goalIndex, questionIndex);
      return elementsFromPath.elements.length == elementsFromPath.formValue.length
    };

    function convertFromElementsToGraph(elements) {
      var graph = {"nodes":[],"edges":[]};
      for (var key in elements) {
        if (elements.hasOwnProperty(key)) {
           var element = elements[key];
           if (element.group == 'nodes'){
              var node = {
                "data":{},
                "position": {
                    "x": 139.33333333333331,
                    "y": 205.33333333333334
                },
                "group": "nodes",
                "removed": false,
                "selected": false,
                "selectable": true,
                "locked": false,
                "grabbable": true,
                "classes": ""
              }

              node.data = angular.copy(element.data);
              graph.nodes.push(node);
           } else if (element.group == 'edges'){
              var node = {
                "data":{},
                "position": {
                    "x": 139.33333333333331,
                    "y": 205.33333333333334
                },
                "group": "edges",
                "removed": false,
                "selected": false,
                "selectable": true,
                "locked": false,
                "grabbable": true,
                "classes": ""
              }
              
              node.data = angular.copy(element.data);
              graph.edges.push(node);
           }
        }
      }
      return graph;

    }

    // TODO: Sarebbe meglio usare un Service apposito in modo da averlo più performante
    $scope.$watch("formData", function() {
            
            
            $localStorage.configurationsName = $scope.configurationsName;
            //$localStorage.configurationSelected.name = $scope.configurationSelected.name;
            
            $localStorage.formData = $scope.formData;
            $localStorage.elements = $scope.elements;
            
            $localStorage.anagraphic = $scope.anagraphic;
            $localStorage.oozieWorkflow = $scope.oozieWorkflow;
            $localStorage.owlsWorkflow = $scope.owlsWorkflow;
            //$localStorage.graph = $scope.graph.json().elements;
    }, true);

    $scope.$watch("elements", function() {
            $localStorage.elements = $scope.elements;
            //$localStorage.graph = $scope.graph.json().elements;
            
            //$scope.$apply();
            /*cytoData.getGraph().then(function(graph){

              $localStorage.graph = graph.json().elements;
            });
            */
            $localStorage.graph = convertFromElementsToGraph($scope.elements);
            //$scope.$apply();

            //console.log($scope.graph);
            //console.log($scope.graph.json().elements);
           
            //$localStorage.graph = $scope.graph.json().elements;
    }, true);

    /*
    $scope.$watch($scope.graph, function() {
            $localStorage.graph = $scope.graph.json().elements;
    }, true);
    */

    $scope.$watch("anagraphic", function() {
            $localStorage.anagraphic = $scope.anagraphic;
    }, true);

    $scope.$watch("oozieWorkflow", function() {
            $localStorage.oozieWorkflow = $scope.oozieWorkflow;
    }, true);

    $scope.$watch("owlsWorkflow", function() {
            $localStorage.owlsWorkflow = $scope.owlsWorkflow;
    }, true);


    $scope.calculateInterference = function(answer){
      console.log($scope.formData[$scope.configurationSelected.name]['tdm:analytics']['tdm:incorporates'][0]['tdm:incorporates'][0]['tdm:incorporates']);
      // Lo copio per avere una reference del vecchio dic
      angular.copy($scope.hideDictionary, $scope.oldDictionary); 
      
      //try{
        // Calcolo le interferenze parziali per la subconf che sto visualizzando
        var interference = $scope.transformToInterference($scope.formData[$scope.configurationSelected.name]);
        var dictionary = $scope.calculateDictionary(
          interference, 
          $scope.hideDictionary[$scope.configurationSelected.name], 
          $scope.conflicts, 
          $scope.notifications
        );
        debugger;
        //$scope.hideDictionary[$scope.configurationSelected] = dictionary;
        purgeDisabledAndSelected(dictionary);
      
        //TODO: Ricordarsi di modificare lo slider per avere 
        //le proprità esterne e poterlo disabilitare


        $scope.calculateNotifications(dictionary, $scope.oldDictionary[$scope.configurationSelected.name]);
      
        // TODO: DEVO CONTROLLARE SE NECESSARIA
        angular.copy(dictionary, $scope.hideDictionary[$scope.configurationSelected.name]);
     
     // } catch(err){
     //   console.log("Errore in calculateInterference");
     // }

      
      
    };

    function purgeDisabledAndSelected(dictionary){

      var data = jsonQ($scope.formData[$scope.configurationSelected.name]);

      jQuery.each(dictionary, function(stringElement){
        
        var element = fromStringToElement(stringElement);
        
        var parentElement = data.find("tdm:label", function(){
          return this.toLowerCase() == element.indicator;
        }).parent();

        var count = 0;
        parentElement.each(function (index, path, value) { 
          //value["tdm:incorporates"].length = 0;
          count += 1;
          debugger;
          switch(value["tdm:visualisationType"]) {
            case 'Option':
              value["tdm:incorporates"].length = 0;
              break;
            case 'Checkbox':
              removeFromCheckList(value, element);
              break;
            case 'Slider':
              value["tdm:incorporates"][0]['tdm:value'] = 0;
              break;
            default:
              break;
          }
        });

      });

    };

    $scope.checkAll = function(area, goalIndex, questionIndex){
      var elementsFromPath = getElementFromPath(area, goalIndex, questionIndex);
      elementsFromPath.formValue.length = 0;
      debugger;
      jQuery.each(elementsFromPath.elements, function(index, el){
        //elementsFromPath.formValue.push(angular.copy(el));
        elementsFromPath.formValue.push(el);
      });
      
    };

    $scope.uncheckAll = function(area, goalIndex, questionIndex){
      var elementsFromPath = getElementFromPath(area, goalIndex, questionIndex);
      elementsFromPath.formValue.length = 0;
    };

    function getElementFromPath(area, goalIndex, questionIndex){
      var areaToPick = {};
      var objectProperty = '';
      var elementsRetrieved = {};

      switch(area){
        case 'tdm:analytics':
          areaToPick = $scope.analytics;
          objectProperty = 'tdm:analytics';
          break;
        case 'tdm:processing':
          areaToPick = $scope.processing;
          objectProperty = 'tdm:processing';
          break;
        case 'tdm:display':
          areaToPick = $scope.display;
          objectProperty = 'tdm:display';
          break;
        case 'tdm:preparation':
          areaToPick = $scope.preparation;
          objectProperty = 'tdm:preparation';
          break;
        case 'tdm:representation':
          areaToPick = $scope.representation;
          objectProperty = 'tdm:representation';
          break;
        default:
          break;

      }

      var elementToAdd = jsonQ.pathValue(areaToPick, [
        'tdm:incorporates', 
        goalIndex ,
        'tdm:incorporates', 
        questionIndex, 
        'tdm:incorporates'
      ]);
       

      var formValues = jsonQ.pathValue($scope.formData[$scope.configurationSelected.name], [
        objectProperty, 
        'tdm:incorporates', 
        goalIndex, 
        'tdm:incorporates',
        questionIndex, 
        'tdm:incorporates'
        ]);

      elementsRetrieved.formValue = formValues;
      elementsRetrieved.elements = elementToAdd;

      return elementsRetrieved;
    };

    function removeFromCheckList(value, element){
      //if(value['tdm:incorporates'] )

      //var shallowCopy = $.extend({}, value['tdm:incorporates']);
      //$.each(shallowCopy, function(i, item){
      try{
        $.each(value['tdm:incorporates'], function(i, item){
          debugger;
          if(item !== undefined){
            if (item['tdm:label'].toLowerCase() == element.feature) value['tdm:incorporates'].splice(i, 1);
          }
        });
      }catch(err){
        console.log("Some error removing from checklist");
      }
      
      
    }

    $scope.calculateNotifications = function(dictionary, oldDictionary){
      /* if(jQuery.isEmptyObject(dictionary)){
        $scope.notifications.length = 0;
        return true;
      }
      */ 

      if(jQuery.isEmptyObject(oldDictionary)){
        jQuery.each(dictionary, function(el){
          var element = fromStringToElement(el);
          if(element !== undefined && !jQuery.isEmptyObject(element)){
            $scope.notifications.push(element);
          }
          
        });
        return true;
      } 

      jQuery.each(dictionary, function(el){
        if(oldDictionary[el] === undefined){
          var element = fromStringToElement(el);
          if(element !== undefined && !jQuery.isEmptyObject(element)){
            $scope.notifications.push(element);
          }
        }
      });
      
      //TODO: calcolo le differenze tra quello vecchio e il nuovo
      

      return true;
    };

    function fromStringToElement(element){
      var elementForNotification = element.split('::');
      var notification = {};
      
      if(elementForNotification[0] !== undefined){
        notification.area = elementForNotification[0];
      }

      if(elementForNotification[1] !== undefined){
        notification.goal = elementForNotification[1];
      }

      if(elementForNotification[2] !== undefined){
        notification.indicator = elementForNotification[2];
      }

      if(elementForNotification[3] !== undefined){
        notification.feature = elementForNotification[3];
      }

      return notification;
    };

    function titleCase(str) {
      var firstLetterRx = /(^|\s)[a-z]/g;
      return str.replace(firstLetterRx, upperCase);
    };

    // Function that controls if the element in the form should be disabled
    $scope.hideMe = function(mainType, goal, question, answer){
      var element = mainType.toLowerCase()+'::'+goal.toLowerCase()+'::'+question.toLowerCase()+'::'+answer.toLowerCase();
      //return $scope.hideDictionary[element]; //OLD SENZA MULTI
      return $scope.hideDictionary[$scope.configurationSelected.name][element];
    };

    $scope.transformToInterference = function(formData){
        var interference = [];
        var goals = ['tdm:analytics', 'tdm:display', 'tdm:preparation', 'tdm:processing', 'tdm:representation'];
        for(index = 0; index < goals.length; index++){
          var goal = goals[index];
          if(formData[goal] !== undefined){
            var area = formData[goal];
            for(var goalIndex in area['tdm:incorporates']){
              var goal = area['tdm:incorporates'][goalIndex];
              
              for(var indicatorIndex in goal['tdm:incorporates']){
                var indicator = goal['tdm:incorporates'][indicatorIndex];
                if(indicator['tdm:visualisationType'] !== 'Slider'){
                  for(var featureIndex in indicator['tdm:incorporates']){
                    //debugger;
                    var feature = indicator['tdm:incorporates'][featureIndex];
                    if(feature !== undefined){
                      var element = area['tdm:label'].toLowerCase()+'::'+goal['tdm:label'].toLowerCase()+'::'+indicator['tdm:label'].toLowerCase()+'::'+feature['tdm:label'].toLowerCase();
                      interference.push(element);
                    }
                  }
                }
              }
            }
          }
        }
        return interference;
      };

    $scope.calculateDictionary = function(interference, dictionary, conflicts, notifications){
        dictionary = {};
        notifications.length = 0;
        if(interference !== undefined && interference.length > 0){
          if(conflicts !== undefined){
            for(var index = 0; index < interference.length; index++){
              if(conflicts[interference[index]] !== undefined){
                for(var indexConflicts = 0; indexConflicts < conflicts[interference[index]].length; indexConflicts++){
                  var el = conflicts[interference[index]][indexConflicts];
                  dictionary[el] = true;
                }
              }
            }
          }
        }
        return dictionary;
      };

    $scope.clickToOpen = function () {
        ngDialog.openConfirm({ 
            template: 'confirmDialog', 
            className: 'ngdialog-theme-default',
            scope: $scope 
        }).then(function (success) {
          console.log($scope.formData);
          var formToSave = {}
          formToSave.anagraphic = angular.copy($scope.anagraphic);
          formToSave.form = angular.copy($scope.formData);
          formToSave.dictionary = angular.copy($scope.hideDictionary);

          ProjectsService.saveForm($stateParams.id, formToSave).then(function(response){
              console.log("Saved in the DB");
              $scope.platform_flow_type == 'code_based' ? $state.go('form.code_generation') : $state.go('form.services');
          })
            //$scope.$storage.formData = $scope.formData;
            
            
        }, function (error) {
            
        });
    };

    $scope.downloadConf = function($event) {

      var data, blob, url;
      data = angular.toJson($scope.formData, undefined, 4);
      blob = new Blob([data], {type: "application/json"});
      var url = URL.createObjectURL(blob);

      $event.target.parentNode.download = "configuration.json";
      $event.target.parentNode.href = url;
    };

    $scope.downloadGraph = function($event) {
      console.log("Sto per fare il download del grafo");
      var data, blob, url;
      data = angular.toJson({"id": "http://www.toreador.it", "elements": $localStorage.graph}, undefined, 4);

      blob = new Blob([data], {type: "application/json"});
      var url = URL.createObjectURL(blob);

      $event.target.parentElement.download = "graph.json";
      $event.target.parentElement.href = url;


    }; 

    $scope.downloadWorkflow = function($event) {

      var blob, url;
      blob = new Blob([$scope.oozieWorkflow.data], {type: "text/xml"});
      var url = URL.createObjectURL(blob);

      $event.target.parentElement.download = "workflow.xml";
      $event.target.parentElement.href = url;
    };

    $scope.downloadOwls = function($event) {

      var blob, url;
      blob = new Blob([$scope.owlsWorkflow.data], {type: "text/xml"});
      
      var url = URL.createObjectURL(blob);

      $event.target.parentElement.download = "workflow.owls";
      $event.target.parentElement.href = url;
    };

    $scope.addToTargetDataSource = function(){
      $scope.formData[$scope.configurationSelected.name]['tdm:targetDataSources'].push('');
    };
    
    $scope.removeFromTargetDataSource = function(index){
      $scope.formData[$scope.configurationSelected.name]['tdm:targetDataSources'].splice(index, 1);
    };

    $scope.addToEgestionDataSource = function(){
      $scope.egestionDataSources.push('');
    };
    
    $scope.removeFromEgestionDataSource = function(index){
      $scope.egestionDataSources.splice(index, 1);
    };

    $scope.reset = function(){
      //TODO: Mettere a posto
      DataService.getCleanForm().then(
        function(data){
          $scope.anagraphic = {};
          $scope.formData[$scope.configurationsName[0]] = data;
          $scope.configurationSelected.name = $scope.configurationsName[0];
        }).catch(
        function(err){
          console.log("Errore");
        });
    };

    $scope.compileWorkflow = function(id, owls, platform){
      var myId = parseInt(id,10);
      var body = {"id": myId, "OWLS": owls, "platform": platform};
      DataService.compileWorkflow(body)
      .then(function(){
        console.log("Lanciato il run");
        $state.go('form.workflow');
      }).catch(function(){
        console.log("Something goes wrong");
      });
    };

    $scope.runWorkflow = function(id, owls){
      var myId = parseInt(id,10);
      var body = {"id": myId, "OWLS": owls};
      DataService.runWorkflow(body)
      .then(function(){
        $scope.done=true;
        console.log("Lanciato il run");
      }).catch(function(){
        console.log("Something goes wrong");
      });
    };

    $scope.goToOwls = function(){
       //$localStorage.graph=$scope.graph.json().elements;
       $state.go('form.owls');
    };

    $scope.goToCodeBasedConstraint = function(){
       //$localStorage.graph=$scope.graph.json().elements;
       $state.go('form.owls');
    };

    $scope.goToCodeBasedCompiler = function(id){
      if($scope.platform_flow_type=='code_based'){
        ProjectsService.saveConstraint(id, $scope.constraint).then(function(){
            $state.go('form.code_based_generator');
            
        }).catch(function(){
          $rootScope.addAlert('danger', 'error', 'This project cannot go to the Code Based Compiler');
        });
      } else {
          $rootScope.addAlert('danger', 'error', 'This project cannot go to the Code Based Compiler');
      }
    };

    $scope.calculateRandom = function getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $scope.priceSlider = 100;

    $scope.sliders = {};

    $scope.getSliderOptions = function(mainType, goal, question, answer){
      var element = mainType+'::'+goal+'::'+question+'::'+answer;
      return $scope.sliders[element];
    };

    $scope.findInArray = function(form, goalIndex, questionIndex, answerLabel){
      
      indexes = $.map($scope.formData[$scope.configurationSelected.name][form]['tdm:incorporates'][goalIndex]['tdm:incorporates'][questionIndex]['tdm:incorporates'], function(obj, index) {
        if(obj['tdm:label'] == answerLabel) {
          return index;
        }
      })
      return indexes[0];
    }

    $scope.transformToOozieWorkflow = function(){
      $localStorage.owls = $scope.owlsWorkflow;
      $state.go('form.workflow');
    };

    // Update the anagraphic on each subconfiguration
    $scope.$watch('anagraphic', function(newVal, oldVal){
      angular.forEach($scope.formData, function(subconf, key) {
        subconf['tdm:anagraphic'] = newVal;
      });
    }, true);

    $scope.logout = function(){
      console.log("Mi sto sloggando");
      UserService.logout();
    };

    $scope.showConfigurations = function(){
      $state.go('main_projects');
    };

    $scope.getInputType = function(inputType){
     
      if(inputType === "STRING"){
        return "text";
      }  
      else if(inputType === "INT"){
        return "int";
      }
      else if(inputType === "DOUBLE"){
        return "double";
      } 
      else if(inputType === "BOOLEAN"){
        return "boolean";
      }
        
    };

    $scope.saveServiceComposition = function(){
      ProjectsService.saveWorkflow($stateParams.id, $scope.elements)
      .then(function(response){
        // Salvo il tutto nel localstorage
        $localStorage.elements = $scope.elements;
        //$localStorage.graph = convertFromElementsToGraph($scope.elements);
        /*cytoData.getGraph().then(function(graph){
          $localStorage.graph = graph.json().elements;
        });*/
      });
       
    };


    function filterTerminator(value){
      return value.data.nodeType.type.toUpperCase() !== "START" && value.data.nodeType.type.toUpperCase() !== "STOP";
    };

    function linkedToTerminator(value){
      return value.data.source.toUpperCase() !== "START" && value.data.target.toUpperCase() !== "STOP";

    };

    $scope.saveTemplate = function(){
      //TODO: Fare vera funzione di salvataggio template
      ngDialog.openConfirm({ 
          template: 'saveTemplate', 
          className: 'ngdialog-theme-default',
          scope: $scope 
      }).then(function (success) {
          if($scope.newTemplate.name !== undefined  
              && $scope.newTemplate.name !== ''){
              

              //Appendo all'oggetto template
              cytoData.getGraph().then(function(graph){
                $scope.newTemplate.graph = graph.json().elements;
                
                //Filter the edges and nodes to remove the terminator nodes and edges
                var edges = $scope.newTemplate.graph.edges.filter(linkedToTerminator);
                var nodes = $scope.newTemplate.graph.nodes.filter(filterTerminator);
                
                $scope.newTemplate.graph.edges = edges;
                $scope.newTemplate.graph.nodes = nodes;
                $scope.newTemplate.batch = true;
                DataService.saveTemplate($scope.newTemplate).then(function(response){
                  
                  $scope.templates.push($scope.newTemplate);
                  $scope.newTemplate = {};
                });
                
              });
              
              

            };
      });
    };


    $scope.saveOwls = function(){
      
      ProjectsService.saveOwls($stateParams.id, $scope.owlsWorkflow.data)
      .then(function(response){
        // Salvo il tutto nel localstorage
        $localStorage.owlsWorkflow = $scope.owlsWorkflow;
      });
       
    };

    $scope.saveWorkflow = function(){
      
      ProjectsService.saveOozie($stateParams.id, $scope.oozieWorkflow.data)
      .then(function(response){
        // Salvo il tutto nel localstorage
        $localStorage.oozieWorkflow = $scope.oozieWorkflow.data;
      });
       
    };

    $scope.saveForm = function(){
      var formToSave = {}
      formToSave.anagraphic = angular.copy($scope.anagraphic);
      formToSave.form = angular.copy($scope.formData);
      formToSave.dictionary = angular.copy($scope.hideDictionary);

      ProjectsService.saveForm($stateParams.id, formToSave).then(function(response){
        console.log("Saved in the DB");
      })
      
    };

    //TODO: Check di questa funzione. Mi serve ancora o no
    $scope.saveAsTemplate = function(){
      var formToSave = {}
      formToSave.anagraphic = angular.copy($scope.anagraphic);
      formToSave.form = angular.copy($scope.formData);
      formToSave.dictionary = angular.copy($scope.hideDictionary);

      ProjectsService.saveForm($stateParams.id, formToSave).then(function(response){
        console.log("Saved in the DB");
      })

      ngDialog.openConfirm({ 
            template: 'confirmDialog', 
            className: 'ngdialog-theme-default',
            scope: $scope 
      }).then(function (success) {
            // salvo tutta la form come template
            var formToSave = {}
            formToSave.description = angular.copy($scope.description);
            formToSave.anagraphic = angular.copy($scope.anagraphic);
            formToSave.form = angular.copy($scope.formData);
            formToSave.dictionary = angular.copy($scope.hideDictionary);
            formToSave.owlsWorkflow = angular.copy($localStorage.owlsWorkflow);
            formToSave.oozieWorkflow = angular.copy($localStorage.oozieWorkflow);
        }, function (error) {
            
        });
      
    };

    $scope.hideOverallAccuracy = function(){
      var interference = $scope.transformToInterference($scope.formData[$scope.configurationSelected.name]);
      for(i=0; i< interference.length; i++){
        if(interference[i] === 'data preparation::anonymization::anonymization techniques::k-anonimity family'){
          return true;
        }
      }
      return false;
    }

    $scope.removeNull = function(arrayToClean){
      if(arrayToClean.length >= 1){
        arrayToClean.splice(0, 1);
        return true;
      }
      return false;
      
    };

    $scope.showResults = function(id){
      $state.go('form.visualization');
    }

    $scope.iframeLoaded = function () {
      var iFrameID = document.getElementById('visualization');
      debugger;
      if(iFrameID) {
            iFrameID.height = "";
            iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + 2000 + "px";
            console.log(iFrameID.height);
      }   
    }



}]);

