class RemoveAnagraphicFromTemplates < ActiveRecord::Migration[5.0]
  
  def change
  	remove_column :templates, :anagraphic, :mediumtext 
  	remove_column :templates, :hide_dictionary, :longtext 
  	remove_column :templates, :workflow, :longtext 
  	remove_column :templates, :owls_workflow, :longtext 
  	remove_column :templates, :oozie_workflow, :longtext 
  	remove_column :templates, :visualization_url, :mediumtext
  	add_column :templates, :graph, :longtext
  	add_column :templates, :batch, :boolean
  end
end
