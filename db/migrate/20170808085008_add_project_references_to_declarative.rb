class AddProjectReferencesToDeclarative < ActiveRecord::Migration[5.0]
  def change
  	add_reference :declaratives, :project, index: true
  	add_foreign_key :declaratives, :projects
  end
end
