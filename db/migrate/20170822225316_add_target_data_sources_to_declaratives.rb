class AddTargetDataSourcesToDeclaratives < ActiveRecord::Migration[5.0]
  def change
  	add_column :declaratives, :target_data_sources, :longtext
  end
end
