class AddUidToUsers < ActiveRecord::Migration[5.0]
  def change
    ## Required
    add_column :users, :provider, :string, :null => false, :default => "email"
    add_column :users, :uid, :string, :null => false, :default => ""
  end
end
