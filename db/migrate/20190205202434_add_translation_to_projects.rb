class AddTranslationToProjects < ActiveRecord::Migration[5.0]
  def up
  	execute <<-SQL
        ALTER TABLE projects ADD translation enum('dsl', 'oozie');
    SQL
  end

  def down
    remove_column :projects, :translation
  end
end
