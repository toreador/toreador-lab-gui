class AddHideDictionaryToProjects < ActiveRecord::Migration[5.0]
  def change
  	add_column :projects, :hide_dictionary, :longtext
  end
end
