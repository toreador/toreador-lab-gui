class AddNameAddSurnameToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string, default: ""
    add_column :users, :surname, :string, default: ""
  end
end
