class AddPlatformFlowTypeToProject < ActiveRecord::Migration[5.0]
  def up
  	execute <<-SQL
        ALTER TABLE projects ADD platform_flow_type enum('service_based', 'code_based') default 'service_based';
    SQL
  end

  def down
    remove_column :projects, :platform_flow_type
  end
end
