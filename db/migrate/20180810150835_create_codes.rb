class CreateCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :codes do |t|
    	t.string :name
    	t.longtext :code
    	t.mediumtext :platform
    	t.mediumtext :deployment
    	t.mediumtext :result
    	t.mediumtext :log
    	t.mediumtext :code_generated
    	t.boolean :running

    	t.references :main_project, foreign_key: true

    	t.timestamps
    end

  end
end
