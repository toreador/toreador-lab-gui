class AddVisualizationColumnToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :visualization_url, :mediumtext
  end
end
