class AddConstraintToProject < ActiveRecord::Migration[5.0]
  def change
  	add_column :projects, :constraint, :longtext
  end
end
