class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.mediumtext :anagraphic
      t.longtext :workflow
      t.longtext :owls_workflow
      t.longtext :oozie_workflow

      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
