class CreateTemplatesTable < ActiveRecord::Migration[5.0]
  def change
    create_table :templates do |t|
      t.string :name
      t.longtext :description
      t.mediumtext :anagraphic
      t.longtext :hide_dictionary
      t.longtext :workflow
      t.longtext :owls_workflow
      t.longtext :oozie_workflow
      t.mediumtext :visualization_url

      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
