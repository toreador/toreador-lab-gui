class RenameTranslationInProjects < ActiveRecord::Migration[5.0]
  def change
  	rename_column :projects, :translation, :platform
  end
end
