class CreateDeclarativesTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :declarative_templates do |t|
      t.string :name
      t.mediumtext :anagraphic
      t.longtext :target_data_sources
      t.longtext :representation
      t.longtext :preparation
      t.longtext :analytics
      t.longtext :processing
      t.longtext :display

      t.references :templates, foreign_key: true

      t.timestamps
    end
  end
end
