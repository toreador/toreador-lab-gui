class CreateDeclaratives < ActiveRecord::Migration[5.0]
  def change
    create_table :declaratives do |t|
      t.string :name
      t.mediumtext :anagraphic
      t.longtext :representation
      t.longtext :preparation
      t.longtext :analytics
      t.longtext :processing
      t.longtext :display

      t.timestamps
    end
  end
end
