class ChangeMainProjects < ActiveRecord::Migration[5.0]
  def change

  	#add_column :main_projects, :name, :string, default: ""
  	#add_column :main_projects, :description, :mediumtext
  	#add_reference :main_projects, :user, index: true
  	#add_foreign_key :main_projects, :user

  	add_timestamps(:main_projects)
  end
end
