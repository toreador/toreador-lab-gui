class AddIntegratioToDeclaratives < ActiveRecord::Migration[5.0]
  def change
  	add_column :declaratives, :integration, :longtext
  end
end
