class AddColumnTypeToProjects < ActiveRecord::Migration[5.0]
  def change
  	add_column :projects, :type, :string, default: "batch"
  end
end
