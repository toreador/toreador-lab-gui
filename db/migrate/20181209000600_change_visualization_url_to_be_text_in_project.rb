class ChangeVisualizationUrlToBeTextInProject < ActiveRecord::Migration[5.0]
  def up
  	change_column_default :projects, :visualization_url, nil
  	change_column :projects, :visualization_url, :mediumtext
  end

  def down
  	change_column :projects, :visualization_url, :string
  	change_column_default :projects, :visualization_url, ''
  end
end
