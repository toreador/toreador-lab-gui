class AddMainProjectsReferencesToProjects < ActiveRecord::Migration[5.0]
  def change
  	add_reference :projects, :main_project, index: true
  	add_foreign_key :projects, :main_projects
  end
end
