class ChangeTypeColumn < ActiveRecord::Migration[5.0]
  def change
  	rename_column :projects, :type, :flow_type
  end
end
