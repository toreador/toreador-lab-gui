# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190215180227) do

  create_table "codes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "name"
    t.text     "code",            limit: 4294967295
    t.text     "platform",        limit: 16777215
    t.text     "deployment",      limit: 16777215
    t.text     "result",          limit: 16777215
    t.text     "log",             limit: 16777215
    t.text     "code_generated",  limit: 16777215
    t.boolean  "running"
    t.integer  "main_project_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["main_project_id"], name: "index_codes_on_main_project_id", using: :btree
  end

  create_table "credentials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "platform"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "declarative_templates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "name"
    t.text     "anagraphic",          limit: 16777215
    t.text     "target_data_sources", limit: 4294967295
    t.text     "representation",      limit: 4294967295
    t.text     "preparation",         limit: 4294967295
    t.text     "analytics",           limit: 4294967295
    t.text     "processing",          limit: 4294967295
    t.text     "display",             limit: 4294967295
    t.integer  "templates_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["templates_id"], name: "index_declarative_templates_on_templates_id", using: :btree
  end

  create_table "declaratives", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "name"
    t.text     "anagraphic",          limit: 16777215
    t.text     "representation",      limit: 4294967295
    t.text     "preparation",         limit: 4294967295
    t.text     "analytics",           limit: 4294967295
    t.text     "processing",          limit: 4294967295
    t.text     "display",             limit: 4294967295
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "project_id"
    t.text     "target_data_sources", limit: 4294967295
    t.text     "integration",         limit: 4294967295
    t.index ["project_id"], name: "index_declaratives_on_project_id", using: :btree
  end

  create_table "main_projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "name",                         default: ""
    t.text     "description", limit: 16777215
    t.integer  "user_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["user_id"], name: "index_main_projects_on_user_id", using: :btree
  end

  create_table "projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "name"
    t.text     "anagraphic",         limit: 16777215
    t.text     "workflow",           limit: 4294967295
    t.text     "owls_workflow",      limit: 4294967295
    t.text     "oozie_workflow",     limit: 4294967295
    t.integer  "user_id"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.text     "hide_dictionary",    limit: 4294967295
    t.text     "visualization_url",  limit: 16777215
    t.boolean  "done",                                  default: false
    t.string   "flow_type",                             default: "batch"
    t.integer  "main_project_id"
    t.string   "platform",           limit: 5
    t.string   "platform_flow_type", limit: 13,         default: "service_based"
    t.text     "constraint",         limit: 4294967295
    t.index ["main_project_id"], name: "index_projects_on_main_project_id", using: :btree
    t.index ["user_id"], name: "index_projects_on_user_id", using: :btree
  end

  create_table "templates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "name"
    t.text     "description", limit: 4294967295
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "graph",       limit: 4294967295
    t.boolean  "batch"
    t.index ["user_id"], name: "index_templates_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.string   "email",                                default: "",      null: false
    t.string   "encrypted_password",                   default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "name",                                 default: ""
    t.string   "surname",                              default: ""
    t.boolean  "is_admin",                             default: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "unconfirmed_email"
    t.text     "tokens",                 limit: 65535
    t.string   "provider",                             default: "email", null: false
    t.string   "uid",                                  default: "",      null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "codes", "main_projects"
  add_foreign_key "declarative_templates", "templates", column: "templates_id"
  add_foreign_key "declaratives", "projects"
  add_foreign_key "projects", "main_projects"
  add_foreign_key "projects", "users"
  add_foreign_key "templates", "users"
end
