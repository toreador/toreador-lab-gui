Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  # devise_for :users
  
  # # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # get 'user/all' => 'users#all', as: :user_all
  # get 'user/:id' => 'users#show', as: :user_show
  # put 'user/:id/update' => 'users#update', as: :user_update
  # get 'user/:id/edit' => 'users#edit', as: :user_edit
  # delete 'user/:id/delete' => 'users#delete', as: :user_delete
  # get 'configuration/new' => 'configurations#new', as: :configuration_new
  # #get 'user/all' => 'users#all', as: :user_all

  # authenticated :user do
  #   root :to => "users#index", as: :user_index
  # end

  # unauthenticated :user do
  #   devise_scope :user do
  #     get "/" => "devise/sessions#new"
  #   end
  # end

  root to: 'application#angular'
  get 'configuration/getcleansubconf' => 'configurations#cleansubconf', as: :configuration_get_clean_subconf
  get 'configuration/getoozie/:id' => 'configurations#getoozie', as: :configuration_get_oozie
  post 'configuration/getowls' => 'configurations#getowls', as: :configuration_get_owls
  post 'configuration/run' => 'configurations#run', as: :configuration_run
  post 'configuration/compile' => 'configurations#compile', as: :configuration_compile
  post 'project/close' => 'configurations#close_run', as: :close_run

  get 'projects/index/:id' => 'projects#index', as: :projects
  delete 'projects/:id' => 'projects#destroy', as: :projects_destroy
  post 'projects/new' => 'projects#new', as: :projects_new
  post 'projects/clone' => 'projects#clone', as: :projects_clone
  get 'projects/:id' => 'projects#show', as: :projects_get
  post 'projects/workflow' => 'projects#save_workflow', as: :projects_save_workflow
  get 'projects/workflow/:id' => 'projects#get_workflow', as: :projects_get_workflow
  post 'projects/owls' => 'projects#save_owls', as: :projects_save_owls
  post 'projects/oozie' => 'projects#save_oozie', as: :projects_save_oozie
  post 'projects/constraint' => 'projects#save_constraint', as: :projects_save_constraint
  post 'projects/declarative' => 'projects#save_declarative', as: :projects_save_declarative

  get 'main_projects/index' => 'main_projects#index', as: :main_projects
  post 'main_projects/clone' => 'main_projects#clone', as: :main_projects_clone
  delete 'main_projects/:id' => 'main_projects#destroy', as: :main_projects_destroy
  post 'main_projects/new' => 'main_projects#new', as: :main_projects_new

  post 'declaratives/new' => 'declaratives#new', as: :declaratives_new
  delete 'declaratives/:id' => 'declaratives#destroy', as: :declaratives_destroy

  post 'configurations/getserviceselection' => 'configurations#getserviceselection', as: :configurations_get_service_selection
  post 'templates/new' => 'templates#new', as: :template_new
  delete 'templates/:id' => 'templates#destroy', as: :templates_destroy
  get 'templates/index' => 'templates#index', as: :templates

  delete 'codes/:id' => 'codes#destroy', as: :codes_destroy
  post 'codes/clone' => 'codes#clone', as: :codes_clone
  get 'codes/:id' => 'codes#show', as: :codes_get
  post 'codes/save' => 'codes#save', as: :codes_save


end
