# README

This project aims to provide a UI to the Toreador Lab, in order to provide the users the possibility to interact with the System and compile the Service Composition to the appropriate platform

Things you may want to cover:

* Ruby version: 2.3.1

* System dependencies: 
    - Rails
    - a SQL DB (MariaDB or MySQL)